# The Island

## Table of Contents
* Overview
* Manual
* Obtaining the Source Code
* Contributing
* License
* Reporting Issues

## I. Overview

The Island is a sandbox game. Your character is stranded on a large island,
making the only goal survival. Unfortunately, the flora and fauna living on this
island want to do unspeakable things to you.

This is very much still a work in progress.

## II. Manual

The game has a built-in manual of dubious quality. The rest of this section will
be a manual for installing and running the game.

Gitlab's support for binary releases is nearly nonexistent. Further instructions
will be provided here when the first release is ready. If you are impatient and
want to build the code yourself, install [Go](https://golang.org/doc/install),
fetch this repository, build it, and run the binary from the main directory.

Once the release files are downloaded for your operating system, simply run the
executable. It will start up a web server only accessible locally, so visit
[http://localhost:8080/game](http://localhost:8080/game) in your browser to
play.

Note that your browser isn't really the game, it is the server that is. So if
you shutdown the server but leave your browser open, you'll have to reload the
game next time you start up the server (even if it looks like the game is still
running in the browser).

## III. Obtaining the Source Code

The canonical location for the code is on
[gitlab](https://gitlab.com/tydl/theisland), though if you are using a different
fork then it may live elsewhere.

## IV. Contributing
See the accompanying CONTRIBUTING.md file.

## V. License
See the accompanying COPYING file. This work is released under the GNU GPLv3
license. Share all the shit openly. So if you disagree how things are going:
fuck it and fork it.

## VI. Reporting Issues

Open an [issue](https://gitlab.com/tydl/theisland/issues/new)
and be sure to say **shit's broke**. If you are wanting to start a discussion,
label it as **bullshit**. Finally, if you are requesting a feature, be sure to
mark it as **new shit**. Be careful if you use foul language, I may mark it pink
with *pottymouth*.
