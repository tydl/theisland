# Contributing to The Island

## Prerequisites

So you want to contribute to a game inspired by content on /d/, eh? You will
need to go through this list and check off as many as you can to make sure you
really are up for it:

* Motivation
* Thick Skin
* Strong passion in weakly-held ideas (AKA: Open to change)
* Understanding of the Go language
* Understanding of HTML, CSS, Javascript
* More specifically, the React Javascript framework (and JSX)
* No, really, you need motivation
* Willingness to undergo code reviews
* Understanding of concurrent applications
* How to use git
* How to make things pretty
* Seriously, this stuff takes effort so you need to be motivated
* An imagination
* How to write smut
* Understanding the theory of dependency injection and a wild imagination in how
it could be bastardized.

Or, to substitute this in for some of the above points:

* The patience and willingness to learn and self-study

This is an incomplete list, one that no one is expected to hit all the points.
However, it is meant to keep you aware of the kinds of things that are involved
with this project.

## Reading

Reading is the best thing that can be done to get started on the project. I
would first start with looking through the current [open issues](https://gitlab.com/tydl/theisland/issues) for a social overview and the
`viking/core` package for a code overview.

## Repository Description

The code is split into three parts:

1. The browser code for rendering the game and making it look not like shit from
1998.
2. The server code that builds the engine. The engine handles boring stuff like
network requests, request lifecycles, event chains, data serving, time-ticking,
etc. It also handles exciting game mechanics like crafting, harvesting,
movement, etc.
3. The server code that configures the engine so a vibrant world can be
presented to a player. It is the "skin" of the game. It leverages the engine's
game mechanics to do so.

The code is split in the following structure:

* `main.go`: This is the main server file that has the game server configuration
meeting the game server engine.
* `js/jsx/`: The main browser code being rendered to the player. It is written
using JSX. It is compiled into the `js/` folder.
* `css/`: Your typical css to make stuff look like 2008 instead of 1998.
* `phalanx/`: The server code for configuration; all the spoilers and flavors of
the game. Avoid this folder if you do not want spoilers. Live in this folder if
all you want to do is write smut and add content.
* `viking/`: The server code for the engine; all the boring and some exciting
stuff. The core concept of the server code is outlined in
`viking/core/interfaces.go`. Modifying the server is very easy since a unit of
code is kept in a `Module`. While modules may depend on one another, their
isolated nature makes it relatively easy to allow you to mod the game and add
your favorite feature. The lack of generics in Go mean this is usually painful
to execute, though. Everything in `viking/` is a module except for `core/` and
`mapgen/`.
* `viking/mapgen/`: This folder is special because it is not a module and is not
`core/`. It is all the crazy world generation code. It is based on the
description of
[this guy's work](http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/)
but has several modifications for this game and does not implement everything he
outlines. It can be built as a standalone program.

## Contribution Process

First, find an issue you want to work on. Open a new one if there is none. Be
sure to mark it with the appropriate label. I may^H^H^Hwill get anal about this
because no one likes shit coming out of the blue *at* them, and no one likes
having *their* shit coming out of the blue and being torn apart or marked
`duped`.

Once enough discussion has happened to give you the confidence to make a change,
do so: fork the repository, download your local copy, do the open source dance
and get the merge request in. Feel free to talk about design along the way.

When submitting a merge request, ask the following things:

* Did I collapse my changes into a single git commit (`rebase` and `squash`)?
* (Optional but suggested) Did I sign the commit with my
[GPG](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work)?
* Did I run my code through `go fmt`?
* Are my comments Golang-convention/`golint` compliant?
* Did `go vet` return all clear?
* Do I have sufficient test coverage?

Older code may not have tests nor comments. I will also **rarely** accept code
missing both. However, I am a fan of Go's testing tools. Hint: If you want to
get on my good side, submit a bunch of merge requests adding tests while you
learn the codebase. I appreciate comments worth having. Don't sweat the simple
shit.

Your code will undergo a review. You will more than likely have to make at least
one change. If this offends your ego, see the second bullet point in the
Overview.

## Changing Process

Feel free to discuss process changes and submit merge requests for this
document. Similar to being on The Island, adapt or die.
