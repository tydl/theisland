package empty

import (
	"html/template"

	"gitlab.com/tydl/theisland/viking/core"
)

const (
	emptyModuleName = "Empty: BAD DEVELOPER"
)

var _ core.Moduler = &EmptyModule{}

type EmptyModule struct{}

func (*EmptyModule) Name() string {
	return emptyModuleName
}

func (*EmptyModule) SaveLoaders() []core.SaveLoader {
	return nil
}

func (*EmptyModule) TimePassers() []core.TimePasser {
	return nil
}

func (*EmptyModule) Actions() []core.Actions {
	return nil
}

func (*EmptyModule) DataHandlers() []core.DataHandlers {
	return nil
}

func (*EmptyModule) ManualEntries() []core.ManualEntry {
	return nil
}

func (*EmptyModule) Observers() []core.ObserveEvent {
	return nil
}

func (*EmptyModule) Templates() []*template.Template {
	return nil
}
