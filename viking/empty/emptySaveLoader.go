package empty

import (
	"gitlab.com/tydl/theisland/viking/core"
)

const (
	emptySaveLoaderId = "Empty: BAD DEVELOPER"
)

var _ core.SaveLoader = &EmptySaveLoader{}

type EmptySaveLoader struct{}

func (*EmptySaveLoader) Id() string {
	return emptySaveLoaderId
}

func (*EmptySaveLoader) Save() (data interface{}, version int, err error) {
	return nil, 0, nil
}

func (*EmptySaveLoader) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	return nil
}
