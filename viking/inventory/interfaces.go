package inventory

import "gitlab.com/tydl/theisland/viking/core"

type ItemMetadata struct {
	Kind       string
	Id         int
	Consumable func(i *InventoryItem) bool
	Consume    func(i *InventoryItem, s core.Systemer) error
}

type ItemSlot struct {
	Item     *InventoryItem
	Quantity int
}

type InventoryItem struct {
	Kind          string
	Id            int
	Name          string
	DescriptionId string
	Metadata      map[string]interface{}
}

type InventoryItemQuantity struct {
	Item     InventoryItem
	Quantity int
}

func (i *InventoryItem) Equals(other *InventoryItem) bool {
	if i.Kind != other.Kind ||
		i.Id != other.Id ||
		len(i.Metadata) != len(other.Metadata) {
		return false
	}
	for k, v := range i.Metadata {
		if v != other.Metadata[k] {
			return true
		}
	}
	return true
}

func (i *InventoryItem) Copy() *InventoryItem {
	other := &InventoryItem{}
	other.Kind = i.Kind
	other.Id = i.Id
	other.Name = i.Name
	other.DescriptionId = i.DescriptionId
	copy := make(map[string]interface{})
	for k, v := range i.Metadata {
		copy[k] = v
	}
	other.Metadata = copy
	return other
}
