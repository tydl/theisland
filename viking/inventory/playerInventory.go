package inventory

import (
	"sort"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/meta"
	"gitlab.com/tydl/theisland/viking/templates"
)

const (
	PlayerInventoryId = "PlayerInventory"
	kindSlotVersion   = 0
)

var _ core.SaveLoader = &PlayerInventory{}
var _ core.Observer = &PlayerInventory{}

type PlayerInventory struct {
	inventory map[string]*kindSlot
	metaData  map[string]ItemMetadata
}

func newPlayerInventory(itemMetaData []ItemMetadata) *PlayerInventory {
	p := &PlayerInventory{
		make(map[string]*kindSlot),
		make(map[string]ItemMetadata, len(itemMetaData)),
	}
	for _, metaData := range itemMetaData {
		p.metaData[metaData.Kind] = metaData
	}
	return p
}

type kindSlot struct {
	Items []*ItemSlot
}

func (p *PlayerInventory) Id() string {
	return PlayerInventoryId
}

func (p *PlayerInventory) Save() (data interface{}, version int, err error) {
	return p.inventory, kindSlotVersion, nil
}

func (p *PlayerInventory) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	switch version {
	case kindSlotVersion:
		p.inventory = make(map[string]*kindSlot)
		if err := loadFunction(&p.inventory); err != nil {
			return errors.StackWrap(err)
		}
	default:
		return errors.NewStackErrorStringf("Unknown version %d", version)
	}
	return nil
}

func (p *PlayerInventory) Observe(e core.Event, s core.Systemer) error {
	switch e.Name() {
	case meta.CreateGameEventName:
		p.inventory = make(map[string]*kindSlot)
	}
	return nil
}

func (p *PlayerInventory) GetInventoryItemsForKind(kind string) []*ItemSlot {
	kSlot, ok := p.inventory[kind]
	if ok {
		return kSlot.Items
	}
	return nil
}

func (p *PlayerInventory) GetInventoryItemsForKindId(kind string, id int) *ItemSlot {
	kSlot, ok := p.inventory[kind]
	if !ok {
		return nil
	}
	for _, itemSlot := range kSlot.Items {
		if itemSlot.Item.Id == id {
			return itemSlot
		}
	}
	return nil
}

func (p *PlayerInventory) RemoveInventoryItemsForKindId(kind string, id int, quantity int) bool {
	kSlot, ok := p.inventory[kind]
	if !ok {
		return false
	}
	for idx, itemSlot := range kSlot.Items {
		if itemSlot.Item.Id == id {
			if itemSlot.Quantity > quantity {
				itemSlot.Quantity -= quantity
				return true
			} else if itemSlot.Quantity == quantity {
				kSlot.Items = append(kSlot.Items[:idx], kSlot.Items[idx+1:]...)
				return true
			} else {
				return false
			}
		}
	}
	return false
}

func (p *PlayerInventory) ConsumeItem(kind string, id int, s core.Systemer) error {
	metaData, ok := p.metaData[kind]
	if !ok {
		return errors.NewStackErrorString("No item with kind " + kind)
	}
	slot := p.GetInventoryItemsForKindId(kind, id)
	if slot == nil {
		return errors.NewStackErrorStringf("No items in inventory with kind %s and id %d", kind, id)
	}
	if !metaData.Consumable(slot.Item) {
		return errors.NewStackErrorStringf("Items with kind %s and id %d is not consumable", kind, id)
	}
	if !p.RemoveInventoryItemsForKindId(kind, id, 1) {
		return errors.NewStackErrorStringf("No items in inventory with kind %s and id %d", kind, id)
	}
	return metaData.Consume(slot.Item, s)
}

func (p *PlayerInventory) Add(i ...*InventoryItem) {
	for _, item := range i {
		p.addItem(item)
	}
}

func (p *PlayerInventory) AddQuantity(item *InventoryItem, quantity int) {
	for i := 0; i < quantity; i++ {
		p.addItem(item)
	}
}

func (p *PlayerInventory) addItem(i *InventoryItem) {
	kSlot, ok := p.inventory[i.Kind]
	if !ok {
		kSlot = &kindSlot{
			Items: make([]*ItemSlot, 0, 1),
		}
	}
	bFound := false
	for _, iSlot := range kSlot.Items {
		if iSlot.Item.Equals(i) {
			bFound = true
			iSlot.Quantity++
		}
	}
	if !bFound {
		kSlot.Items = append(kSlot.Items, &ItemSlot{
			Item:     i,
			Quantity: 1,
		})
		p.inventory[i.Kind] = kSlot
	}
}

type inventoryItemJson struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Quantity    int    `json:"quantity"`
	Consumable  bool   `json:"consumable"`
	Kind        string `json:"kind"`
	Id          int    `json:"id"`
}

type inventoryItemJsonSlice []inventoryItemJson

func (inv inventoryItemJsonSlice) Len() int {
	return len(inv)
}

func (inv inventoryItemJsonSlice) Less(i, j int) bool {
	return inv[i].Name < inv[j].Name ||
		(inv[i].Name == inv[j].Name && inv[i].Description < inv[j].Description)
}

func (inv inventoryItemJsonSlice) Swap(i, j int) {
	inv[i], inv[j] = inv[j], inv[i]
}

func (p *PlayerInventory) handleGetPlayerInventory(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	tempSys, err := templates.GetTemplateSystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	toReturn := make([]inventoryItemJson, 0, len(p.inventory))
	for _, kindItem := range p.inventory {
		for _, itemSlot := range kindItem.Items {
			description, err := tempSys.Execute(itemSlot.Item.DescriptionId)
			if err != nil {
				return nil, errors.StackWrap(err)
			}
			consumable := false
			if metaData, ok := p.metaData[itemSlot.Item.Kind]; ok {
				consumable = metaData.Consumable(itemSlot.Item)
			}
			toReturn = append(toReturn, inventoryItemJson{
				Name:        itemSlot.Item.Name,
				Description: description,
				Quantity:    itemSlot.Quantity,
				Consumable:  consumable,
				Kind:        itemSlot.Item.Kind,
				Id:          itemSlot.Item.Id,
			})
		}
	}
	sort.Sort(inventoryItemJsonSlice(toReturn))
	return toReturn, nil
}

func GetPlayerInventorySystem(s core.Systemer) (*PlayerInventory, error) {
	system, err := s.System(PlayerInventoryId)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	casted, ok := system.(*PlayerInventory)
	if !ok {
		return nil, errors.NewStackErrorString("could not cast to *PlayerInventory")
	}
	return casted, nil
}
