package inventory

import "gitlab.com/tydl/theisland/viking/core"

func getManualEntries() []core.ManualEntry {
	return []core.ManualEntry{
		{
			Title: "Inventory",
			Text: "A player's inventory holds items and resources gained in the world " +
				"from resource nodes or attained by crafting. Items weigh nothing and occupy " +
				"no space so a PC can carry an infinite amount of items. From the inventory " +
				"a PC can consume items directly, if they are consumable.",
		},
		{
			Title: "Item",
			Text: "An item is anything that can be put into a PC's inventory. The item " +
				"could be attained from resource nodes or crafting. They may or may not be " +
				"consumable for the PC's use. They may also be a recipe item for a crafting " +
				"recipe.",
		},
	}
}
