package inventory

import (
	"html/template"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/empty"
	"gitlab.com/tydl/theisland/viking/meta"
)

const (
	GetPlayerInventory = "/player/inventory"
	ConsumeItem        = "/player/inventory/consume"
)

var _ core.Moduler = &Module{}
var emptyTemplate *template.Template = template.Must(template.New("").Parse("{{.Content}}"))

type Module struct {
	empty.EmptyModule
	playerInventory   *PlayerInventory
	consumeItemAction *consumeItem
}

func NewModule(itemMetaData []ItemMetadata) *Module {
	return &Module{
		empty.EmptyModule{},
		newPlayerInventory(itemMetaData),
		&consumeItem{},
	}
}

func (Module) Name() string {
	return "Inventory"
}

func (m Module) SaveLoaders() []core.SaveLoader {
	return []core.SaveLoader{
		m.playerInventory,
	}
}

func (m Module) Observers() []core.ObserveEvent {
	return []core.ObserveEvent{
		{m.playerInventory, meta.CreateGameEventName},
	}
}

func (m Module) DataHandlers() []core.DataHandlers {
	return []core.DataHandlers{
		{GetPlayerInventory, m.playerInventory.handleGetPlayerInventory},
	}
}

func (m Module) Actions() []core.Actions {
	return []core.Actions{
		{ConsumeItem, m.consumeItemAction, emptyTemplate},
	}
}

func (Module) ManualEntries() []core.ManualEntry {
	return getManualEntries()
}
