package inventory

import (
	"encoding/json"
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/errors"
)

var _ core.Acter = &consumeItem{}

type consumeItem struct{}

type consumeItemParams struct {
	Kind string `json:"kind"`
	Id   int    `json:"id"`
}

func (consumeItem) Act(vars core.RequestVariables, game core.Systemer) (map[string]interface{}, time.Duration, error) {
	if creature.GetReaperSystemOrPanic(game).IsPcDead() {
		return nil, 0, errors.NewStackErrorString("PC is dead")
	}
	invSys, err := GetPlayerInventorySystem(game)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	body, ok := vars.String("body")
	if !ok {
		return nil, 0, errors.NewStackErrorString("No body")
	}
	var params consumeItemParams
	if err = json.Unmarshal([]byte(body), &params); err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	if err := invSys.ConsumeItem(params.Kind, params.Id, game); err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	return map[string]interface{}{
		"Content": "",
	}, time.Second * 5, nil
}
