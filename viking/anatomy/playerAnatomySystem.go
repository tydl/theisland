package anatomy

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/meta"
)

const (
	PlayerAnatomyId         = "PlayerAnatomy"
	kindFirstAnatomyVersion = 0
)

var _ core.SaveLoader = &PlayerAnatomy{}
var _ core.Observer = &PlayerAnatomy{}

type PlayerAnatomy struct {
	player *Head
}

func (p *PlayerAnatomy) Id() string {
	return PlayerAnatomyId
}

func (p *PlayerAnatomy) Save() (data interface{}, version int, err error) {
	return p.player, kindFirstAnatomyVersion, nil
}

func (p *PlayerAnatomy) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	switch version {
	case kindFirstAnatomyVersion:
		p.player = &Head{}
		return loadFunction(p.player)
	default:
		return errors.NewStackErrorStringf("Unknown version %d", version)
	}
}

func (p *PlayerAnatomy) Observe(e core.Event, s core.Systemer) error {
	switch e.Name() {
	case meta.CreateGameEventName:
		event, ok := e.(*meta.CreateGameEvent)
		if !ok {
			return errors.NewStackErrorString("event not *meta.CreateGameEvent")
		}
		switch event.Sex {
		case "Male":
			p.player = HumanMaleAnatomy()
		case "Female":
			p.player = HumanFemaleAnatomy()
		default:
			return errors.NewStackErrorStringf("not Male nor Female: %s", event.Sex)
		}
	}
	return nil
}

func (p *PlayerAnatomy) Anatomy() *Head {
	return p.player
}

func GetPlayerAnatomySystem(s core.Systemer) (*PlayerAnatomy, error) {
	system, err := s.System(PlayerAnatomyId)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	casted, ok := system.(*PlayerAnatomy)
	if !ok {
		return nil, errors.NewStackErrorString("could not cast to *PlayerAnatomy")
	}
	return casted, nil
}
