package anatomy

import (
	"encoding/json"
	"strconv"

	"gitlab.com/tydl/theisland/viking/errors"
)

var _ json.Unmarshaler = &SerializableAnatomyMap{}
var _ json.Marshaler = SerializableAnatomyMap{}

type SerializableAnatomyMap map[int]*Head

func (s SerializableAnatomyMap) MarshalJSON() ([]byte, error) {
	objMap := make(map[string]*json.RawMessage, len(s))
	for k, v := range s {
		if b, err := json.Marshal(v); err != nil {
			return nil, errors.StackWrap(err)
		} else {
			addressable := json.RawMessage(b)
			objMap[strconv.Itoa(k)] = &addressable
		}
	}
	return json.Marshal(objMap)
}

func (s *SerializableAnatomyMap) UnmarshalJSON(data []byte) error {
	objMap := make(map[string]*json.RawMessage)
	if err := json.Unmarshal(data, &objMap); err != nil {
		return errors.StackWrap(err)
	}
	*s = make(map[int]*Head, len(objMap))
	for k, v := range objMap {
		var head Head
		if err := json.Unmarshal(*v, &head); err != nil {
			return errors.StackWrap(err)
		}
		intKey, err := strconv.Atoi(k)
		if err != nil {
			return errors.StackWrap(err)
		}
		(*s)[intKey] = &head
	}
	return nil
}
