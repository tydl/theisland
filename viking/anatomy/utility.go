package anatomy

func SetAllKind(head *Head, kind AnatomyKindId) {
	head.Core.Kind = kind
	head.Torso.Core.Kind = kind
	head.Torso.Hip.Core.Kind = kind
	for _, elem := range head.Torso.Breasts {
		elem.Core.Kind = kind
	}
	for _, elem := range head.Torso.LeftArm {
		elem.Core.Kind = kind
	}
	for _, elem := range head.Torso.RightArm {
		elem.Core.Kind = kind
	}
	for _, elem := range head.Torso.Stomach {
		elem.Core.Kind = kind
	}
	for _, elem := range head.Torso.Womb {
		elem.Core.Kind = kind
	}
	for _, elem := range head.Torso.Hip.Anus {
		elem.Core.Kind = kind
	}
	for _, elem := range head.Torso.Hip.LeftLeg {
		elem.Core.Kind = kind
	}
	for _, elem := range head.Torso.Hip.Penis {
		elem.Core.Kind = kind
	}
	for _, elem := range head.Torso.Hip.RightLeg {
		elem.Core.Kind = kind
	}
	for _, elem := range head.Torso.Hip.Vagina {
		elem.Core.Kind = kind
	}
}

func HumanMaleAnatomy() *Head {
	head := HumanCoreAnatomy()
	head.Torso.Hip.Penis = []*Penis{&Penis{}}
	head.Torso.Hip.Scrotum = []*Scrotum{
		&Scrotum{
			Balls: []*Balls{
				&Balls{},
			},
		},
	}
	SetAllKind(head, Human)
	return head
}

func HumanFemaleAnatomy() *Head {
	head := HumanCoreAnatomy()
	head.Torso.Womb = []*Womb{&Womb{}}
	head.Torso.Hip.Vagina = []*Vagina{&Vagina{}}
	SetAllKind(head, Human)
	return head
}

func HumanCoreAnatomy() *Head {
	head := &Head{}
	head.Torso = &Torso{}
	head.Torso.Hip = &Hip{}
	head.Torso.LeftArm = []*Arm{&Arm{}}
	head.Torso.RightArm = []*Arm{&Arm{}}
	head.Torso.Stomach = []*Stomach{&Stomach{}}
	head.Torso.Breasts = []*Breasts{
		&Breasts{
			Nipples: []*Nipples{
				&Nipples{},
			},
		},
	}
	head.Torso.Hip.LeftLeg = []*Leg{&Leg{}}
	head.Torso.Hip.RightLeg = []*Leg{&Leg{}}
	head.Torso.Hip.Anus = []*Anus{&Anus{}}
	return head
}
