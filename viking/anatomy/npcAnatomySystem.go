package anatomy

import (
	"sync"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
)

const (
	NpcAnatomyId              = "NpcAnatomy"
	npcInstanceAnatomyVersion = 0
)

var _ core.SaveLoader = &NpcAnatomy{}

type NpcAnatomy struct {
	instances map[int]*Head
	// Event syncing
	eventMutex    sync.Mutex
	genEvent      bool
	createEvent   bool
	instanceMutex sync.Mutex
	instanceIndex int
}

func newNpcAnatomy() *NpcAnatomy {
	return &NpcAnatomy{
		instances: make(map[int]*Head, 0),
	}
}

func (n *NpcAnatomy) Id() string {
	return NpcAnatomyId
}

func (n *NpcAnatomy) Save() (data interface{}, version int, err error) {
	addressable := SerializableAnatomyMap(n.instances)
	return addressable, npcInstanceAnatomyVersion, nil
}

func (n *NpcAnatomy) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	if version == npcInstanceAnatomyVersion {
		n.instances = make(map[int]*Head, 0)
		jsonSupported := SerializableAnatomyMap(n.instances)
		err := loadFunction(&jsonSupported)
		if err != nil {
			return errors.StackWrap(err)
		}
		n.instances = jsonSupported
	} else {
		return errors.NewStackErrorStringf("unknown version %d", version)
	}
	return nil
}

func (n *NpcAnatomy) SetNpcAnatomies(instances map[int]*Head) {
	n.instances = instances
}

func (n *NpcAnatomy) GetAnatomyForInstance(id int) *Head {
	head, ok := n.instances[id]
	if !ok {
		return nil
	}
	return head
}

func GetNpcAnatomySystem(s core.Systemer) (*NpcAnatomy, error) {
	system, err := s.System(NpcAnatomyId)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	casted, ok := system.(*NpcAnatomy)
	if !ok {
		return nil, errors.NewStackErrorString("could not cast to *NpcAnatomy")
	}
	return casted, nil
}
