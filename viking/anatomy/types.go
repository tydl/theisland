package anatomy

type AnatomyKindId string

const (
	Human AnatomyKindId = "Human"
)

type Core struct {
	Kind AnatomyKindId
}

type Head struct {
	Core
	Torso *Torso
}

type Torso struct {
	Core
	LeftArm  []*Arm
	RightArm []*Arm
	Hip      *Hip
	Breasts  []*Breasts
	Stomach  []*Stomach
	Womb     []*Womb
}

type Hip struct {
	Core
	LeftLeg  []*Leg
	RightLeg []*Leg
	Penis    []*Penis
	Scrotum  []*Scrotum
	Vagina   []*Vagina
	Anus     []*Anus
}

type Arm struct {
	Core
}

type Leg struct {
	Core
}

type Breasts struct {
	Core
	Nipples []*Nipples
}

type Nipples struct {
	Core
}

type Stomach struct {
	Core
}

type Womb struct {
	Core
}

type Penis struct {
	Core
}

type Scrotum struct {
	Core
	Balls []*Balls
}

type Balls struct {
	Core
}

type Vagina struct {
	Core
}

type Anus struct {
	Core
}
