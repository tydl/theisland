package anatomy

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/empty"
	"gitlab.com/tydl/theisland/viking/meta"
)

var _ core.Moduler = &Module{}

type Module struct {
	empty.EmptyModule
	playerAnatomy    *PlayerAnatomy
	npcAnatomySystem *NpcAnatomy
}

func NewModule() *Module {
	return &Module{
		empty.EmptyModule{},
		&PlayerAnatomy{},
		newNpcAnatomy(),
	}
}

func (Module) Name() string {
	return "Anatomy"
}

func (m Module) SaveLoaders() []core.SaveLoader {
	return []core.SaveLoader{
		m.playerAnatomy,
		m.npcAnatomySystem,
	}
}

func (m Module) Observers() []core.ObserveEvent {
	return []core.ObserveEvent{
		{m.playerAnatomy, meta.CreateGameEventName},
	}
}
