package crafting

import (
	"html/template"
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/empty"
	"gitlab.com/tydl/theisland/viking/inventory"
)

const (
	CraftItem = "/player/craft"
)

var craftItemActionTemplate *template.Template = template.Must(template.New("").Parse(""))

var _ core.Moduler = &Module{}

type CraftRecipe struct {
	Id        int
	CraftTime time.Duration
	Consumed  []RecipeItem
	Produced  []inventory.InventoryItemQuantity
}

type RecipeItem struct {
	Kind     string
	Id       int
	Quantity int
}

type Module struct {
	empty.EmptyModule
	recipeSystem *RecipeSystem
}

func NewModule(recipes []CraftRecipe) *Module {
	return &Module{
		empty.EmptyModule{},
		newRecipeSystem(recipes),
	}
}

func (Module) Name() string {
	return "Crafting"
}

func (m Module) SaveLoaders() []core.SaveLoader {
	return []core.SaveLoader{
		m.recipeSystem,
	}
}

func (m *Module) Actions() []core.Actions {
	return []core.Actions{
		{CraftItem, &craftItemAction{m.recipeSystem.recipes}, craftItemActionTemplate},
	}
}

func (Module) ManualEntries() []core.ManualEntry {
	return getManualEntries()
}
