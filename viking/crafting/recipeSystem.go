package crafting

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/inventory"
)

const (
	RecipeSystemId       = "RecipeSystem"
	nothingToSaveVersion = 0
)

var _ core.SaveLoader = &RecipeSystem{}

type RecipeSystem struct {
	recipes []CraftRecipe
}

func newRecipeSystem(recipes []CraftRecipe) *RecipeSystem {
	return &RecipeSystem{recipes}
}

func (r *RecipeSystem) Id() string {
	return RecipeSystemId
}

func (r *RecipeSystem) Save() (data interface{}, version int, err error) {
	return nil, nothingToSaveVersion, nil
}

func (r *RecipeSystem) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	return nil
}

func (r *RecipeSystem) Recipes() []CraftRecipe {
	return r.recipes
}

func (r *RecipeSystem) GetAvailableRecipes(s core.Systemer) ([]CraftRecipe, error) {
	invSys, err := inventory.GetPlayerInventorySystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	var toReturn []CraftRecipe
	for _, r := range r.recipes {
		if HasResourcesForRecipe(invSys, r) {
			toReturn = append(toReturn, r)
		}
	}
	return toReturn, nil
}

func GetRecipeSystem(s core.Systemer) (*RecipeSystem, error) {
	system, err := s.System(RecipeSystemId)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	casted, ok := system.(*RecipeSystem)
	if !ok {
		return nil, errors.NewStackErrorString("could not cast to *RecipeSystem")
	}
	return casted, nil
}
