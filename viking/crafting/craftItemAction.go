package crafting

import (
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/inventory"
)

type craftItemAction struct {
	recipes []CraftRecipe
}

func (c craftItemAction) Act(vars core.RequestVariables, s core.Systemer) (map[string]interface{}, time.Duration, error) {
	if creature.GetReaperSystemOrPanic(s).IsPcDead() {
		return nil, 0, errors.NewStackErrorString("PC is dead")
	}
	recipeId := 0
	if tempRecipeId, present := vars.Int("recipe"); present {
		recipeId = tempRecipeId
	} else {
		return nil, 0, errors.NewStackErrorString("No recipe parameter present")
	}
	var recipeToCraft CraftRecipe
	for _, recipe := range c.recipes {
		if recipe.Id == recipeId {
			recipeToCraft = recipe
		}
	}

	invSys, err := inventory.GetPlayerInventorySystem(s)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	if !HasResourcesForRecipe(invSys, recipeToCraft) {
		return nil, 0, errors.NewStackErrorString("Not enough resources to craft")
	}
	for _, required := range recipeToCraft.Consumed {
		invSys.RemoveInventoryItemsForKindId(required.Kind, required.Id, required.Quantity)
	}
	for _, produced := range recipeToCraft.Produced {
		copy := produced.Item.Copy()
		invSys.AddQuantity(copy, produced.Quantity)
	}
	return nil, recipeToCraft.CraftTime, nil
}

func HasResourcesForRecipe(invSys *inventory.PlayerInventory, recipeToCraft CraftRecipe) bool {
	for _, required := range recipeToCraft.Consumed {
		if item := invSys.GetInventoryItemsForKindId(required.Kind, required.Id); item != nil {
			if item.Quantity < required.Quantity {
				return false
			}
		} else {
			return false
		}
	}
	return true
}
