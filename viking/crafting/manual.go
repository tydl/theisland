package crafting

import "gitlab.com/tydl/theisland/viking/core"

func getManualEntries() []core.ManualEntry {
	return []core.ManualEntry{
		{
			Title: "Crafting",
			Text: "To craft an item, simply collect the prerequisite ingredients. These " +
				"are usually available through resource nodes or other crafting recipes. Then, " +
				"the inventory screen will have a button available to craft the resulting item. " +
				"Note that crafting can sometimes take a significant amount of time.",
		},
		{
			Title: "Recipe",
			Text: "Recipes are always unlocked for the PC. They are discoverable by " +
				"collecting enough prerequisite items in the world to craft the recipe. " +
				"The recipe will then be displayed in the inventory screen. A recipe always " +
				"requires at least one item, but can require multiple quantities of multiple " +
				"items. Furthermore, a recipe can produce multiple items or different kinds " +
				"of items. A recipe has a time to craft associated with it, which can be " +
				"significant.",
		},
	}
}
