package resources

import (
	"math"
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/mapgen"
)

const (
	lowPeakLogNormalMean    = 0
	lowPeakLogNormalStdDev  = 1.4
	lowPeakApproxMaximum    = 0.75927 // Occurs at roughly 14.0858%
	medPeakLogNormalMean    = 0
	medPeakLogNormalStdDev  = 0.8
	medPeakApproxMaximum    = 0.686743 // Occurs at roughly 52.7292%
	highPeakLogNormalMean   = 0
	highPeakLogNormalStdDev = 0.5
	highPeakApproxMaximum   = 0.904123 // Occurs at roughly 77.8801%
)

type GrowthFactorType int

const (
	Low  GrowthFactorType = 0
	Med  GrowthFactorType = 1
	High GrowthFactorType = 2
)

type ResourceId string
type ResourceNodeId string

type ResourceNodeFactory interface {
	IdSpawned() ResourceNodeId
	Text() Text
	NumberSpawnsInBiome(mapgen.Biome) int
	BaseHarvest() BaseHarvest
	TickInterval() time.Duration
	BaseQuantityGrowth() map[ResourceId]QuantityGrowth
	InitialQuantity(ResourceId) BoundedQuantity
	OnHarvested() map[string]func(s core.Systemer) error
}

type Text struct {
	Name           string
	DescriptionIds []string
}

type BaseHarvest struct { // TODO: Multiple actions, action tree
	Id               string
	ResourceQuantity map[ResourceId]HarvestReapItemPrototype
	Time             time.Duration
}

type HarvestReapItemPrototype struct {
	Quantity      int
	Id            int
	Name          string
	DescriptionId string
	Metadata      map[string]interface{}
}

type QuantityGrowth struct {
	Resource ResourceId
	Quantity int
	Growth   GrowthFactorType
}

type BoundedQuantity struct {
	Current int
	Maximum int
}

type ResourceNodeData struct {
	Instance        int
	Id              ResourceNodeId
	Text            Text
	X               int
	Y               int
	ResourcesInNode map[ResourceId]*BoundedQuantity
	Tick            *Tick
	Harvest         BaseHarvest
}

type Tick struct {
	LastTicked time.Time
	Duration   time.Duration
	Quantity   map[ResourceId]int
	Kind       map[ResourceId]GrowthFactorType
}

func (r *ResourceNodeData) IsEmpty() bool {
	for _, quantities := range r.ResourcesInNode {
		if quantities.Current > 0 {
			return false
		}
	}
	return true
}

func (r *ResourceNodeData) applyResourceGrowth() {
	for res, boundedQty := range r.ResourcesInNode {
		tickQty := r.Tick.Quantity[res]
		kind := r.Tick.Kind[res]
		growthFactor := applyGrowthFactorType(boundedQty.Maximum, boundedQty.Current, kind)
		effectiveGrowth := int(math.Max(1, math.Floor(float64(tickQty)*growthFactor)))
		r.ResourcesInNode[res].Current += effectiveGrowth
		if r.ResourcesInNode[res].Current > r.ResourcesInNode[res].Maximum {
			r.ResourcesInNode[res].Current = r.ResourcesInNode[res].Maximum
		}
	}
}

func applyGrowthFactorType(max, current int, kind GrowthFactorType) float64 {
	switch kind {
	case Low:
		return lowPeakLogNormalFrac(max, current)
	case Med:
		return medPeakLogNormalFrac(max, current)
	case High:
		return highPeakLogNormalFrac(max, current)
	default:
		return 0
	}
}

func lowPeakLogNormalFrac(max, current int) float64 {
	return logNormalPdf(max, current, lowPeakLogNormalMean, lowPeakLogNormalStdDev) / lowPeakApproxMaximum
}

func medPeakLogNormalFrac(max, current int) float64 {
	return logNormalPdf(max, current, medPeakLogNormalMean, medPeakLogNormalStdDev) / medPeakApproxMaximum
}

func highPeakLogNormalFrac(max, current int) float64 {
	return logNormalPdf(max, current, highPeakLogNormalMean, highPeakLogNormalStdDev) / highPeakApproxMaximum
}

// logNormalPdf fraction based on current and maximum quantity fraction.
func logNormalPdf(max, current int, mean, stdDev float64) float64 {
	frac := float64(current) / float64(max)
	numerator := -math.Pow(math.Log(frac)-mean, 2)
	denominator := 2 * stdDev * stdDev
	coeff := 1 / (frac * stdDev * math.Sqrt(2*math.Pi))
	return coeff * math.Exp(numerator/denominator)
}

type sortedResourceNodeData []*ResourceNodeData

func (s sortedResourceNodeData) Len() int {
	return len(s)
}

func (s sortedResourceNodeData) Less(i, j int) bool {
	return s[i].X < s[j].X || (s[i].X == s[j].X && s[i].Y < s[j].Y)
}

func (s sortedResourceNodeData) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
