package resources

import "gitlab.com/tydl/theisland/viking/core"

func getManualEntries() []core.ManualEntry {
	return []core.ManualEntry{
		{
			Title: "Resource Node",
			Text: "A resource node is the generic name for a location in the world that " +
				"can provide a natural resource to the PC. They have a finitie pool to be " +
				"harvested at any given time, but naturally regenerate slowly over " +
				"time. Resource nodes regenerate their pool of resources every resource tick. " +
				"Some resource nodes are able to replenish themselves very quickly when nearly " +
				"exhausted. Others should not have too many resources removed or else " +
				"it may take quite a while to regenerate. All resource nodes are renewable.",
		},
		{
			Title: "Resource Tick",
			Text: "Different resource nodes regenerate their pool of resources at different " +
				"rates. Some tick faster than others. Additionally, the amount gained each " +
				"tick is dependent on the amount of resources in their pool. Some resource " +
				"nodes are able to regenerate quickly despite having a drained pool of resources, " +
				"while others should not be tapped too hard.",
		},
	}
}
