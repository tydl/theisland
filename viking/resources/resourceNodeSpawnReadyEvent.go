package resources

import "gitlab.com/tydl/theisland/viking/core"

const (
	ResourceNodeSpawnReadyName = "ResourceNodeSpawnReady"
)

var _ core.Event = &ResourceNodeSpawnReady{}

type ResourceNodeSpawnReady struct{}

func (ResourceNodeSpawnReady) Name() string {
	return ResourceNodeSpawnReadyName
}
