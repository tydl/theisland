package resources

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
)

const (
	AddResourceNodeSystemId = "AddResourceNodeSystem"
)

var _ core.Observer = &AddResourceNodeSystem{}

type AddResourceNodeSystem struct {
	Factories []ResourceNodeFactory
}

func NewAddResourceNodeSystem(factories []ResourceNodeFactory) *AddResourceNodeSystem {
	return &AddResourceNodeSystem{
		factories,
	}
}

func (r *AddResourceNodeSystem) Id() string {
	return AddResourceNodeSystemId
}

func (r *AddResourceNodeSystem) Observe(e core.Event, s core.Systemer) error {
	switch e.Name() {
	case ResourceNodeSpawnReadyName:
		return r.doSpawn(s)
	case core.LoadedGameEventName:
		return r.registerFactories(s)
	}
	return nil
}

func (r *AddResourceNodeSystem) registerFactories(s core.Systemer) error {
	resSys, err := GetResourceSystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	for _, factory := range r.Factories {
		resSys.RegisterFactoryOnLoad(factory)
	}
	return nil
}

func (r *AddResourceNodeSystem) doSpawn(s core.Systemer) error {
	resSys, err := GetResourceSystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	for _, factory := range r.Factories {
		if err := resSys.SpawnResources(factory, s); err != nil {
			return errors.StackWrap(err)
		}
	}
	return nil
}
