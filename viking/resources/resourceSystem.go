package resources

import (
	"math/rand"
	"sort"
	"sync"
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/daynight"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/mapgen"
	"gitlab.com/tydl/theisland/viking/meta"
	"gitlab.com/tydl/theisland/viking/world"
)

const (
	resourceNodeDataBuffer = 1000
	consecutiveAttemptsMax = 500
	ResourceSystemId       = "ResourceSystem"
	nodeInstancesVersion   = 0
)

var _ core.SaveLoader = &ResourceSystem{}
var _ core.Observer = &ResourceSystem{}
var _ core.TimePasser = &ResourceSystem{}

type ResourceSystem struct {
	onHarvestedMap map[string]func(s core.Systemer) error
	nodeInstances  []*ResourceNodeData // TODO: map instead
	// Only needed for creating a game -- not loading one
	rng  *rand.Rand
	xMax int
	yMax int
	// Event syncing
	eventMutex    sync.Mutex
	genEvent      bool
	createEvent   bool
	instanceMutex sync.Mutex
	instanceIndex int
}

func (r *ResourceSystem) setFlagSafely(toSet *bool, b bool) {
	r.eventMutex.Lock()
	defer r.eventMutex.Unlock()
	*toSet = b
}

func (r *ResourceSystem) allFlagsTrue() bool {
	r.eventMutex.Lock()
	defer r.eventMutex.Unlock()
	return r.genEvent && r.createEvent
}

func (r *ResourceSystem) Id() string {
	return ResourceSystemId
}

func (r *ResourceSystem) Save() (data interface{}, version int, err error) {
	return r.nodeInstances, nodeInstancesVersion, nil
}

func (r *ResourceSystem) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	if version == nodeInstancesVersion {
		err := loadFunction(&r.nodeInstances)
		if err != nil {
			return errors.StackWrap(err)
		}
		sort.Sort(sortedResourceNodeData(r.nodeInstances))
	} else {
		return errors.NewStackErrorStringf("unknown version %d", version)
	}
	return nil
}

func (r *ResourceSystem) Observe(e core.Event, s core.Systemer) error {
	switch e.Name() {
	case world.GeneratedWorldEventName:
		event, ok := e.(*world.GeneratedWorldEvent)
		if !ok {
			return errors.NewStackErrorString("event not *meta.CreateGameEvent")
		}
		r.xMax = event.XMax
		r.yMax = event.YMax
		r.setFlagSafely(&r.genEvent, true)
	case meta.CreateGameEventName:
		event, ok := e.(*meta.CreateGameEvent)
		if !ok {
			return errors.NewStackErrorString("event not *meta.CreateGameEvent")
		}
		r.rng = rand.New(rand.NewSource(event.Seed))
		r.setFlagSafely(&r.createEvent, true)
	}
	if r.allFlagsTrue() {
		r.onHarvestedMap = nil
		eventer, err := core.GetAddEventer(s)
		if err != nil {
			return errors.StackWrap(err)
		}
		eventer.AddEvent(&ResourceNodeSpawnReady{})
	}
	return nil
}

func (r *ResourceSystem) TimePass(duration time.Duration, systemer core.Systemer) error {
	timeKeeper, err := daynight.GetDayNightSystem(systemer)
	if err != nil {
		return errors.StackWrap(err)
	}
	now := timeKeeper.Now()
	for _, instance := range r.nodeInstances {
		nextTick := instance.Tick.LastTicked.Add(instance.Tick.Duration)
		if now.After(nextTick) || now.Equal(nextTick) {
			instance.applyResourceGrowth()
		}
	}
	return nil
}

func (r *ResourceSystem) RegisterFactoryOnLoad(factory ResourceNodeFactory) {
	if r.onHarvestedMap == nil {
		r.onHarvestedMap = make(map[string]func(core.Systemer) error)
	}
	for k, v := range factory.OnHarvested() {
		r.onHarvestedMap[k] = v
	}
}

// SpawnResources spawns resources for a given node factory. It can only be
// called after creating a new game and after a world has been generated.
//
// TODO: Remove restrictions
func (r *ResourceSystem) SpawnResources(factory ResourceNodeFactory, systemer core.Systemer) error {
	if !r.allFlagsTrue() {
		return errors.NewStackErrorString("SpawnResources called when not all setup events done")
	}

	// Only one can spawn at a time
	r.instanceMutex.Lock()
	defer r.instanceMutex.Unlock()
	r.createEvent = false
	r.genEvent = false

	timeKeeper, err := daynight.GetDayNightSystem(systemer)
	if err != nil {
		return errors.StackWrap(err)
	}
	mapSys, err := world.GetMapSystem(systemer)
	if err != nil {
		return errors.StackWrap(err)
	}
	r.nodeInstances = make([]*ResourceNodeData, 0, resourceNodeDataBuffer)
	occurrences := make(map[string]struct {
		Count  int
		Target int
	})
	for _, biome := range mapgen.AllBiomes {
		occurrences[biome.Name()] = struct {
			Count  int
			Target int
		}{
			0,
			factory.NumberSpawnsInBiome(biome),
		}
	}
	if r.onHarvestedMap == nil {
		r.onHarvestedMap = make(map[string]func(core.Systemer) error)
	}
	for k, v := range factory.OnHarvested() {
		r.onHarvestedMap[k] = v
	}
	currAttempts := 0
	for currAttempts < consecutiveAttemptsMax {
		x, y := r.rng.Intn(r.xMax), r.rng.Intn(r.yMax)
		biome := mapSys.GetBiome(x, y)
		targetData, ok := occurrences[biome.Name()]
		if !ok || targetData.Target == targetData.Count {
			currAttempts++
			continue
		}
		targetData.Count++
		occurrences[biome.Name()] = targetData

		instanceId := r.instanceIndex
		r.instanceIndex++

		instance := &ResourceNodeData{
			Instance: instanceId,
			Id:       factory.IdSpawned(),
			Text:     factory.Text(),
			X:        x,
			Y:        y,
			Harvest:  factory.BaseHarvest(),
		}
		instance.Tick = &Tick{
			LastTicked: timeKeeper.Now(), // TODO: Can vary this
			Duration:   factory.TickInterval(),
			Quantity:   make(map[ResourceId]int),
			Kind:       make(map[ResourceId]GrowthFactorType),
		}
		instance.ResourcesInNode = make(map[ResourceId]*BoundedQuantity)
		for res, growInfo := range factory.BaseQuantityGrowth() {
			temp := factory.InitialQuantity(res)
			instance.ResourcesInNode[res] = &temp
			instance.Tick.Quantity[res] = growInfo.Quantity
			instance.Tick.Kind[res] = growInfo.Growth
		}
		r.nodeInstances = append(r.nodeInstances, instance)
		currAttempts = 0
		bDone := true
		for _, val := range occurrences {
			if val.Count != val.Target {
				bDone = false
				break
			}
		}
		if bDone {
			break
		}
	}
	sort.Sort(sortedResourceNodeData(r.nodeInstances))
	return nil
}

func (r ResourceSystem) OnHarvested(s core.Systemer, id string) error {
	return r.onHarvestedMap[id](s)
}

func (r *ResourceSystem) GetNodesAt(x, y int) []ResourceNodeData {
	var toReturn []ResourceNodeData
	for _, nodeData := range r.nodeInstances {
		if nodeData.X == x && nodeData.Y == y {
			toReturn = append(toReturn, *nodeData)
		}
	}
	return toReturn
}

func (r *ResourceSystem) GetNodeInstance(instanceId int) (ResourceNodeData, error) {
	for _, nodeData := range r.nodeInstances {
		if nodeData.Instance == instanceId {
			return *nodeData, nil
		}
	}
	return ResourceNodeData{}, errors.NewStackErrorStringf("No resource node found for id %s", instanceId)
}

func (r *ResourceSystem) Update(data ResourceNodeData) error {
	err := errors.NewStackErrorStringf("No resource node instance with id %d", data.Instance)
	for i, nodeData := range r.nodeInstances {
		if nodeData.Instance == data.Instance {
			r.nodeInstances = append(r.nodeInstances[:i], r.nodeInstances[i+1:]...)
			r.nodeInstances = append(r.nodeInstances, &data)
			sort.Sort(sortedResourceNodeData(r.nodeInstances))
			return nil
		}
	}
	return err
}

func GetResourceSystem(s core.Systemer) (*ResourceSystem, error) {
	system, err := s.System(ResourceSystemId)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	casted, ok := system.(*ResourceSystem)
	if !ok {
		return nil, errors.NewStackErrorString("could not cast to *ResourceSystem")
	}
	return casted, nil
}
