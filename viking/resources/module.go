package resources

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/empty"
	"gitlab.com/tydl/theisland/viking/meta"
	"gitlab.com/tydl/theisland/viking/world"
)

var _ core.Moduler = &Module{}

type Module struct {
	empty.EmptyModule
	resSys           *ResourceSystem
	resNodeFactories *AddResourceNodeSystem
}

func NewModule(resNodeFactory *AddResourceNodeSystem) *Module {
	return &Module{
		empty.EmptyModule{},
		&ResourceSystem{},
		resNodeFactory,
	}
}

func (Module) Name() string {
	return "Resources"
}

func (m Module) SaveLoaders() []core.SaveLoader {
	return []core.SaveLoader{
		m.resSys,
	}
}

func (m Module) Observers() []core.ObserveEvent {
	return []core.ObserveEvent{
		{m.resSys, world.GeneratedWorldEventName},
		{m.resSys, meta.CreateGameEventName},
		{m.resNodeFactories, ResourceNodeSpawnReadyName},
		{m.resNodeFactories, core.LoadedGameEventName},
	}
}

func (m Module) TimePassers() []core.TimePasser {
	return []core.TimePasser{m.resSys}
}

func (Module) ManualEntries() []core.ManualEntry {
	return getManualEntries()
}
