package procedural

import (
	"github.com/ojrac/opensimplex-go"
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
)

const (
	ProceduralSystemManagerId = "proceduralSystemManager"
	versionMapSeedKinds       = 0
)

type Noise interface {
	Eval2(x, y float64) float64
}

var _ core.SaveLoader = &ProceduralSystemManager{}

type ProceduralSystemManager struct {
	systems map[string]*proceduralSystem
}

func NewProceduralSystemManager() *ProceduralSystemManager {
	return &ProceduralSystemManager{
		systems: make(map[string]*proceduralSystem),
	}
}

func (p *ProceduralSystemManager) UpsertSystem(id string, seeds []int64, delta float64) error {
	if system, err := newProceduralSystem(seeds, delta); err == nil {
		p.systems[id] = system
		return nil
	} else {
		return err
	}
}

func (p *ProceduralSystemManager) GetNoise(id string, x, y float64) (float64, error) {
	if s, ok := p.systems[id]; ok {
		value := 0.0
		freq := 1.0
		amp := 1.0
		cumAmp := 0.0
		for _, gen := range s.generator {
			value += (gen.Eval2(x*freq*s.delta, y*freq*s.delta)/2 + 0.5) * amp
			freq /= 2
			cumAmp += amp
			amp *= 2
		}
		return value / cumAmp, nil
	} else {
		return 0, errors.NewStackErrorStringf("No procedural system with id %s", id)
	}
}

func (p *ProceduralSystemManager) Id() string {
	return ProceduralSystemManagerId
}

func (p *ProceduralSystemManager) Save() (data interface{}, version int, err error) {
	saveMap := make(map[string]saveV0, len(p.systems))
	for k, v := range p.systems {
		saveMap[k] = saveV0{v.seeds, v.delta}
	}
	return saveMap, versionMapSeedKinds, nil
}

func (p *ProceduralSystemManager) Load(loadFunction func(interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	switch version {
	case versionMapSeedKinds:
		loadMap := make(map[string]saveV0)
		if err := loadFunction(&loadMap); err != nil {
			return errors.StackWrap(err)
		}
		p.systems = make(map[string]*proceduralSystem, len(loadMap))
		for k, v := range loadMap {
			gen, err := newProceduralSystem(v.Seeds, v.Delta)
			if err != nil {
				return errors.StackWrap(err)
			}
			p.systems[k] = gen
		}
	default:
		return errors.NewStackErrorStringf("unsupported version %d", version)
	}
	return nil
}

type proceduralSystem struct {
	seeds     []int64
	delta     float64
	generator []Noise
}

func newProceduralSystem(seeds []int64, delta float64) (*proceduralSystem, error) {
	if len(seeds) <= 0 {
		return nil, errors.NewStackErrorString("len(seeds)==0")
	} else {
		var generator []Noise
		for _, seed := range seeds {
			if gen, err := newGenerator(seed); err != nil {
				return nil, errors.StackWrap(err)
			} else {
				generator = append(generator, gen)
			}
		}
		return &proceduralSystem{
			seeds:     seeds,
			delta:     delta,
			generator: generator,
		}, nil
	}
}

func newGenerator(seed int64) (Noise, error) {
	return opensimplex.NewWithSeed(seed), nil
}

type saveV0 struct {
	Seeds []int64
	Delta float64
}
