package procedural

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/empty"
)

const (
	proceduralModuleName = "Procedural Generation"
)

var _ core.Moduler = &ProceduralModule{}

type ProceduralModule struct {
	empty.EmptyModule
	proceduralManager *ProceduralSystemManager
}

func NewProceduralModule() *ProceduralModule {
	return &ProceduralModule{
		empty.EmptyModule{},
		NewProceduralSystemManager(),
	}
}

func (p *ProceduralModule) Name() string {
	return proceduralModuleName
}

func (p *ProceduralModule) SaveLoaders() []core.SaveLoader {
	return []core.SaveLoader{p.proceduralManager}
}
