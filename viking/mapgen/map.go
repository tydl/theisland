package mapgen

import (
	"fmt"
	"image"
	"image/color"
	"math"
	"math/rand"
	"sort"

	"github.com/pzsz/voronoi"
	"github.com/pzsz/voronoi/utils"
)

const (
	landOrderingWeight  = 1
	waterOrderingWeight = 0.001
	riverAttemptLimit   = 1000
	nSteps              = 50
)

var centerColor = color.NRGBA{255, 0, 0, 255}
var edgeColor = color.NRGBA{0, 0, 0, 255}

type data struct {
	idx         int            // Edges and Cells - arbitrary but consistent sorts
	kind        MapSurfaceType // Edges and Cells
	moisture    float64        // Edges and Cells
	elevation   float64        // Edges and Cells
	biome       Biome          // Cells only
	downslope   *voronoi.Edge  // Edges only
	isRiver     bool           // Edges only
	riverVolume float64        // Edges only
}

type elevationDataSlice []*data

func (d elevationDataSlice) Len() int { return len(d) }
func (d elevationDataSlice) Less(i, j int) bool {
	return d[i].elevation < d[j].elevation || (d[i].elevation == d[j].elevation && d[i].idx < d[j].idx)
}
func (d elevationDataSlice) Swap(i, j int) { d[i], d[j] = d[j], d[i] }

type moistureDataSlice []*data

func (d moistureDataSlice) Len() int { return len(d) }
func (d moistureDataSlice) Less(i, j int) bool {
	return d[i].moisture < d[j].moisture || (d[i].moisture == d[j].moisture && d[i].idx < d[j].idx)
}
func (d moistureDataSlice) Swap(i, j int) { d[i], d[j] = d[j], d[i] }

type GeneratedMap struct {
	diagram      *voronoi.Diagram
	traceLevel   InfoLevel
	corners      map[*voronoi.Edge]*data
	cells        map[*voronoi.Cell]*data
	dataAtCached *voronoi.Cell
}

func (g *GeneratedMap) BiomeAt(x, y float64) Biome {
	if data := g.dataAt(x, y); data != nil {
		return data.biome
	}
	return nil
}

func (g *GeneratedMap) ElevationAt(x, y float64) float64 {
	if data := g.dataAt(x, y); data != nil {
		return data.elevation
	}
	return -1
}

func (g *GeneratedMap) RiverSizeAt(x, y float64) float64 {
	var inCell *voronoi.Cell
	v := voronoi.Vertex{X: x, Y: y}
	for _, cell := range g.diagram.Cells {
		if utils.InsideCell(cell, v) {
			inCell = cell
			break
		}
	}
	for _, halfEdge := range inCell.Halfedges {
		if g.corners[halfEdge.Edge].isRiver {
			xt := halfEdge.Edge.Va.X
			yt := halfEdge.Edge.Va.Y
			deltaX := (halfEdge.Edge.Vb.X - xt) / (nSteps)
			deltaY := (halfEdge.Edge.Vb.Y - yt) / (nSteps)
			for i := 0; i < nSteps; i++ {
				if math.Floor(xt) < x && x < math.Ceil(xt) && math.Floor(yt) < y && y < math.Ceil(yt) {
					return math.Sqrt(g.corners[halfEdge.Edge].riverVolume)
				}
				x += deltaX
				y += deltaY
			}
		}
	}
	return 0
}

func (g *GeneratedMap) dataAt(x, y float64) *data {
	v := voronoi.Vertex{X: x, Y: y}
	if g.dataAtCached != nil && utils.InsideCell(g.dataAtCached, v) {
		return g.cells[g.dataAtCached]
	}
	for _, cell := range g.diagram.Cells {
		if utils.InsideCell(cell, v) {
			g.dataAtCached = cell
			return g.cells[cell]
		}
	}
	return nil
}

func round(x float64) int {
	return int(math.Floor(x + 0.5))
}

func (g *GeneratedMap) DrawDiagram(img *image.NRGBA) {
	for _, cell := range g.diagram.Cells {
		img.SetNRGBA(round(cell.Site.X), round(cell.Site.Y), centerColor)
		for _, edge := range g.diagram.Edges {
			x := edge.Va.X
			y := edge.Va.Y
			deltaX := (edge.Vb.X - x) / nSteps
			deltaY := (edge.Vb.Y - y) / nSteps
			for i := 0; i < nSteps; i++ {
				img.SetNRGBA(round(x), round(y), edgeColor)
				x += deltaX
				y += deltaY
			}
		}
	}
}

func (g *GeneratedMap) DrawRivers(img *image.NRGBA, maxY, scaleFactor int, c color.NRGBA) {
	for _, edge := range g.diagram.Edges {
		if g.corners[edge].isRiver {
			fScaleFactor := float64(scaleFactor)
			x := edge.Va.X * fScaleFactor
			y := edge.Va.Y * fScaleFactor
			deltaX := (edge.Vb.X*fScaleFactor - x) / (nSteps * fScaleFactor)
			deltaY := (edge.Vb.Y*fScaleFactor - y) / (nSteps * fScaleFactor)
			for i := 0; i < nSteps*scaleFactor; i++ {
				img.SetNRGBA(round(x), maxY-round(y)-1, c)
				x += deltaX
				y += deltaY
			}
		}
	}
}

func (g *GeneratedMap) DrawDownslopes(img *image.NRGBA, maxY, scaleFactor int) {
	for _, edge := range g.diagram.Edges {
		if g.corners[edge].downslope != edge {
			ds := g.corners[edge].downslope
			fScaleFactor := float64(scaleFactor)
			x := edge.Va.X * fScaleFactor
			y := edge.Va.Y * fScaleFactor
			deltaX := (ds.Va.X*fScaleFactor - x) / (nSteps * fScaleFactor)
			deltaY := (ds.Va.Y*fScaleFactor - y) / (nSteps * fScaleFactor)
			for i := 0; i < nSteps*scaleFactor; i++ {
				colorVal := uint8((float64(i*nSteps*scaleFactor) / float64(nSteps*scaleFactor)) * 255)
				img.SetNRGBA(round(x), maxY-round(y)-1, color.NRGBA{colorVal, colorVal, colorVal, 255})
				x += deltaX
				y += deltaY
			}
		}
	}
}

func (g *GeneratedMap) DrawCoastPoints(img *image.NRGBA, maxY, scaleFactor int) {
	for _, edge := range g.diagram.Edges {
		if g.corners[edge].kind == Coast {
			fScaleFactor := float64(scaleFactor)
			x := edge.Va.X * fScaleFactor
			y := edge.Va.Y * fScaleFactor
			img.SetNRGBA(round(x), maxY-round(y)-1, centerColor)
		}
	}
}

func (g *GeneratedMap) traceDump() {
	g.traceln(Info, "Edge Data:")
	for _, edge := range g.corners {
		g.tracef(Info, "%v\n", edge)
	}
	g.traceln(Info, "Cell Data:")
	for _, cell := range g.cells {
		g.tracef(Info, "%v %v\n", cell, cell.biome)
	}
}

func newGeneratedMap(d *voronoi.Diagram, g GenerationPipeline, center voronoi.Vertex) *GeneratedMap {
	gen := &GeneratedMap{
		d,
		g.Trace,
		make(map[*voronoi.Edge]*data),
		make(map[*voronoi.Cell]*data),
		nil,
	}
	for idx, edge := range d.Edges {
		gen.corners[edge] = &data{idx, Water, -1, -1, nil, nil, false, 0}
	}
	for idx, cell := range d.Cells {
		gen.cells[cell] = &data{idx, Water, -1, -1, nil, nil, false, 0}
	}
	gen.traceln(Info, "Determining water...")
	gen.waterIterator(g.IsWater)
	gen.traceln(Info, "Applying elevations...")
	gen.applyElevations()
	gen.traceln(Info, "Determining kind of water...")
	gen.determineKindWater(g.FractionForLakes)
	if g.RemoveNotConnectedToMiddle {
		gen.traceln(Info, "Removing land not connected to middle...")
		gen.removeNotConnectedToMiddle(center)
	}
	gen.traceln(Info, "Determining coast...")
	gen.determineCoast()
	gen.traceln(Info, "Determining coast in corners...")
	gen.determineCornersWaterCoast()
	gen.traceln(Info, "Normalizing elevations...")
	gen.normalizeCorners(sortByElevation, parabolicInverseCDF, setElevation)
	gen.traceln(Info, "Setting oceans and coast as low elevation...")
	gen.setOceanCoastCornersAsZeroElevation()
	gen.traceln(Info, "Setting cell elevations...")
	gen.setCellsAsAverage(getElevation, setElevation)
	gen.traceln(Info, "Calculating downslopes...")
	gen.calculateDownslopes()
	gen.traceln(Info, "Creating rivers...")
	gen.createRivers(g.NumberRiverSources, g.RiverRandSeed, g.RiverVolumePerEdge, g.IsRiverPoint)
	gen.traceln(Info, "Calculating moisture in corners...")
	gen.calculateCornerMoisture(g.FreshwaterMoistureSpreadRate, g.SaltwaterMoisture, g.RiverMoistureVolume, g.FreshwaterMoisture)
	gen.traceln(Info, "Normalizing moisture...")
	gen.normalizeCorners(sortByMoisture, linearInverseCDF, setMoisture)
	gen.traceln(Info, "Setting cell moisture...")
	gen.setCellsAsAverage(getMoisture, setMoisture)
	gen.traceln(Info, "Determining biomes...")
	gen.determineBiomes(g.DetermineBiome)
	return gen
}

func (g *GeneratedMap) waterIterator(isWater IsWater) {
	for _, edge := range g.diagram.Edges {
		data := g.corners[edge]
		if isWater(edge.Va.Vertex) {
			data.kind = Water
		} else {
			data.kind = Land
		}
	}
}

func (g *GeneratedMap) applyElevations() {
	// Mark border cells as zero
	g.traceln(Fine, "Marking border edges as zero elevation...")
	edgesToVisit := make([]*voronoi.Edge, 0, len(g.diagram.Edges))
	for _, cell := range g.diagram.Cells {
		edge := g.getBorderEdge(cell)
		if edge != nil {
			data := g.corners[edge]
			data.elevation = 0
			edgesToVisit = append(edgesToVisit, edge)
			g.traceln(Finest, "Found border edge...")
		}
	}
	// Traverse along edges: increase elevation weight from border so they can
	// be later ordered from smallest to largest and normalized based on their
	// ordering.
	for i := 0; i < len(edgesToVisit); i++ {
		g.tracef(Finest, "i=%d, len(edgesToVisit)=%d\n", i, len(edgesToVisit))
		edge := edgesToVisit[i]
		data := g.corners[edge]
		adjacentEdges := adjacentEdges(edge)
		for _, adjacentEdge := range adjacentEdges {
			adjacentData := g.corners[adjacentEdge]
			adjacentElevation := data.elevation
			adjacentElevation += waterOrderingWeight
			if adjacentData.kind == Land && data.kind == Land {
				adjacentElevation += landOrderingWeight
			}
			if adjacentData.elevation < 0 || adjacentElevation < adjacentData.elevation {
				adjacentData.elevation = adjacentElevation
				edgesToVisit = append(edgesToVisit, adjacentEdge)
			}
		}
	}
}

func (g *GeneratedMap) getBorderEdge(cell *voronoi.Cell) *voronoi.Edge {
	for _, halfEdge := range cell.Halfedges {
		// Border cells have RightCell as nil -- see createBorderEdge implementation
		if halfEdge.Edge.RightCell == nil {
			return halfEdge.Edge
		}
	}
	return nil
}

func adjacentEdges(e *voronoi.Edge) []*voronoi.Edge {
	adjacent := make([]*voronoi.Edge, 0, len(e.Va.Edges)+len(e.Vb.Edges)-2)
	for _, other := range e.Va.Edges {
		if other != e {
			adjacent = append(adjacent, other)
		}
	}
	for _, other := range e.Vb.Edges {
		if other != e {
			adjacent = append(adjacent, other)
		}
	}
	return adjacent
}

func (g *GeneratedMap) determineKindWater(fractionForLakes float64) {
	cellsToVisit := make([]*voronoi.Cell, 0, len(g.diagram.Cells))
	// Find all border cells and lake cells
	g.traceln(Fine, "Finding border and lake cells...")
	for _, cell := range g.diagram.Cells {
		edge := g.getBorderEdge(cell)
		if edge != nil { // Border = ocean
			g.cells[cell].kind = Ocean
			cellsToVisit = append(cellsToVisit, cell)
			g.traceln(Finest, "Found ocean...")
		} else { // Test if lake
			numPtsWater := 0
			for _, edge := range cell.Halfedges {
				data := g.corners[edge.Edge]
				if data.kind.IsWater() {
					numPtsWater++
				}
			}
			if float64(numPtsWater) >= float64(len(cell.Halfedges))*fractionForLakes {
				g.cells[cell].kind = Lake
				g.traceln(Finest, "Found lake...")
			} else {
				g.cells[cell].kind = Land
				g.traceln(Finest, "Found land...")
			}
		}
	}
	// Fill in the rest of the water cells adjacent to ocean as Ocean.
	g.traceln(Fine, "Filling in oceans...")
	for i := 0; i < len(cellsToVisit); i++ {
		g.tracef(Finest, "i=%d len(cellsToVisit)=%d\n", i, len(cellsToVisit))
		cell := cellsToVisit[i]
		adjacentCells := g.adjacentCells(cell)
		for _, adjacentCell := range adjacentCells {
			if g.cells[adjacentCell].kind.IsWater() && g.cells[adjacentCell].kind != Ocean {
				g.cells[adjacentCell].kind = Ocean
				cellsToVisit = append(cellsToVisit, adjacentCell)
			}
		}
	}
}

func (g *GeneratedMap) adjacentCells(c *voronoi.Cell) []*voronoi.Cell {
	adjacent := make([]*voronoi.Cell, 0, len(c.Halfedges))
	for _, edge := range c.Halfedges {
		if other := edge.Edge.GetOtherCell(c); other != nil {
			adjacent = append(adjacent, other)
		}
	}
	return adjacent
}

func (g *GeneratedMap) removeNotConnectedToMiddle(center voronoi.Vertex) {
	var centerCell *voronoi.Cell
	g.traceln(Fine, "Finding center cell...")
	for _, cell := range g.diagram.Cells {
		if utils.InsideCell(cell, center) {
			centerCell = cell
			break
		}
	}
	var savedCells []*voronoi.Cell
	toVisit := []*voronoi.Cell{centerCell}
	g.traceln(Fine, "Building saved cell list...")
	for i := 0; i < len(toVisit); i++ {
		visiting := toVisit[i]
		for _, edge := range visiting.Halfedges {
			otherCell := edge.Edge.GetOtherCell(visiting)
			if g.cells[otherCell].kind == Land {
				otherAlreadyVisited := false
				for j := 0; j < len(toVisit); j++ {
					if toVisit[j] == otherCell {
						otherAlreadyVisited = true
						break
					}
				}
				if !otherAlreadyVisited {
					toVisit = append(toVisit, otherCell)
				}
			}
		}
		savedCells = append(savedCells, visiting)
	}
	g.traceln(Fine, "Applying saved cell list...")
	for _, cell := range g.diagram.Cells {
		if g.cells[cell].kind != Land {
			continue
		}
		bFound := false
		for _, maybeSave := range savedCells {
			if cell == maybeSave {
				bFound = true
				break
			}
		}
		if !bFound {
			g.cells[cell].kind = Ocean
		}
	}
}

func (g *GeneratedMap) determineCoast() {
	for _, cell := range g.diagram.Cells {
		if g.cells[cell].kind == Land {
			hasWaterAdjacent := false
			adjacentCells := g.adjacentCells(cell)
			for _, adjacentCell := range adjacentCells {
				if g.cells[adjacentCell].kind == Ocean {
					hasWaterAdjacent = true
					break
				}
			}
			if hasWaterAdjacent {
				g.cells[cell].kind = Coast
			}
		}
	}
}

func (g *GeneratedMap) determineCornersWaterCoast() {
	for _, edge := range g.diagram.Edges {
		hasAdjacentLand := false
		hasAdjacentOcean := false
		hasAdjacentLake := false
		for _, adjacentEdge := range edge.Va.Edges {
			if adjacentEdge.LeftCell != nil {
				hasAdjacentLand = hasAdjacentLand || !g.cells[adjacentEdge.LeftCell].kind.IsWater()
				hasAdjacentOcean = hasAdjacentOcean || g.cells[adjacentEdge.LeftCell].kind == Ocean
				hasAdjacentLake = hasAdjacentLake || g.cells[adjacentEdge.LeftCell].kind == Lake
			}
			if adjacentEdge.RightCell != nil {
				hasAdjacentLand = hasAdjacentLand || !g.cells[adjacentEdge.RightCell].kind.IsWater()
				hasAdjacentOcean = hasAdjacentOcean || g.cells[adjacentEdge.RightCell].kind == Ocean
				hasAdjacentLake = hasAdjacentLake || g.cells[adjacentEdge.RightCell].kind == Lake
			}
		}
		if hasAdjacentLand && (hasAdjacentLake || hasAdjacentOcean) {
			g.corners[edge].kind = Coast
		} else if hasAdjacentLand {
			g.corners[edge].kind = Land
		} else if hasAdjacentOcean {
			g.corners[edge].kind = Ocean
		} else if hasAdjacentLake {
			g.corners[edge].kind = Lake
		}
	}
}

func (g *GeneratedMap) normalizeCorners(sortFn func([]*data), invCdfFn func(float64) float64, setValueFn func(*data, float64)) {
	allCorners := g.getAllCornerData()
	sortFn(allCorners)
	for idx, data := range allCorners {
		cdf := float64(idx) / float64(len(allCorners)-1)
		value := invCdfFn(cdf)
		setValueFn(data, value)
	}
}

func (g *GeneratedMap) getAllCornerData() []*data {
	toReturn := make([]*data, 0, len(g.corners))
	for _, corner := range g.corners {
		toReturn = append(toReturn, corner)
	}
	return toReturn
}

func sortByElevation(d []*data) {
	sort.Sort(elevationDataSlice(d))
}

// parabolicInverseCDF computes the inverse CDF given by the parabola
// 1 - (1-x)^2.
func parabolicInverseCDF(x float64) float64 {
	return 1 - math.Sqrt(1-x)
}

func setElevation(d *data, elev float64) {
	d.elevation = elev
}

func (g *GeneratedMap) setOceanCoastCornersAsZeroElevation() {
	setAllElevations(filterDataOnKind(g.getAllCornerData(), Coast, Ocean), 0)
}

func filterDataOnKind(d []*data, kinds ...MapSurfaceType) []*data {
	toReturn := make([]*data, 0, len(d))
	for _, data := range d {
		for _, kind := range kinds {
			if data.kind == kind {
				toReturn = append(toReturn, data)
				break
			}
		}
	}
	return toReturn
}

func setAllElevations(d []*data, elev float64) {
	for _, data := range d {
		setElevation(data, elev)
	}
}

func (g *GeneratedMap) setCellsAsAverage(getCornerValueFn func(*data) float64, setCellValueFn func(*data, float64)) {
	for _, cell := range g.diagram.Cells {
		value := 0.0
		for _, edge := range cell.Halfedges {
			value += getCornerValueFn(g.corners[edge.Edge])
		}
		setCellValueFn(g.cells[cell], value/float64(len(cell.Halfedges)))
	}
}

func getElevation(d *data) float64 {
	return d.elevation
}

func (g *GeneratedMap) calculateDownslopes() {
	for _, edge := range g.diagram.Edges {
		myDownslope := edge
		adjEdges := adjacentEdges(edge)
		for _, adjEdge := range adjEdges {
			if g.corners[adjEdge].elevation < g.corners[myDownslope].elevation {
				myDownslope = adjEdge
			}
		}
		g.corners[edge].downslope = myDownslope
	}
}

func (g *GeneratedMap) createRivers(nRiverSources int, riverRandSeed int64, riverVolumePerEdge float64, isRiverPoint IsRiverPoint) {
	riverCount := 0
	nTry := 0
	random := rand.New(rand.NewSource(riverRandSeed))
	for riverCount < nRiverSources && nTry < riverAttemptLimit {
		idx := random.Intn(len(g.diagram.Edges))
		edge := g.diagram.Edges[idx]
		data := g.corners[edge]
		g.tracef(Finest, "Potential river (%d of %d, try %d) elevation: %f\n", riverCount, nRiverSources, nTry, data.elevation)
		if !isRiverPoint(data.elevation) || data.isRiver {
			nTry++
			continue
		}
		currentData := data
		currentEdge := edge
		currentVolume := riverVolumePerEdge
		for currentData.kind != Coast && currentData.downslope != currentEdge {
			currentData.isRiver = true
			currentData.riverVolume = currentVolume
			currentEdge = currentData.downslope
			currentData = g.corners[currentEdge]
			currentVolume += riverVolumePerEdge
		}
		// Go to next point if edge is marked coast but we didn't actually reach the
		// water, which would be evidenced by the next edge being parallel to or out
		// into water. This fixes a bug where a downslops edge goes from a coast
		// point inland and gets marked as coast overall (triggering the river to
		// end one segment short).
		if currentData.kind == Coast && !g.cells[currentEdge.LeftCell].kind.IsWater() && !g.cells[currentEdge.RightCell].kind.IsWater() {
			currentData.isRiver = true
			currentData.riverVolume = currentVolume
		}
		riverCount++
		nTry = 0
	}
	if riverAttemptLimit == nTry {
		g.tracef(Fine, "Only created %d out of %d rivers...\n", riverCount, nRiverSources)
	} else {
		g.tracef(Fine, "Successfully created %d rivers...\n", riverCount)
	}
}

func (g *GeneratedMap) calculateCornerMoisture(freshwaterMoistureSpreadRate, saltwaterMoisture float64, riverFn RiverMoistureVolume, lakeFn FreshwaterMoisture) {
	// Initialize Freshwater
	toSpread := make([]*voronoi.Edge, 0)
	for _, edge := range g.diagram.Edges {
		if g.corners[edge].isRiver {
			g.corners[edge].moisture = riverFn(g.corners[edge].riverVolume)
			toSpread = append(toSpread, edge)
		} else if g.corners[edge].kind == Lake {
			g.corners[edge].moisture = lakeFn()
			toSpread = append(toSpread, edge)
		} else {
			g.corners[edge].moisture = 0
		}
	}
	// Spread moisture from freshwater sources
	for i := 0; i < len(toSpread); i++ {
		edge := toSpread[i]
		adjacentEdges := adjacentEdges(edge)
		for _, adjacentEdge := range adjacentEdges {
			adjacentMoisture := g.corners[edge].moisture * freshwaterMoistureSpreadRate
			if adjacentMoisture > g.corners[adjacentEdge].moisture {
				g.corners[adjacentEdge].moisture = adjacentMoisture
				toSpread = append(toSpread, adjacentEdge)
			}
		}
	}
	// Set moisture from saltwater
	setAllMoistures(filterDataOnKind(g.getAllCornerData(), Coast, Ocean), saltwaterMoisture)
}

func setAllMoistures(d []*data, m float64) {
	for _, data := range d {
		setMoisture(data, m)
	}
}

func setMoisture(d *data, m float64) {
	d.moisture = m
}

func sortByMoisture(d []*data) {
	sort.Sort(moistureDataSlice(d))
}

func linearInverseCDF(x float64) float64 {
	return x
}

func getMoisture(d *data) float64 {
	return d.moisture
}

func (g *GeneratedMap) determineBiomes(biome DetermineBiome) {
	for _, cell := range g.diagram.Cells {
		data := g.cells[cell]
		data.biome = biome(data.kind, data.elevation, data.moisture)
	}
}

func (g *GeneratedMap) tracef(atLevel InfoLevel, format string, a ...interface{}) (n int, err error) {
	if !g.traceLevel.shouldTrace(atLevel) {
		return 0, nil
	}
	return fmt.Printf(format, a...)
}

func (g *GeneratedMap) traceln(atLevel InfoLevel, a ...interface{}) (n int, err error) {
	if !g.traceLevel.shouldTrace(atLevel) {
		return 0, nil
	}
	return fmt.Println(a...)
}

func ToImage(generatedMap *GeneratedMap, bBox voronoi.BBox, zoomFactor int, rivers, downslopes, coastPts bool) image.Image {
	fZoomFactor := float64(zoomFactor)
	xPt := int(math.Floor(fZoomFactor * bBox.Xr))
	yPt := int(math.Floor(fZoomFactor * bBox.Yb))
	img := image.NewNRGBA(image.Rectangle{image.Point{0, 0}, image.Point{xPt, yPt}})
	for y := img.Bounds().Min.Y; y < img.Bounds().Max.Y; y++ {
		for x := img.Bounds().Min.X; x < img.Bounds().Max.X; x++ {
			yActual := img.Bounds().Max.Y - y - 1
			pixelCenterX := (float64(x) + 0.5) / fZoomFactor
			pixelCenterY := (float64(yActual) + 0.5) / fZoomFactor
			biome := generatedMap.BiomeAt(pixelCenterX, pixelCenterY)
			if biome != nil {
				img.SetNRGBA(x, y, biome.Color())
			}
		}
	}
	if rivers {
		generatedMap.DrawRivers(img, img.Bounds().Max.Y, zoomFactor, color.NRGBA{34, 85, 136, 255})
	}
	if downslopes {
		generatedMap.DrawDownslopes(img, img.Bounds().Max.Y, zoomFactor)
	}
	if coastPts {
		generatedMap.DrawCoastPoints(img, img.Bounds().Max.Y, zoomFactor)
	}
	return img
}
