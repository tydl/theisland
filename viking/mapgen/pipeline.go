package mapgen

import (
	"image/color"
	"math"
	"sort"

	"github.com/pzsz/voronoi"
)

// InfoLevel dictates the tracing level when generating a map
type InfoLevel int

func (i InfoLevel) shouldTrace(other InfoLevel) bool {
	return int(i) >= int(other)
}

const (
	None   InfoLevel = 0
	Info             = 5
	Fine             = 15
	Finest           = 50
)

// MapSurfaceType determines if a piece of map surface is land, water, etc.
type MapSurfaceType int

const (
	Land  MapSurfaceType = 0
	Ocean                = 1
	Water                = 2
	Lake                 = 3
	Coast                = 4
)

// IsWater returns true if it is neither land nor coast.
func (m MapSurfaceType) IsWater() bool {
	return m != Land && m != Coast
}

// VoronoiPointSelecter selects the initial voronoi points.
type VoronoiPointSelecter interface {
	Vertices() voronoi.Vertices
	BBox() voronoi.BBox
}

// PostPointProcess operates on the voronoi diagram after point selection.
type PostPointProcess interface {
	Process(d *voronoi.Diagram, b voronoi.BBox) *voronoi.Diagram
}

// IsWater determines if a certain voronoi cell corner should be treated as
// water (true) or land (false).
type IsWater func(voronoi.Vertex) bool

// IsRiverPoint can restrict the elevation rivers begin to form at.
type IsRiverPoint func(elevation float64) bool

// RiverMoistureVolume determines the amount of moisture provided by a river
// based on its volume.
type RiverMoistureVolume func(volume float64) float64

// FreshwaterMoisture determines the amount of moisture provided by a source of
// freshwater.
type FreshwaterMoisture func() float64

// DetermineBiome determines the biome for the provided information about a
// voronoi cell. Elevation and Moisture will range in value between 0 and 1.
type DetermineBiome func(surface MapSurfaceType, elevation, moisture float64) Biome

// Biome represents a general region with shared climate.
type Biome interface {
	Name() string
	Color() color.NRGBA
	IsWater() bool
}

// GenerationPipeline creates a procedurally-generated map using voronoi cells
// and technique based on the reference in the package documentation.
type GenerationPipeline struct {
	// Trace turns on print statements to debug
	Trace InfoLevel
	// RemoveNotConnectedToMiddle removes land that is not connected to the middle
	// cell.
	RemoveNotConnectedToMiddle bool
	// FractionForLakes is a value between 0.0 and 1.0 for determining how often
	// lakes form. It is the fraction of corners that have to be water for the
	// cell to be considered a lake.
	FractionForLakes float64
	// RiverVolumePerEdge is river volume gain per edge traversed. Used for
	// calculating moisture.
	RiverVolumePerEdge float64
	// NumberRiverSources is the number of river sources for the map to have.
	NumberRiverSources int
	// RiverRandSeed is the seed to use for selecting random rivers
	RiverRandSeed int64
	// FreshwaterMoistureSpreadRate is the fraction of moisture that is spread
	// along cell edges from freshwater sources.
	FreshwaterMoistureSpreadRate float64
	// SaltwaterMoisture is the amount of moisture provided by salt water.
	SaltwaterMoisture   float64
	PointSelecter       VoronoiPointSelecter
	PostPointProcesses  []PostPointProcess
	IsWater             IsWater
	IsRiverPoint        IsRiverPoint
	RiverMoistureVolume RiverMoistureVolume
	FreshwaterMoisture  FreshwaterMoisture
	DetermineBiome      DetermineBiome
}

func (g GenerationPipeline) Generate() *GeneratedMap {
	bBox := g.PointSelecter.BBox()
	diagram := voronoi.ComputeDiagram(g.PointSelecter.Vertices(), bBox, true)
	for _, p := range g.PostPointProcesses {
		diagram = p.Process(diagram, bBox)
	}
	sort.Sort(byYCell(diagram.Cells))
	sort.Sort(byY(diagram.Edges))
	center := voronoi.Vertex{X: (bBox.Xr + bBox.Xl) / 2, Y: (bBox.Yt + bBox.Yb) / 2}
	gen := newGeneratedMap(diagram, g, center)
	return gen
}

func distance(x0, y0, x1, y1 float64) float64 {
	return math.Sqrt((x0-x1)*(x0-x1) + (y0-y1)*(y0-y1))
}

type byY []*voronoi.Edge

func (d byY) Len() int           { return len(d) }
func (d byY) Less(i, j int) bool { return d[i].Va.Y < d[j].Va.Y }
func (d byY) Swap(i, j int)      { d[i], d[j] = d[j], d[i] }

type byYCell []*voronoi.Cell

func (d byYCell) Len() int           { return len(d) }
func (d byYCell) Less(i, j int) bool { return d[i].Site.Y < d[j].Site.Y }
func (d byYCell) Swap(i, j int)      { d[i], d[j] = d[j], d[i] }
