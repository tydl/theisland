package mapgen

import (
	"math"
	"math/rand"

	"github.com/ojrac/opensimplex-go"
	"github.com/pzsz/voronoi"
	"github.com/pzsz/voronoi/utils"
)

var _ VoronoiPointSelecter = &DefaultVoronoiPointSelecter{}

type DefaultVoronoiPointSelecter struct {
	Width       float64
	Height      float64
	NumberCells int
	Seed        int64
	bBox        voronoi.BBox
}

func NewDefaultVoronoiPointSelecter(Width, Height float64, NumberCells int, Seed int64) DefaultVoronoiPointSelecter {
	return DefaultVoronoiPointSelecter{Width, Height, NumberCells, Seed, voronoi.BBox{}}
}

func (d DefaultVoronoiPointSelecter) Vertices() voronoi.Vertices {
	rnd := rand.New(rand.NewSource(d.Seed))
	bbox := d.BBox()
	count := d.NumberCells
	sites := make([]voronoi.Vertex, count)
	w := bbox.Xr - bbox.Xl
	h := bbox.Yb - bbox.Yt
	for j := 0; j < count; j++ {
		sites[j].X = rnd.Float64()*w + bbox.Xl
		sites[j].Y = rnd.Float64()*h + bbox.Yt
	}
	return sites
}

func (d DefaultVoronoiPointSelecter) BBox() voronoi.BBox {
	d.bBox = voronoi.NewBBox(0, d.Width, 0, d.Height)
	return d.bBox
}

var _ PostPointProcess = &RelaxProcess{}

type RelaxProcess struct {
	NRelaxations int
}

func (r RelaxProcess) Process(d *voronoi.Diagram, b voronoi.BBox) *voronoi.Diagram {
	for i := 0; i < r.NRelaxations; i++ {
		v := utils.LloydRelaxation(d.Cells)
		d = voronoi.ComputeDiagram(v, b, true)
	}
	return d
}

var _ IsWater = NewSimplexWater(0)

func NewSimplexWater(seed int64) IsWater {
	gen := opensimplex.NewWithSeed(seed)
	return func(v voronoi.Vertex) bool {
		return gen.Eval2(v.X, v.Y) < 0
	}
}

var _ IsWater = NewBiasMiddle(0, 0, 0, 0, 0)

func NewBiasMiddle(xMid, yMid, rad, factor float64, seed int64) IsWater {
	gen := opensimplex.NewWithSeed(seed)
	return func(v voronoi.Vertex) bool {
		noise := gen.Eval2(v.X, v.Y)
		bias := (rad - distance(v.X, v.Y, xMid, yMid)) / factor
		return noise+bias < 0
	}
}

var _ IsRiverPoint = DefaultRiverPoint

func DefaultRiverPoint(elevation float64) bool {
	return elevation > 0.3 && elevation < 0.9
}

var _ RiverMoistureVolume = DefaultRiverMoistureVolume

func DefaultRiverMoistureVolume(volume float64) float64 {
	return math.Min(3, volume*0.25)
}

var _ FreshwaterMoisture = DefaultFreshwaterMoisture

func DefaultFreshwaterMoisture() float64 {
	return 1.0
}

var _ DetermineBiome = DefaultDetermineBiome

func DefaultDetermineBiome(surface MapSurfaceType, elevation, moisture float64) Biome {
	if surface == Ocean {
		return &OceanBiome{}
	} else if surface == Coast {
		return &BeachBiome{}
	} else if surface == Lake {
		if elevation > 0.8 {
			return &IceBiome{}
		} else if elevation < 0.1 {
			return &MarshBiome{}
		}
		return &LakeBiome{}
	} else if elevation > 0.8 {
		if moisture > 0.5 {
			return &SnowBiome{}
		} else if moisture > 0.33 {
			return &TundraBiome{}
		} else if moisture > 0.16 {
			return &BareBiome{}
		}
		return &ScorchedBiome{}
	} else if elevation > 0.6 {
		if moisture > 0.66 {
			return &TaigaBiome{}
		} else if moisture > 0.33 {
			return &ShrublandBiome{}
		}
		return &TemperateDesertBiome{}
	} else if elevation > 0.3 {
		if moisture > 0.83 {
			return &TemperateRainforestBiome{}
		} else if moisture > 0.5 {
			return &TemperateDeciduousBiome{}
		} else if moisture > 0.16 {
			return &GrasslandBiome{}
		}
		return &TemperateDesertBiome{}
	}
	if moisture > 0.66 {
		return &TropicalRainforestBiome{}
	} else if moisture > 0.33 {
		return &TropicalSeasonalForestBiome{}
	} else if moisture > 0.16 {
		return &GrasslandBiome{}
	}
	return &SubtropicalDesertBiome{}
}

var DefaultGenerationPipeline GenerationPipeline = GenerationPipeline{
	None,
	true,
	0.37, // FractionForLakes
	1,    // RiverVolumePerEdge
	40,   // NumberRiverSources
	1,    // RiverRandSeed
	0.8,  // FreshwaterMoistureSpreadRate
	0.7,  // SaltwaterMoisture
	NewDefaultVoronoiPointSelecter(500, 500, 2000, 0), // PointSelecter
	[]PostPointProcess{RelaxProcess{3}},               // PostPointProcess
	NewBiasMiddle(250, 250, 250, 250, 1),              // IsWater
	DefaultRiverPoint,                                 // IsRiverPoint
	DefaultRiverMoistureVolume,                        // RiverMoistureVolume
	DefaultFreshwaterMoisture,                         // FreshwaterMoisture
	DefaultDetermineBiome,                             // DetermineBiome
}
