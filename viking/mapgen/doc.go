/*
Package mapgen is based on Amit's Voronoi-polygon Lloyd-relaxation generation
techniques as outlined at http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/.
The code this is inspired from can be found at https://github.com/amitp/mapgen2.
*/
package mapgen
