package main

import (
	"bytes"
	"image/png"
	"io/ioutil"

	"gitlab.com/tydl/theisland/viking/mapgen"
)

const (
	scaleFactor = 4
)

func main() {
	generatedMap := mapgen.DefaultGenerationPipeline.Generate()
	bBox := mapgen.DefaultGenerationPipeline.PointSelecter.BBox()
	toDisplay := mapgen.ToImage(generatedMap, bBox, scaleFactor)
	buff := bytes.NewBuffer(make([]byte, 0))
	if err := png.Encode(buff, toDisplay); err != nil {
		panic(err)
	}
	ioutil.WriteFile("gen.png", buff.Bytes(), 0666)
}
