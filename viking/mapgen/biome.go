package mapgen

import "image/color"

const (
	OceanName                    = "Ocean"
	MarshName                    = "Marsh"
	IceName                      = "Ice"
	LakeName                     = "Lake"
	BeachName                    = "Beach"
	SnowName                     = "Snow"
	TundraName                   = "Tundra"
	BareName                     = "Bare"
	ScorchedName                 = "Scorched"
	TaigaName                    = "Taiga"
	ShrublandName                = "Shrubland"
	TemperateDesertName          = "Temperate Desert"
	TemperateRainforestName      = "Temperate Rainforest"
	TemperateDeciduousForestName = "Temperate Deciduous Forest"
	GrasslandName                = "Grassland"
	TropicalRainforestName       = "Tropical Rainforest"
	TropicalSeasonalForestName   = "Tropical Seasonal Forest"
	SubtropicalDesertName        = "Subtropical Desert"
)

var _ Biome = &OceanBiome{}
var _ Biome = &MarshBiome{}
var _ Biome = &IceBiome{}
var _ Biome = &LakeBiome{}
var _ Biome = &BeachBiome{}
var _ Biome = &SnowBiome{}
var _ Biome = &TundraBiome{}
var _ Biome = &BareBiome{}
var _ Biome = &ScorchedBiome{}
var _ Biome = &TaigaBiome{}
var _ Biome = &ShrublandBiome{}
var _ Biome = &TemperateDesertBiome{}
var _ Biome = &TemperateRainforestBiome{}
var _ Biome = &TemperateDeciduousBiome{}
var _ Biome = &GrasslandBiome{}
var _ Biome = &TropicalRainforestBiome{}
var _ Biome = &TropicalSeasonalForestBiome{}
var _ Biome = &SubtropicalDesertBiome{}

// AllBiomes is a list of all supported biomes
var AllBiomes = []Biome{
	&OceanBiome{},
	&MarshBiome{},
	&IceBiome{},
	&LakeBiome{},
	&BeachBiome{},
	&SnowBiome{},
	&TundraBiome{},
	&BareBiome{},
	&ScorchedBiome{},
	&TaigaBiome{},
	&ShrublandBiome{},
	&TemperateDesertBiome{},
	&TemperateRainforestBiome{},
	&TemperateDeciduousBiome{},
	&GrasslandBiome{},
	&TropicalRainforestBiome{},
	&TropicalSeasonalForestBiome{},
	&SubtropicalDesertBiome{},
}

func FromString(s string) Biome {
	for _, b := range AllBiomes {
		if b.Name() == s {
			return b
		}
	}
	return &InvalidBiome{}
}

type InvalidBiome struct{}

func (InvalidBiome) Name() string       { return "Invalid" }
func (InvalidBiome) Color() color.NRGBA { return color.NRGBA{0, 0, 0, 255} }
func (b InvalidBiome) String() string   { return b.Name() }
func (InvalidBiome) IsWater() bool      { return false }

type OceanBiome struct{}

func (OceanBiome) Name() string       { return OceanName }
func (OceanBiome) Color() color.NRGBA { return color.NRGBA{68, 68, 122, 255} }
func (b OceanBiome) String() string   { return b.Name() }
func (OceanBiome) IsWater() bool      { return true }

type MarshBiome struct{}

func (MarshBiome) Name() string       { return MarshName }
func (MarshBiome) Color() color.NRGBA { return color.NRGBA{47, 102, 102, 255} }
func (b MarshBiome) String() string   { return b.Name() }
func (MarshBiome) IsWater() bool      { return false }

type IceBiome struct{}

func (IceBiome) Name() string       { return IceName }
func (IceBiome) Color() color.NRGBA { return color.NRGBA{153, 255, 255, 255} }
func (b IceBiome) String() string   { return b.Name() }
func (IceBiome) IsWater() bool      { return false }

type LakeBiome struct{}

func (LakeBiome) Name() string       { return LakeName }
func (LakeBiome) Color() color.NRGBA { return color.NRGBA{51, 102, 153, 255} }
func (b LakeBiome) String() string   { return b.Name() }
func (LakeBiome) IsWater() bool      { return true }

type BeachBiome struct{}

func (BeachBiome) Name() string       { return BeachName }
func (BeachBiome) Color() color.NRGBA { return color.NRGBA{160, 144, 119, 255} }
func (b BeachBiome) String() string   { return b.Name() }
func (BeachBiome) IsWater() bool      { return false }

type SnowBiome struct{}

func (SnowBiome) Name() string       { return SnowName }
func (SnowBiome) Color() color.NRGBA { return color.NRGBA{255, 255, 255, 255} }
func (b SnowBiome) String() string   { return b.Name() }
func (SnowBiome) IsWater() bool      { return false }

type TundraBiome struct{}

func (TundraBiome) Name() string       { return TundraName }
func (TundraBiome) Color() color.NRGBA { return color.NRGBA{187, 187, 170, 255} }
func (b TundraBiome) String() string   { return b.Name() }
func (TundraBiome) IsWater() bool      { return false }

type BareBiome struct{}

func (BareBiome) Name() string       { return BareName }
func (BareBiome) Color() color.NRGBA { return color.NRGBA{136, 136, 136, 255} }
func (b BareBiome) String() string   { return b.Name() }
func (BareBiome) IsWater() bool      { return false }

type ScorchedBiome struct{}

func (ScorchedBiome) Name() string       { return ScorchedName }
func (ScorchedBiome) Color() color.NRGBA { return color.NRGBA{85, 85, 85, 255} }
func (b ScorchedBiome) String() string   { return b.Name() }
func (ScorchedBiome) IsWater() bool      { return false }

type TaigaBiome struct{}

func (TaigaBiome) Name() string       { return TaigaName }
func (TaigaBiome) Color() color.NRGBA { return color.NRGBA{153, 170, 119, 255} }
func (b TaigaBiome) String() string   { return b.Name() }
func (TaigaBiome) IsWater() bool      { return false }

type ShrublandBiome struct{}

func (ShrublandBiome) Name() string       { return ShrublandName }
func (ShrublandBiome) Color() color.NRGBA { return color.NRGBA{136, 153, 119, 255} }
func (b ShrublandBiome) String() string   { return b.Name() }
func (ShrublandBiome) IsWater() bool      { return false }

type TemperateDesertBiome struct{}

func (TemperateDesertBiome) Name() string       { return TemperateDesertName }
func (TemperateDesertBiome) Color() color.NRGBA { return color.NRGBA{201, 210, 155, 255} }
func (b TemperateDesertBiome) String() string   { return b.Name() }
func (TemperateDesertBiome) IsWater() bool      { return false }

type TemperateRainforestBiome struct{}

func (TemperateRainforestBiome) Name() string       { return TemperateRainforestName }
func (TemperateRainforestBiome) Color() color.NRGBA { return color.NRGBA{68, 136, 85, 255} }
func (b TemperateRainforestBiome) String() string   { return b.Name() }
func (TemperateRainforestBiome) IsWater() bool      { return false }

type TemperateDeciduousBiome struct{}

func (TemperateDeciduousBiome) Name() string       { return TemperateDeciduousForestName }
func (TemperateDeciduousBiome) Color() color.NRGBA { return color.NRGBA{103, 148, 89, 255} }
func (b TemperateDeciduousBiome) String() string   { return b.Name() }
func (TemperateDeciduousBiome) IsWater() bool      { return false }

type GrasslandBiome struct{}

func (GrasslandBiome) Name() string       { return GrasslandName }
func (GrasslandBiome) Color() color.NRGBA { return color.NRGBA{136, 170, 85, 255} }
func (b GrasslandBiome) String() string   { return b.Name() }
func (GrasslandBiome) IsWater() bool      { return false }

type TropicalRainforestBiome struct{}

func (TropicalRainforestBiome) Name() string       { return TropicalRainforestName }
func (TropicalRainforestBiome) Color() color.NRGBA { return color.NRGBA{51, 119, 85, 255} }
func (b TropicalRainforestBiome) String() string   { return b.Name() }
func (TropicalRainforestBiome) IsWater() bool      { return false }

type TropicalSeasonalForestBiome struct{}

func (TropicalSeasonalForestBiome) Name() string       { return TropicalSeasonalForestName }
func (TropicalSeasonalForestBiome) Color() color.NRGBA { return color.NRGBA{85, 153, 68, 255} }
func (b TropicalSeasonalForestBiome) String() string   { return b.Name() }
func (TropicalSeasonalForestBiome) IsWater() bool      { return false }

type SubtropicalDesertBiome struct{}

func (SubtropicalDesertBiome) Name() string       { return SubtropicalDesertName }
func (SubtropicalDesertBiome) Color() color.NRGBA { return color.NRGBA{210, 185, 139, 255} }
func (b SubtropicalDesertBiome) String() string   { return b.Name() }
func (SubtropicalDesertBiome) IsWater() bool      { return false }
