// Package core contain the game engine and other essential systems that drive
// other features and systems. Under the hood, it leverages parallelism where
// possible which merits caution from implementers. The core game loop is
// similar to other roguelike games: A player takes an action, which causes
// time to pass. That passing of time enables AI systems to act and react.
// Events can also be triggered by the player's actions or by the game's
// systems.
//
// Logically, the game is separated into three behavior types that happen in
// four stages. The first behavior type is a player's actions. The second are
// events. The third are notifications of time passing.
//
// The flow of the game is separated into four stages. First, a player must
// take an action. This, as a side effect, could add Events or Observers.
// Second, events that were added in this manner are triggered. Events could
// trigger more events, the addition of Observers, or their removal. Third, the
// game's systems are notified of the time passing due to the player's action.
// These systems are not added nor removed throughout the life of the game. The
// systems could also add Events to be executed. The fourth step is resolving
// these final events until there are none left. It is then the player's turn
// once more.
//
// The second, third, and fourth step are all individually parallelized as much
// as possible. This enables having more numerous game systems, events, and
// observers before impacting performance. However, this means implementors
// must not make assumptions about ordering between systems or observers.
package core
