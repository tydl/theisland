package core

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"sort"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/tydl/theisland/viking/errors"
)

const (
	defaultBufferCapacity   = 2048
	ActionResponseType      = "actionResponse"
	SaveGameURI             = "/save"
	SaveGameMethod          = "GET"
	LoadGameURI             = "/load"
	LoadGameMethod          = "POST"
	StatusURI               = "/status"
	StatusMethod            = "GET"
	ListActionsURI          = "/actions"
	ListActionsMethod       = "GET"
	ListDatasURI            = "/datas"
	ListDatasMethod         = "GET"
	RequestVariablePostBody = "body"
	JavascriptURI           = "/js/{filename:(.*\\/*)*.*}"
	JavascriptMethod        = "GET"
	textJavascript          = "text/javascript"
	CSSURI                  = "/css/{filename:(.*\\/*)*.*}"
	CSSMethod               = "GET"
	textCSS                 = "text/css"
	// Status variables
	startTimeStatus      = "Start Time"
	uptimeStatus         = "Uptime"
	moduleRequestsServed = "Module Requests Served"
	moduleDataServed     = "Module Data Requests Served"
	numberErrorsLogged   = "Number Errors Logged"
	contentTypeHeader    = "Content-Type"
	applicationJson      = "application/json"
	megabyte             = 1048576
	maxLoadFileSize      = 10 * megabyte
)

var _ moduleRegistrar = &Server{}
var _ errorLogger = &Server{}

type stats struct {
	Stats         map[string]interface{}
	RegisterStats map[string]registerStats
}

type registerStats struct {
	Endpoint string
	Method   string
	Module   string
	Times    int
}

// Server handles the mechanics of servicing requests from the player and
// providing responses back to the player. It is a HTTP server running on the
// player's computer as localhost. This approach utilizes the browser as a
// cross-platform GUI, instead of burdensome alternatives.
//
// It contains a game engine within, to drive the flow of the game.
//
// None of the Server's functions here are multithreaded safe. It does handle
// incoming requests in a multithreaded environment. While normally a boon,
// having a single player makes this point rather moot.
type Server struct {
	router      *mux.Router
	listener    *quittableTCPListener
	errorLogger *log.Logger
	game        *engine
	manual      *manual
	// Statistics
	stats      *stats
	statsMutex sync.Mutex
}

// NewServer constructs a new server using the Logger to write out errors. The
// provided RetrieveSystemer is used when handling requests.
func NewServer(errLog *log.Logger) *Server {
	s := &Server{
		router:      mux.NewRouter(),
		listener:    nil,
		errorLogger: errLog,
		stats: &stats{
			Stats:         make(map[string]interface{}, 20),
			RegisterStats: make(map[string]registerStats, 20),
		},
		statsMutex: sync.Mutex{},
	}
	s.game = newEngine(s)
	s.manual = newManual(s)
	s.addDefaultEngineRoutes()
	s.initializeStats()
	return s
}

// LogError logs an error out to a Logger.
func (s *Server) LogError(err error) {
	s.incrementErrorsLogged()
	s.errorLogger.Println(err)
}

// RegisterModule will register the module with the server.
//
// This call is not multithreaded safe.
func (s *Server) RegisterModule(module Moduler) {
	actions := module.Actions()
	for _, elem := range actions {
		s.handle(elem.Uri, elem.Acter, elem.Text, module.Name())
	}
	handlers := module.DataHandlers()
	for _, elem := range handlers {
		s.serveData(elem.Uri, elem.Handler, module.Name())
	}
	manualEntries := module.ManualEntries()
	for _, elem := range manualEntries {
		s.manual.AddManualData(elem.Title, elem.Text, module.Name())
	}
	templates := module.Templates()
	for _, elem := range templates {
		s.serveTemplate("/"+elem.Name(), elem, module.Name())
	}
	s.game.RegisterModule(module)
}

// Start asynchronously starts the server on localhost, returning an error if
// it could not be started. If the server is already running, this function
// does nothing.
func (s *Server) Start() error {
	if s.listener != nil {
		return nil
	}
	host := net.TCPAddr{
		IP:   net.IPv6loopback,
		Port: 8080,
	}
	tcpListener, err := net.ListenTCP("tcp", &host)
	if err != nil {
		return errors.StackWrap(err)
	}
	s.listener = newQuittableTCPListener(tcpListener, time.Second)
	go func() {
		s.stats.Stats[startTimeStatus] = time.Now()
		err := http.Serve(s.listener, s.router)
		if err != nil && err != quittingError {
			s.LogError(errors.StackWrap(err))
		}
	}()
	s.game.AddEvent(InitializeGameEvent{})
	s.game.triggerEvents()
	fmt.Printf("Server started at http://localhost:%d\n", host.Port)
	return nil
}

// Stop stops the server if it is running, blocking until it is stopped. Once
// this function returns, the server is guaranteed to be stopped.
func (s *Server) Stop() {
	if s.listener != nil {
		s.listener.Stop()
		s.listener = nil
	}
}

// WrapData is a convenience function to transform the DataRequestHandler into
// a http.HandlerFunc.
//
// Queries are placed directly in their keys. For queries with multiple values,
// only the first value is placed in the map.
func (s *Server) wrapData(handler DataRequestHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var err error = nil
		status := http.StatusInternalServerError
		defer func(status *int, err *error) {
			if *err != nil {
				s.LogError(*err)
			}
			if *status != http.StatusOK {
				w.WriteHeader(*status)
			}
		}(&status, &err)
		vars := requestVariables(mux.Vars(r))
		for k, v := range r.URL.Query() {
			if len(v) > 0 {
				vars[k] = v[0]
			}
		}
		data, err := handler(vars, s.game)
		if err != nil {
			err = errors.StackWrap(err)
			return
		}
		payload, err := json.Marshal(data)
		if err != nil {
			err = errors.StackWrap(err)
			return
		}
		s.incrementDataRequestStat()
		w.Header().Set(contentTypeHeader, applicationJson)
		status = http.StatusOK
		w.WriteHeader(status)
		n, err := w.Write(payload)
		if err != nil {
			s.LogError(errors.StackWrap(err))
		} else if n != len(payload) {
			s.LogError(errors.NewStackErrorStringf("truncated output for len=%d: %d", len(payload), n))
		}
	}
}

// Wrap is a convenience function to transform the action and template pair
// into a http.HandlerFunc.
//
// If there is a body, it is placed into the key "body".
func (s *Server) wrap(acter Acter, text *template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var err error = nil
		status := http.StatusInternalServerError
		defer func(status *int, err *error) {
			if *err != nil {
				s.LogError(*err)
			}
			if *status != http.StatusOK {
				w.WriteHeader(*status)
			}
		}(&status, &err)

		vars := requestVariables(mux.Vars(r))
		for k, v := range r.URL.Query() {
			if len(v) > 0 {
				vars[k] = v[0]
			}
		}
		if r.Body != nil {
			var body []byte
			body, err = ioutil.ReadAll(r.Body)
			if err != nil {
				err = errors.StackWrap(err)
				return
			}
			vars[RequestVariablePostBody] = string(body)
		}
		textData, err := s.game.applyForPlayer(acter, vars)
		if err != nil {
			err = errors.StackWrap(err)
			return
		}

		buffer := bytes.NewBuffer(make([]byte, 0, defaultBufferCapacity))
		err = text.Execute(buffer, textData)
		if err != nil {
			err = errors.StackWrap(err)
			return
		}
		payload, err := json.Marshal(struct {
			Type string `json:"type"`
			Text string `json:"text"`
		}{
			Type: ActionResponseType,
			Text: buffer.String(),
		})
		if err != nil {
			err = errors.StackWrap(err)
			return
		}

		s.incrementRequestStat()
		w.Header().Set(contentTypeHeader, applicationJson)
		status = http.StatusOK
		w.WriteHeader(status)
		n, err := w.Write(payload)
		if err != nil {
			s.LogError(errors.StackWrap(err))
		} else if n != len(payload) {
			s.LogError(errors.NewStackErrorStringf("truncated output for len=%d: %d", len(payload), n))
		}
	}
}

// WrapTemplate is a convenience function to deliver a template in a
// http.HandlerFunc. No data is passed to the template.
func (s *Server) wrapTemplate(page *template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := page.Execute(w, nil); err != nil {
			s.LogError(err)
		}
	}
}

// ServeData sets the function to handle calls that match the URI pattern in a
// GET request. The queries of the request are accessible by using the
// request variables.
func (s *Server) serveData(uri string, handler DataRequestHandler, module string) {
	s.registerHandler(uri, s.wrapData(handler), "GET", module)
}

// Handle sets the function to handle calls that match the URI pattern in a
// POST request. The body of the request is accessible by calling:
//
//         requestBody := vars.String("body")
func (s *Server) handle(uri string, acter Acter, text *template.Template, module string) {
	s.registerHandler(uri, s.wrap(acter, text), "POST", module)
}

// ServeTemplate serves the template at the given URI when a GET request is
// made. No arguments are passed into the template.
func (s *Server) serveTemplate(uri string, page *template.Template, module string) {
	s.registerHandler(uri, s.wrapTemplate(page), "GET", module)
}

// registerHandler adds the handler to the server for public API exposure.
func (s *Server) registerHandler(uri string, handlerFunc http.HandlerFunc, method, module string) {
	registrationKey := fmt.Sprintf("%s %s", uri, method)
	if times, ok := s.stats.RegisterStats[registrationKey]; ok {
		times.Times += 1
		times.Module = module
		s.stats.RegisterStats[registrationKey] = times
	} else {
		s.stats.RegisterStats[registrationKey] = registerStats{uri, method, module, 1}
	}
	s.router.Handle(uri, handlerFunc).Methods(method)
}

// addDefaultEngineRoutes adds URIs that core supports for players to call.
func (s *Server) addDefaultEngineRoutes() {
	s.registerHandler(SaveGameURI, s.saveGame(), SaveGameMethod, coreModule)
	s.registerHandler(LoadGameURI, s.loadGame(), LoadGameMethod, coreModule)
	s.registerHandler(StatusURI, s.statsHandler(), StatusMethod, coreModule)
	s.registerHandler(ListActionsURI, s.listActions(), ListActionsMethod, coreModule)
	s.registerHandler(ListDatasURI, s.listDatas(), ListDatasMethod, coreModule)
	s.registerHandler(s.manual.Register())
	s.registerHandler(s.manual.RegisterHome())
	s.registerHandler(JavascriptURI, s.fetchFile(textJavascript), JavascriptMethod, coreModule)
	s.registerHandler(CSSURI, s.fetchFile(textCSS), CSSMethod, coreModule)
}

// saveGame returns a function that can handle calling the game to save to a
// specific file.
func (s *Server) saveGame() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		payload, err := s.game.saveAll()
		if err != nil {
			s.LogError(errors.StackWrap(err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Set(contentTypeHeader, applicationJson)
		w.WriteHeader(http.StatusOK)
		n, err := w.Write(payload)
		if err != nil {
			s.LogError(errors.StackWrap(err))
		} else if n != len(payload) {
			s.LogError(errors.NewStackErrorStringf("truncated output for len=%d: %d", len(payload), n))
		}
	}
}

// saveGame returns a function that can handle calling the game to load from a
// specific file
func (s *Server) loadGame() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		payload, err := getFileContentsFromBody(r)
		if err != nil {
			s.LogError(errors.StackWrap(err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		err = s.game.loadAllFromFile(payload)
		if err != nil {
			s.LogError(errors.StackWrap(err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
	}
}

// statsHandler returns a function that builds a status page of the
// public API endpoints registered with the server and the number of times each
// has been registered. If the number is greater than one, then somewhere there
// are calls to the DataHandler interface that are stomping on each other.
func (s *Server) statsHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.setUptimeStat()
		payload, err := json.Marshal(s.stats)
		if err != nil {
			s.LogError(errors.StackWrap(err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Set(contentTypeHeader, applicationJson)
		w.WriteHeader(http.StatusOK)
		n, err := w.Write(payload)
		if err != nil {
			s.LogError(errors.StackWrap(err))
		} else if n != len(payload) {
			s.LogError(errors.NewStackErrorStringf("truncated output for len=%d: %d", len(payload), n))
		}
	}
}

// listActions lists POST actions that lead to some sort of world change.
func (s *Server) listActions() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		endpoints := make([]string, 0, len(s.stats.RegisterStats))
		for _, registered := range s.stats.RegisterStats {
			if registered.Method == "POST" {
				endpoints = append(endpoints, registered.Endpoint)
			}
		}
		sort.Sort(sort.StringSlice(endpoints))
		payload, err := json.Marshal(endpoints)
		if err != nil {
			s.LogError(errors.StackWrap(err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Set(contentTypeHeader, applicationJson)
		w.WriteHeader(http.StatusOK)
		n, err := w.Write(payload)
		if err != nil {
			s.LogError(errors.StackWrap(err))
		} else if n != len(payload) {
			s.LogError(errors.NewStackErrorStringf("truncated output for len=%d: %d", len(payload), n))
		}
	}
}

// listDatas lists the endpoints that can be called in a GET request to fetch
// data.
func (s *Server) listDatas() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		endpoints := make([]string, 0, len(s.stats.RegisterStats))
		for _, registered := range s.stats.RegisterStats {
			if registered.Method == "GET" {
				endpoints = append(endpoints, registered.Endpoint)
			}
		}
		sort.Sort(sort.StringSlice(endpoints))
		payload, err := json.Marshal(endpoints)
		if err != nil {
			s.LogError(errors.StackWrap(err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Set(contentTypeHeader, applicationJson)
		w.WriteHeader(http.StatusOK)
		n, err := w.Write(payload)
		if err != nil {
			s.LogError(errors.StackWrap(err))
		} else if n != len(payload) {
			s.LogError(errors.NewStackErrorStringf("truncated output for len=%d: %d", len(payload), n))
		}
	}
}

func (s *Server) fetchFile(encodingType string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch encodingType {
		case textCSS:
			w.Header().Set(contentTypeHeader, textCSS)
		case textJavascript:
			w.Header().Set(contentTypeHeader, textJavascript)
		}
		http.ServeFile(w, r, r.RequestURI[1:])
	}
}

// Functions for status measuring

func (s *Server) initializeStats() {
	s.stats.Stats[startTimeStatus] = time.Time{}
	s.stats.Stats[uptimeStatus] = time.Duration(0)
	s.stats.Stats[moduleRequestsServed] = 0
	s.stats.Stats[moduleDataServed] = 0
	s.stats.Stats[numberErrorsLogged] = 0
}

func (s *Server) setUptimeStat() {
	if startTimeInterface, ok := s.stats.Stats[startTimeStatus]; ok {
		if startTime, ok := startTimeInterface.(time.Time); ok {
			s.stats.Stats[uptimeStatus] = time.Now().Sub(startTime)
		}
	}
}

func (s *Server) incrementRequestStat() {
	s.incrementStat(moduleRequestsServed)
}

func (s *Server) incrementDataRequestStat() {
	s.incrementStat(moduleDataServed)
}

func (s *Server) incrementErrorsLogged() {
	s.incrementStat(numberErrorsLogged)
}

func (s *Server) incrementStat(stat string) {
	s.statsMutex.Lock()
	defer s.statsMutex.Unlock()
	if statInterface, ok := s.stats.Stats[stat]; ok {
		if status, ok := statInterface.(int); ok {
			s.stats.Stats[stat] = status + 1
		}
	}
}
