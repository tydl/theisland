package core

import (
	"encoding/json"
	"sync"

	"gitlab.com/tydl/theisland/viking/errors"
)

const (
	AddEventerId         = "addEventer"
	AddRemoveObserverId  = "addRemoveObserver"
	defaultSystemMapSize = 20
	defaultObsMapSize    = 40
)

var _ Systemer = &engine{}
var _ moduleRegistrar = &engine{}
var _ AddEventer = &engine{}
var _ AddRemoveObserver = &engine{}
var _ errorLogger = &engine{}

// engine handles the core game functionality. It manages system registration,
// events, and event observers. When a player action occurs, it progresses the
// game forward in time.
type engine struct {
	eventStackMutex *sync.Mutex
	eventObsMutex   *sync.Mutex
	saveLoadMutex   *sync.Mutex
	errorLogger     errorLogger
	eventStack      []Event
	timePassers     []TimePasser
	systemMap       map[string]SaveLoader
	eventObsMap     map[string][]Observer
}

// NewEngine constructs a new engine using the Logger to output errors.
func newEngine(l errorLogger) *engine {
	e := &engine{
		eventStackMutex: &sync.Mutex{},
		eventObsMutex:   &sync.Mutex{},
		saveLoadMutex:   &sync.Mutex{},
		errorLogger:     l,
		systemMap:       make(map[string]SaveLoader, defaultSystemMapSize),
		eventObsMap:     make(map[string][]Observer, defaultObsMapSize),
	}
	e.registerDefaultSystems()
	return e
}

func (e *engine) Id() string {
	return "engineCore"
}

func (e *engine) Save() (data interface{}, version int, err error) {
	return nil, 0, errors.NewStackErrorString("Stop calling core.engine.Save()")
}

func (e *engine) Load(loadFunction func(data interface{}) error, version int, observerManager AddRemoveObserver) error {
	return errors.NewStackErrorString("Stop calling core.engine.Load()")
}

// LogError logs an error into the system's logs.
func (e *engine) LogError(err error) {
	e.errorLogger.LogError(errors.StackWrap(err))
}

// System returns the object that was registered with the associated id.
//
// A non-nil error is returned when there is no system associated with the
// given id.
//
// This call is not multithreaded safe.
func (e *engine) System(id string) (interface{}, error) {
	if system, ok := e.systemMap[id]; ok {
		return system, nil
	} else {
		return nil, errors.NewStackErrorStringf("no system with id: %s", id)
	}
}

// RegisterModule registers the module with the engine.
//
// This call is not multithreaded safe.
func (e *engine) RegisterModule(module Moduler) {
	saveLoaders := module.SaveLoaders()
	for _, elem := range saveLoaders {
		if err := e.registerSystem(elem); err != nil {
			e.errorLogger.LogError(errors.StackWrap(err))
		}
	}
	timePassers := module.TimePassers()
	for _, elem := range timePassers {
		e.addTimePasser(elem)
	}
	observers := module.Observers()
	for _, elem := range observers {
		if err := e.AddObservers(elem.EventName, elem.Observer); err != nil {
			e.errorLogger.LogError(errors.StackWrap(err))
		}
	}
}

// AddEvent adds the given event to be triggered. Events are triggered in the
// order they are added.
//
// This call is multithreaded safe.
func (e *engine) AddEvent(event Event) {
	e.eventStackMutex.Lock()
	defer e.eventStackMutex.Unlock()
	e.eventStack = append(e.eventStack, event)
}

func (e *engine) clearEvents() {
	e.eventStackMutex.Lock()
	defer e.eventStackMutex.Unlock()
	e.eventStack = nil
}

// AddObservers adds all of the Observers who will be triggered when an event
// with a matching name occurs.
//
// A non-nil error is returned if any of the Observer's Id is not unique for
// the event, which prevents double-observing yet allows a single observer to
// listen to multiple events. If an error is returned, none of the observers
// were added.
//
// This call is multithreaded safe.
func (e *engine) AddObservers(eventName string, obses ...Observer) error {
	e.eventObsMutex.Lock()
	defer e.eventObsMutex.Unlock()
	currListening, ok := e.eventObsMap[eventName]
	if !ok {
		currListening = make([]Observer, 0, len(obses))
	}
	// NOTE: n^2 Sucks. Too lazy for map. Will it hurt in practice?
	for _, curr := range currListening {
		for _, obs := range obses {
			if curr.Id() == obs.Id() {
				return errors.NewStackErrorStringf("Observer already listening to event %s: %s", eventName, obs.Id())
			}
		}
	}
	for _, obs := range obses {
		currListening = append(currListening, obs)
	}
	e.eventObsMap[eventName] = currListening
	return nil
}

// RemoveObservers removes all Observers that are listening for an event with
// the matching name.
//
// This call is multithreaded safe.
func (e *engine) RemoveObservers(eventName string, obsIds ...string) {
	e.eventObsMutex.Lock()
	defer e.eventObsMutex.Unlock()
	currListening := e.eventObsMap[eventName]
	// NOTE: n^2 Sucks. Too lazy for map. Will it hurt in practice?
	for i := 0; i < len(currListening); {
		removed := false
		for _, id := range obsIds {
			if id == currListening[i].Id() {
				currListening = append(currListening[:i], currListening[i+1:]...)
				removed = true
				break
			}
		}
		if !removed {
			i++
		}
	}
	e.eventObsMap[eventName] = currListening
}

// RegisterSystem registers the object with the associated id, so the same
// system can be retrieved using the id. The system must be multithreaded safe.
//
// A non-nil error is returned when the id is already registered.
//
// This call is not multithreaded safe
func (e *engine) registerSystem(system SaveLoader) error {
	return e.registerSystemWithId(system.Id(), system)
}

func (e *engine) registerSystemWithId(id string, system SaveLoader) error {
	if _, ok := e.systemMap[id]; ok {
		return errors.NewStackErrorStringf("id already registered: %s", id)
	}
	e.systemMap[id] = system
	return nil
}

// AddTimePasser enables the TimePasser to receive notifications when the
// player passes game time. Caution: adding a TimePasser twice will have it
// receive the same notification twice.
func (e *engine) addTimePasser(timePasser TimePasser) {
	e.timePassers = append(e.timePassers, timePasser)
}

// applyForPlayer executs the given action for the player. Systems that are
// aware of the time passing are notified in a multithreaded manner. Events
// that are added during a player's action are executed before TimePassers are
// notified. Events added from TimePassers are executed before the function is
// completed.
//
// Errors that arise during this process are logged.
//
// This call is not multithreaded safe.
func (e *engine) applyForPlayer(action Acter, vars RequestVariables) (map[string]interface{}, error) {
	e.saveLoadMutex.Lock()
	defer e.saveLoadMutex.Unlock()
	textData, deltaT, err := action.Act(vars, e)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	e.triggerEvents()
	waitChan := make(chan struct{})
	for _, t := range e.timePassers {
		// Trigger time passing in parallel.
		go func(tPass TimePasser) {
			if err := tPass.TimePass(deltaT, e); err != nil {
				e.errorLogger.LogError(err)
			}
			waitChan <- struct{}{}
		}(t)
	}
	for i := 0; i < len(e.timePassers); i++ {
		<-waitChan
	}
	e.triggerEvents()
	return textData, nil
}

// Allow event triggers to add more events to be executed. Also allow observers
// to add/remove other observers
func (e *engine) triggerEvents() {
	for {
		// Pop current event off
		e.eventStackMutex.Lock() // NOT DEFERRED
		if len(e.eventStack) == 0 {
			e.eventStackMutex.Unlock()
			return
		}
		currEvent := e.eventStack[0]
		e.eventStack = e.eventStack[1:]
		e.eventStackMutex.Unlock()

		// Get listeners
		e.eventObsMutex.Lock()
		listeners := e.eventObsMap[currEvent.Name()]
		e.eventObsMutex.Unlock()

		// Trigger event in parallel
		allDone := make([]chan struct{}, len(listeners))
		for i, listener := range listeners {
			allDone[i] = make(chan struct{})
			go func(i int, listener Observer) {
				if err := listener.Observe(currEvent, e); err != nil {
					e.errorLogger.LogError(errors.StackWrap(err))
				}
				close(allDone[i])
			}(i, listener)
		}
		for _, done := range allDone {
			<-done
		}
	}
}

func (e *engine) registerDefaultSystems() {
	e.registerSystemWithId(AddEventerId, e)
	e.registerSystemWithId(AddRemoveObserverId, e)
}

type saveStruct struct {
	Data    interface{}
	Version int
}

// saveAllToFile saves the entire game's state to the file. The file is saved
// as json.
//
// applyForPlayer cannot be executing while saveAllToFile is executing, and
// vice versa.
func (e *engine) saveAll() ([]byte, error) {
	e.saveLoadMutex.Lock()
	defer e.saveLoadMutex.Unlock()
	saveMap := make(map[string]saveStruct, len(e.systemMap)+len(e.timePassers))
	for _, system := range e.systemMap {
		if system == e {
			continue
		}
		toSave, version, err := system.Save()
		if err != nil {
			return nil, errors.StackWrap(err)
		}
		saveMap[system.Id()] = saveStruct{toSave, version}
	}
	for _, elem := range e.timePassers {
		// No double saves
		if _, ok := saveMap[elem.Id()]; !ok {
			toSave, version, err := elem.Save()
			if err != nil {
				return nil, errors.StackWrap(err)
			}
			saveMap[elem.Id()] = saveStruct{toSave, version}
		}
	}
	payload, err := json.Marshal(saveMap)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	return payload, nil
}

type loadStruct struct {
	Data    json.RawMessage
	Version int
}

// loadAllFromFile loads the entire game's state from the file.
//
// applyForPlayer cannot be executing while loadAllFromFile is executing, and
// vice versa.
func (e *engine) loadAllFromFile(payload []byte) error {
	e.saveLoadMutex.Lock()
	defer e.saveLoadMutex.Unlock()
	loadMap := make(map[string]loadStruct, len(e.systemMap)+len(e.timePassers))
	if err := json.Unmarshal(payload, &loadMap); err != nil {
		return errors.StackWrap(err)
	}
	for id, savedStruct := range loadMap {
		if system, ok := e.systemMap[id]; ok {
			if err := system.Load(e.loadCallback(savedStruct.Data), savedStruct.Version, e); err != nil {
				return errors.StackWrap(err)
			}
		} else {
			for _, elem := range e.timePassers {
				if elem.Id() == id {
					if err := elem.Load(e.loadCallback(savedStruct.Data), savedStruct.Version, e); err != nil {
						return errors.StackWrap(err)
					}
					break
				}
			}
		}
	}
	e.clearEvents()
	e.AddEvent(&LoadedGameEvent{})
	e.triggerEvents()
	return nil
}

func (e *engine) loadCallback(raw json.RawMessage) func(data interface{}) error {
	return func(data interface{}) error {
		return errors.StackWrap(json.Unmarshal(raw, data))
	}
}
