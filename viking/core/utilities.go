package core

import (
	"io"
	"io/ioutil"
	"mime"
	"mime/multipart"
	"net/http"
	"strings"

	"gitlab.com/tydl/theisland/viking/errors"
)

func GetAddEventer(s Systemer) (AddEventer, error) {
	eventer, err := s.System(AddEventerId)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	castedEventer, ok := eventer.(AddEventer)
	if !ok {
		return nil, errors.NewStackErrorString("could not cast to AddEventer")
	}
	return castedEventer, nil
}

type fileRequest struct {
	File string `json:"file"`
}

func getFileContentsFromBody(msg *http.Request) ([]byte, error) {
	mediaType, params, err := mime.ParseMediaType(msg.Header.Get("Content-Type"))
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	if strings.HasPrefix(mediaType, "multipart/") {
		mr := multipart.NewReader(msg.Body, params["boundary"])
		var bytes []byte
		for {
			p, err := mr.NextPart()
			if err == io.EOF {
				break
			} else if err != nil {
				err = errors.StackWrap(err)
				break
			}
			body, err := ioutil.ReadAll(p)
			if err != nil {
				err = errors.StackWrap(err)
				break
			}
			bytes = append(bytes, body...)
		}
		return bytes, err
	}
	return nil, errors.NewStackErrorString("not multipart form")
}

func readAllFromFile(filename string) ([]byte, error) {
	return ioutil.ReadFile(filename)
}
