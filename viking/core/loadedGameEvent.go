package core

const (
	LoadedGameEventName = "LoadedGameEvent"
)

var _ Event = &LoadedGameEvent{}

// LoadedGameEvent is triggered after successfully loading a game.
type LoadedGameEvent struct{}

func (LoadedGameEvent) Name() string {
	return LoadedGameEventName
}
