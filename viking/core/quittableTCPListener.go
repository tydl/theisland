package core

import (
	"net"
	"time"

	"gitlab.com/tydl/theisland/viking/errors"
)

var (
	quittingError = errors.New("Quitting quittableTCPListener")
)

type quittableTCPListener struct {
	*net.TCPListener
	quit chan struct {
		resp chan<- struct{}
	}
	deadline time.Duration
}

func newQuittableTCPListener(l *net.TCPListener, deadline time.Duration) *quittableTCPListener {
	gtl := &quittableTCPListener{
		TCPListener: l,
		quit:        make(chan struct{ resp chan<- struct{} }),
		deadline:    deadline,
	}
	return gtl
}

func (q *quittableTCPListener) Accept() (net.Conn, error) {
	for {
		q.SetDeadline(time.Now().Add(q.deadline))
		conn, err := q.TCPListener.Accept()

		select {
		case quit := <-q.quit:
			close(quit.resp)
			return nil, quittingError
		default:
		}

		if err != nil {
			if netErr, ok := err.(net.Error); ok && netErr.Timeout() && netErr.Temporary() {
				continue
			}
		}
		return conn, errors.StackWrap(err)
	}
}

func (q *quittableTCPListener) Stop() {
	resp := make(chan struct{})
	q.quit <- struct{ resp chan<- struct{} }{resp}
	<-resp
}
