package core

const (
	InitializeGameEventName = "InitializeGameEvent"
)

var _ Event = &InitializeGameEvent{}

// InitializeGameEvent is triggered exactly once: when starting up the server.
type InitializeGameEvent struct{}

func (InitializeGameEvent) Name() string {
	return InitializeGameEventName
}
