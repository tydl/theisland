package core

import (
	"html/template"
	"time"
)

// Consuming Interfaces

// Actions contains data required to register a new API call for the game
// client to allow a player to do.
//
// The URI must be in a format supported by github.com/gorilla/mux. Any path
// variables will be passed through to the Acter's RequestVariables.
//
// The Acter will act when the game client sends a request matching the URI.
// The returned data by the Acter is then used to produce output.
//
// The Text template is used in conjunction with the Acter's result to return
// a response to the game client. The response is in the following json format:
//
//         {
//             "Type": core.ActionResponseType,
//             "Text": "Output from Text.Execute(Acter's results)",
//         }
//
// The call must be a POST request to the URI, and the body is passed into the
// RequestVariables for the Acter to use.
type Actions struct {
	Uri   string
	Acter Acter
	Text  *template.Template
}

// DataHandlers contains data required to register a new API call for the game
// client to display data.
//
// See Actions for a description of the URI's constraints.
//
// The handler's returned data is serialized straight into json and returned in
// the response.
type DataHandlers struct {
	Uri     string
	Handler DataRequestHandler
}

// ManualEntry contains the title and body of text for a player to read in
// order to understand the game's mechanics. If the text contains another
// article's title, it is automatically linkified.
type ManualEntry struct {
	Title string
	Text  string
}

// ObserveEvent contains a pair of data matching an event name with an
// Observer.
type ObserveEvent struct {
	Observer  Observer
	EventName string
}

// Moduler is the top-level functional unit implementors must create in order
// to interface with the core package. It encapsulates a unit of features.
//
// Modulers may be dependent on other Modulers for their behaviors, but such
// information is caught at compile time. Core provides implementors other
// tools for debugging at runtime.
type Moduler interface {
	// Name returns a human-readable name of the module.
	Name() string
	// SaveLoaders provides objects that will be accessible to Acters via
	// the Systemer. They are independent of TimePassers.
	SaveLoaders() []SaveLoader
	// TimePassers provides objects that are notified of the passing of
	// game time. They are not accessible to Acters via the Systemer
	// interface unless they are also returned by the SaveLoaders function.
	TimePassers() []TimePasser
	// Actions provides the new API POST URIs a client can call to initiate
	// a player action.
	Actions() []Actions
	// DataHandlers provides new API GET URIs a client can call to retrieve
	// data for a player.
	DataHandlers() []DataHandlers
	// ManualEntries provides articles for the player to read on how to
	// play the game, the module mechanics, lore, or other topics.
	ManualEntries() []ManualEntry
	// Observers allows modules to add observers during module
	// registration, before any player input has occurred.
	Observers() []ObserveEvent
	// Templates allows modules to provide HTML GUI templates to render.
	Templates() []*template.Template
}

// Acter executes an action on behalf of the player.
//
// Acters should not contain stateful information. All stateful information
// should be placed into a SaveLoader.
type Acter interface {
	// Act retrieves any systems present to execute its behavior on behalf
	// of the player.
	//
	// Returns a map containing data to use in the associated template
	// registered with this action.
	//
	// When returning an error, ensure that any changes partially applied
	// to systems were properly reverted to prevent corrupted states.
	//
	// This call would be made in a multithreaded context if there were
	// multiple players.
	Act(vars RequestVariables, game Systemer) (map[string]interface{}, time.Duration, error)
}

// TimePasser is notified of changes to game time. They are not added and
// removed dynamically during gameplay.
//
// No assumptions can be made about the relationship between TimePassers. No
// assumptions can be made about the ordering between two TimePassers.
//
// A TimePasser can carry internal state, and must satisfy the SaveLoader
// interface.
type TimePasser interface {
	// A TimePasser is also SaveLoader as it could carry internal state.
	SaveLoader
	// TimePass retrieves any systems present to execute any behaviors that
	// occur due to this duration. This duration can also be recorded to
	// trigger longer durations of in-game time (ex: days).
	//
	// When returning an error, ensure that any changes partially applied
	// to systems were properly reverted to prevent corrupted states.
	//
	// This call will be made in a multithreaded context.
	TimePass(time.Duration, Systemer) error
}

// Observer is notified of events that are triggered by Acters, TimePassers, or
// the game engine. An observer is notified only of the events it is registered
// for. It is unique to allow dynamically adding and removing Observers.
//
// No assumptions can be made about the relationship between Observers. No
// assumptions can be made about the ordering between two Observers.
//
// An Observer can carry internal state, and must satisfy the SaveLoader
// interface.
type Observer interface {
	// Unique id for the observer to facilitate dynamic adding and removal.
	// Adding an observer with the same Id will result in errors to prevent
	// double notification.
	Id() string
	// Observe uses the data and systems to carry out the implementing
	// logic for when an event is triggered. The Event's Data must not be
	// modified as it will not impact other Observers.
	//
	// When returning an error, ensure that any changes partially applied
	// to systems were properly reverted to prevent corrupted states.
	//
	// This call will not be made in a multithreaded context.
	Observe(Event, Systemer) error
}

// Event is a generic interface allowing implementors to pass around data that
// is useful to observers.
//
// Note this is an interface instead of a struct to force implementors to
// document the implementing type's data. For example:
//
//         type EventImplementation struct {
//                 Data0 int
//                 Data1 map[string]int
//                 Data2 string
//                 ...
//         }
//
//         func (SomeObserver) Observe(e Event, Systemer) error {
//                 switch concreteEvent := e.(type) {
//                 case ConcreteType1:
//                         concreteEvent.Detail
//                 case ConcreteType2: // Could observe multiple events
//                         concreteEvent.DifferentDetail
//                 }
//         }
//
// This is preferred over a never-ending nest of map[string]interface{} or
// other ugly constructs.
//
// An event carries state but all events must be resolved before it is the
// player's turn to act again so when a player saves there will be no Events to
// save.
type Event interface {
	// Name returns the name of the event. Events of the same type must
	// share the same name. This is an optimization to only notify
	// observers that care about specific events.
	Name() string
}

// SaveLoader is a stateful game system. It is usually a TimePasser as well.
//
// Since a SaveLoader is stateful, it is responsible for saving and loading
// data that the player wants to use offline. Additionally, because Observers
// can be added and removed dynamically during the game, any Observers that
// need to be recreated need to be done by the SaveLoader.
type SaveLoader interface {
	// Id returns a unique identifier to properly route serialized data to
	// this SaveLoader. Changing this will break save compatibility.
	Id() string
	// Save returns a serializable struct, version infomation of the
	// struct, and any errors that occurred in the serialization process.
	//
	// A serializable struct is one that follows Go's rules in the standard
	// library encoding/json.
	//
	// The version number allows implementors to change their save format
	// and support reading older versions when Load is called. The version
	// number is meaningless to the core system, but only the most insane
	// implementors would try to do something quirky beyond the simplest
	// incremental pattern.
	Save() (data interface{}, version int, err error)
	// Load is given a function and a version number. The implementor must
	// use the version number to decide which serializable struct to use,
	// and pass a pointer to it into the load function. On a nil error, the
	// core system successfully loaded the data into that serializable
	// struct. The implementor can then finish initializing the SaveLoader,
	// returning an error if one is encountered in the process.
	//
	// Load is also responsible for recreating observers that are usually
	// dynamically added and removed throughout the game.
	Load(loadFunction func(data interface{}) error, version int, observerManager AddRemoveObserver) error
}

// DataRequestHandler allows implementors to provide new APIs for clients that
// are not actions. Typical examples include new UI elements, status bars, text
// windows, and debugging information.
type DataRequestHandler func(vars RequestVariables, s Systemer) (interface{}, error)

// Providing Interfaces

// Systemer provides game systems given by Moduler.SaveLoaders. A non-nil error
// is returned when a system with the given id does not exist.
//
// Acters, TimePassers, and Observers that use Systemer must type assert the
// system into its concrete useful type:
//
//         system, err := systemer.System("myImpl")
//         if err != nil {
//             systemer.LogError(err)
//             return
//         }
//         myImpl, ok := system.(myImpl)
type Systemer interface {
	LogError(error)
	System(id string) (interface{}, error)
}

// AddEventer is a system provided by core registered with AddEventerId. Use a
// Systemer to acquire an instance.
type AddEventer interface {
	AddEvent(Event)
}

// AddRemoveObserver is a system provided by core registered with
// AddRemoveObserverId. Use a Systemer to acquire an instance.
type AddRemoveObserver interface {
	AddObservers(eventName string, obses ...Observer) error
	RemoveObservers(eventName string, obsIds ...string)
}

// RequestVariables are parameters given to Acters from a request. When a call
// returns false, it means the variable is either nonexistant or could not be
// converted to that type. A value of true guarantees existence and proper
// value.
//
// First for POST and GET requests, any variables defined in the URI path are
// populated by their variable name.
//
// Next for POST requests, those that call Acters, the body of the request is
// put into RequestVariablePostBody. Any values already present at the key
// matching RequestVariablePostBody are silently replaced.
//
// Next For GET requests, those that provide Data, any query parameters of the
// request are put into the RequestVariables and will overwrite existing values
// silently.
type RequestVariables interface {
	String(key string) (string, bool)
	Int(key string) (int, bool)
	Bool(key string) (bool, bool)
	Float64(key string) (float64, bool)
}

// moduleRegistrar can register Modulers.
type moduleRegistrar interface {
	RegisterModule(Moduler)
}

// errorLogger can log errors.
type errorLogger interface {
	LogError(err error)
}
