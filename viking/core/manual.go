package core

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/tydl/theisland/viking/errors"
)

const (
	ManualURI            = "/manual"
	ManualEntryURI       = "/manual/{entry}"
	ManualMethod         = "GET"
	linkify              = "<a href=\"/manual/%s\">%s</a>"
	backToManual         = "Back To Manual Index"
	aboutThisManual      = "About This Manual"
	aboutThisManualEntry = "This manual is a collection of articles " +
		"written by game module maintainers. Since parts of this " +
		"game are authored by different people, some entries will " +
		"be more helpful than others. There may be articles missing " +
		"entirely. These articles are meant to give gameplay " +
		"overviews and advice. If you feel something should be " +
		"changed or added, please check with the author of the " +
		"appropriate module. For example, the module providing this " +
		"article is the \"Core\" module. Any changes to this " +
		"article need to be brought up to the \"Core\" maintainer."
	coreModule = "Core"
)

var appendingSchemes = []string{"", "s", "es"}
var replacementSchemes = []string{"", ".", ",", "!", "?", "'", "\""}

type manualEntry struct {
	Entry  string
	Module string
}

type manual struct {
	errorLogger errorLogger
	manualData  map[string]manualEntry
}

func newManual(errLog errorLogger) *manual {
	m := &manual{
		errorLogger: errLog,
		manualData:  make(map[string]manualEntry, 16),
	}
	m.AddManualData(aboutThisManual, aboutThisManualEntry, coreModule)
	return m
}

func (m *manual) AddManualData(name, entry, module string) {
	m.manualData[name] = manualEntry{
		Entry:  entry,
		Module: module,
	}
}

func (m *manual) RegisterHome() (string, http.HandlerFunc, string, string) {
	return ManualURI, m.getEntryOrListing(), ManualMethod, coreModule
}

func (m *manual) Register() (string, http.HandlerFunc, string, string) {
	return ManualEntryURI, m.getEntryOrListing(), ManualMethod, coreModule
}

func (m *manual) getEntryOrListing() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var err error = nil
		status := http.StatusInternalServerError
		defer func(status *int, err *error) {
			if *err != nil {
				m.errorLogger.LogError(*err)
			}
			if *status != http.StatusOK {
				w.WriteHeader(*status)
			}
		}(&status, &err)
		var data interface{} = nil
		vars := mux.Vars(r)
		if entry, ok := vars["entry"]; ok && len(entry) > 0 {
			data, err = m.getEntry(entry)
		} else {
			data, err = m.getListing()
		}
		if err != nil {
			err = errors.StackWrap(err)
			return
		}
		payload, err := json.Marshal(data)
		if err != nil {
			err = errors.StackWrap(err)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		status = http.StatusOK
		w.WriteHeader(status)
		n, err := w.Write(payload)
		if err != nil {
			m.errorLogger.LogError(errors.StackWrap(err))
		} else if n != len(payload) {
			m.errorLogger.LogError(errors.NewStackErrorStringf("truncated output for len=%d: %d", len(payload), n))
		}
	}
}

func (m *manual) getListing() (interface{}, error) {
	titleList := make([]string, 0, len(m.manualData))
	for title, _ := range m.manualData {
		titleList = append(titleList, title)
	}
	sort.Sort(sort.StringSlice(titleList))
	return titleList, nil
}

func (m *manual) getEntry(name string) (interface{}, error) {
	entry, ok := m.manualData[name]
	if !ok {
		return nil, errors.NewStackErrorStringf("no manual entry for '%s'", name)
	}
	entryTemplate := entry.Entry
	for k, _ := range m.manualData {
		if k != name {
			for toSearch, toReplace := range getTransformations(k) {
				entryTemplate = strings.Replace(entryTemplate, toSearch, toReplace, -1)
			}
		}
	}
	return struct {
		Title  string
		Entry  string
		Module string
	}{
		Title:  name,
		Entry:  entryTemplate,
		Module: entry.Module,
	}, nil
}

func getTransformations(k string) map[string]string {
	toReturn := make(map[string]string, len(appendingSchemes)*len(replacementSchemes))
	for _, toAppend := range appendingSchemes {
		for _, toReplace := range replacementSchemes {
			replace := " " + k + toAppend + toReplace + " "
			replacer := " " + fmt.Sprintf(linkify, k, k+toAppend) + toReplace + " "
			toReturn[replace] = replacer
			lowerReplace := strings.ToLower(" " + k + toAppend + toReplace + " ")
			lowerReplacer := " " + fmt.Sprintf(linkify, k, strings.ToLower(k+toAppend)) + toReplace + " "
			toReturn[lowerReplace] = lowerReplacer
		}
	}
	return toReturn
}
