package core

import (
	"strconv"
)

var _ RequestVariables = &requestVariables{}

type requestVariables map[string]string

func (r requestVariables) String(key string) (string, bool) {
	str, ok := r[key]
	return str, ok
}

func (r requestVariables) Int(key string) (int, bool) {
	str, ok := r.String(key)
	if !ok {
		return 0, false
	}
	i, err := strconv.ParseInt(str, 10, 0)
	if err != nil {
		return 0, false
	}
	return int(i), true
}

func (r requestVariables) Bool(key string) (bool, bool) {
	str, ok := r.String(key)
	if !ok {
		return false, false
	}
	b, err := strconv.ParseBool(str)
	if err != nil {
		return false, false
	}
	return b, true
}

func (r requestVariables) Float64(key string) (float64, bool) {
	str, ok := r.String(key)
	if !ok {
		return 0, false
	}
	f, err := strconv.ParseFloat(str, 64)
	if err != nil {
		return 0, false
	}
	return f, true
}
