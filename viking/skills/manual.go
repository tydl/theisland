package skills

import "gitlab.com/tydl/theisland/viking/core"

func getManualEntries() []core.ManualEntry {
	return []core.ManualEntry{
		{
			Title: "Skill",
			Text: "A skill is a unit of knowledge that the player can learn. It has " +
				"a difficulty that determines how expensive it is to level up with " +
				"experience. A skill can be leveled up multiple times, becoming more " +
				"expensive with each level gained. Becoming deeply proficient in some " +
				"skills unlocks more potent skills as basic skills are prerequisite to " +
				"more advanced ones.<br /><br />Additionally, skills are associated " +
				"with a source and a medium. The type of source and medium dictates " +
				"the kind of experience used to level up a skill.",
		},
		{
			Title: "Level Up",
			Text: "To level up a skill, the player needs to have enough experience " +
				"to spend. The experience must match the source or medium type of the " +
				"skill. For instance, if a skill has a mental source, a self medium, " +
				"and costs 200XP, then the player must have 200 experience combined " +
				"in his or her mental and self experience pools. They player can spend " +
				"200XP from one pool or a combination of XP from both pools. This lets " +
				"the player save more in a pool to help cover if lacking experience " +
				"in other pools.",
		},
		{
			Title: "Prerequisite",
			Text: "All but the most basic of skills have prerequisite skills. The " +
				"prerequisite skills must reach a certain level before they unlock " +
				"further skills. Merely attaining a single level is usually not enough.",
		},
		{
			Title: "Experience",
			Text: "Experience comes to a player's character in one of six forms. Three " +
				"forms are a source: physical, mental, and magical. The other three forms " +
				"are a medium: self, terra, and fauna. Over time, experience is slowly " +
				"gained in each. Activities can earn more for specific sources and mediums. " +
				"Experience is spent from the pools into skills with matching types. For " +
				"example, the physical experience pool can only be spent on skills with " +
				"a physical source.",
		},
		{
			Title: "Source",
			Text: "A source is the root of power in a skill or action. It comes in one " +
				"of three types: physical, mental, and magical. They counter each other " +
				"in a rock-paper-scissors fashion: physical beats mental, mental beats " +
				"magical, and magical beats physical. Skills of the same source tend to " +
				"be prerequisites of one another.",
		},
		{
			Title: "Medium",
			Text: "A medium is the expression of power in a skill or action. It comes " +
				"in one of three types: self, terra, or fauna. They counter each other " +
				"in a rock-paper-scissors fashion: self beats terra, terra beats fauna, " +
				"and fauna beats self. Skills of the same medium tend to be prerequisites " +
				"of one another.",
		},
		{
			Title: "Difficulty",
			Text: "Skills have a difficulty associated with them. The harder the skill, " +
				"the more experience points it will require to level up at all levels. " +
				"the more difficult skills can require significantly more at high levels " +
				"than the easier skills. Careful evaluation is needed to balance between " +
				"going deep into a skill tree versus going wide throughout the tree.",
		},
		{
			Title: "Level",
			Text: "There is not a traditional player character level like in other " +
				"roleplaying game systems. Instead, individual skills can be levelled " +
				"up to provide significant detailed control over a character's development. " +
				"All skills start as unlearned at level zero. Spending experience of the " +
				"correct source and medium types will level it up. Higher levels require " +
				"more experience.",
		},
		{
			Title: "Physical",
			Text: "A skill or action that derives its source of power from the " +
				"usually-brute force of physical exertion. Physical actions usually " +
				"try to assert dominance over other creatures by making opponents go " +
				"unconscious. Physical sources can be expressed in the three mediums: " +
				"self usually focuses on personal strength, terra emphasizes the strength " +
				"of the land and other plants, while fauna highlights the strength of " +
				"other creatures.",
		},
		{
			Title: "Mental",
			Text: "A skill or action that derives its source of power from the " +
				"sheer will of a creature's mind. Mental actions usually try to leverage " +
				"psychological techniques to assert dominance over other creatures by " +
				"breaking their will to resist. Mental sources can be expressed in the " +
				"three mediums: self usually focuses on personal willpower, terra " +
				"emphasizes the oneness of the earth and symbiosis with the flora, while " +
				"fauna highlights leveraging psychological warfare over other creatures.",
		},
		{
			Title: "Magical",
			Text: "A skill or action that derives its source of power from the " +
				"latent magical properties of this fantasy island. Magical actions usually " +
				"try to imbue the mundane and use fantastical spells to assert dominance " +
				"over other creatures by breaking their will to resist or go unconscious. " +
				"Magical sources can be expressed in the three mediums: self usually focuses " +
				"on personal spellcasting ability, terra emphasizes alchemical and botannical " +
				"interactions, while fauna highlights using magic to control other creatures.",
		},
		{
			Title: "Self",
			Text: "A skill or action that expresses its source of power through the " +
				"creature itself. Skills of the self tend to directly benefit the player " +
				"character. Actions that are expressions of the self are also usually direct " +
				"expressions of the source of power.",
		},
		{
			Title: "Terra",
			Text: "A skill or action that expresses its source of power through the " +
				"surrounding land and plants. Skills of terra tend to utilize the environment " +
				"surrounding the player character to his or her advantage. Actions that " +
				"are expressions of terra also leverage the environment to express the " +
				"source of power.",
		},
		{
			Title: "Fauna",
			Text: "A skill or action that expresses its source of power through any " +
				"other creature, in opposition or not. Skills of fauna tend to utilize " +
				"properties of an opposing creature or any neutral creatures nearby. " +
				"Actions that are expressions of fauna also tend to directly use a creature's " +
				"power against itself.",
		},
	}
}
