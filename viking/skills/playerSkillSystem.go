package skills

import (
	"fmt"
	"math"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/meta"
)

const (
	PlayerSkillSystemId   = "PlayerSkillSystem"
	saveLearnedMapVersion = 0
	maxLevel              = 10
	// Formula based on skill leveling in EvE Online. Keep same point scale, but
	// spread it out across 10 levels instead of 5:
	// sqrt(32) ^ 4 ~= sqrt(4.666116158) ^ 9, so sqrt(4.666116158) instead of
	// sqrt(32).
	levelFactor = 2.160119478
	levelBase   = 100
)

var _ core.SaveLoader = &PlayerSkillSystem{}

type skillPoints struct {
	Physical int `json:"physical"`
	Mental   int `json:"mental"`
	Magical  int `json:"magical"`
	Self     int `json:"self"`
	Terra    int `json:"terra"`
	Fauna    int `json:"fauna"`
}

func (s skillPoints) GetSourcePoints(src Source) int {
	switch src {
	case Physical:
		return s.Physical
	case Mental:
		return s.Mental
	case Magical:
		return s.Magical
	default:
		panic(fmt.Sprintf("unknown Source %s", src))
	}
}

func (s *skillPoints) SetSourcePoints(src Source, pts int) bool {
	if pts < 0 {
		return false
	}
	switch src {
	case Physical:
		s.Physical = pts
	case Mental:
		s.Mental = pts
	case Magical:
		s.Magical = pts
	default:
		panic(fmt.Sprintf("unknown Source %s", src))
	}
	return true
}

func (s skillPoints) GetMediumPoints(src SkillMedium) int {
	switch src {
	case Self:
		return s.Self
	case Terra:
		return s.Terra
	case Fauna:
		return s.Fauna
	default:
		panic(fmt.Sprintf("unknown SkillMedium %s", src))
	}
}

func (s *skillPoints) SetMediumPoints(src SkillMedium, pts int) bool {
	if pts < 0 {
		return false
	}
	switch src {
	case Self:
		s.Self = pts
	case Terra:
		s.Terra = pts
	case Fauna:
		s.Fauna = pts
	default:
		panic(fmt.Sprintf("unknown SkillMedium %s", src))
	}
	return true
}

type vZeroSave struct {
	Points        skillPoints
	LearnedSkills map[string]int
}

type PlayerSkillSystem struct {
	availableSkills map[string]Skill
	learnedSkills   map[string]int
	points          *skillPoints
}

func newPlayerSkillSystem(availableSkills []Skill) *PlayerSkillSystem {
	availSkillMap := make(map[string]Skill, len(availableSkills))
	for _, skill := range availableSkills {
		availSkillMap[skill.Id] = skill
	}
	return &PlayerSkillSystem{
		availSkillMap,
		make(map[string]int),
		&skillPoints{},
	}
}

func (*PlayerSkillSystem) Id() string {
	return PlayerSkillSystemId
}

func (p *PlayerSkillSystem) Save() (data interface{}, version int, err error) {
	return vZeroSave{*p.points, p.learnedSkills}, saveLearnedMapVersion, nil
}

func (p *PlayerSkillSystem) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	switch version {
	case saveLearnedMapVersion:
		var vZeroSave vZeroSave
		vZeroSave.LearnedSkills = make(map[string]int)
		if err := loadFunction(&vZeroSave); err != nil {
			return errors.StackWrap(err)
		}
		p.learnedSkills = vZeroSave.LearnedSkills
		p.points = &vZeroSave.Points
	default:
		return errors.NewStackErrorStringf("Unknown version: %d", version)
	}
	return nil
}

func (p *PlayerSkillSystem) Observe(event core.Event, s core.Systemer) error {
	switch event.Name() {
	case meta.CreateGameEventName:
		p.points = &skillPoints{}
		p.learnedSkills = make(map[string]int)
	}
	return nil
}

func (p *PlayerSkillSystem) AddPhysicalPoints(pts int) {
	p.points.Physical += pts
}

func (p *PlayerSkillSystem) AddMentalPoints(pts int) {
	p.points.Mental += pts
}

func (p *PlayerSkillSystem) AddMagicalPoints(pts int) {
	p.points.Magical += pts
}

func (p *PlayerSkillSystem) AddSelfPoints(pts int) {
	p.points.Self += pts
}

func (p *PlayerSkillSystem) AddTerraPoints(pts int) {
	p.points.Terra += pts
}

func (p *PlayerSkillSystem) AddFaunaPoints(pts int) {
	p.points.Fauna += pts
}

func (p *PlayerSkillSystem) LevelUpSkill(s core.Systemer, id string, sourcePts, mediumPts int) (bool, error) {
	ok := p.doLevelUpSkill(id, sourcePts, mediumPts)
	if !ok {
		return false, nil
	}
	return true, p.availableSkills[id].OnLevelUp(s)
}

func (p *PlayerSkillSystem) doLevelUpSkill(id string, sourcePts, mediumPts int) bool {
	currSkill, ok := p.learnedSkills[id]
	if !ok {
		currSkill = 0
	}
	// 1: Ensure not max level
	if currSkill == maxLevel {
		return false
	}
	// 2: Ensure prerequisites are met
	toLearn := p.availableSkills[id]
	if !p.prereqsMet(toLearn) {
		return false
	}
	// 3: Ensure points add up to required amount
	if sourcePts+mediumPts != p.getSkillCost(toLearn.Difficulty, currSkill) {
		return false
	}
	// 4: Ensure have enough points for payment
	currSrcPts := p.points.GetSourcePoints(toLearn.Source)
	if currSrcPts < sourcePts {
		return false
	}
	currMedPts := p.points.GetMediumPoints(toLearn.Medium)
	if currMedPts < mediumPts {
		return false
	}
	// Learn skill
	p.points.SetSourcePoints(toLearn.Source, currSrcPts-sourcePts)
	p.points.SetMediumPoints(toLearn.Medium, currMedPts-mediumPts)
	p.learnedSkills[id] = currSkill + 1
	return true
}

func (p *PlayerSkillSystem) TotalLevelsLearnedForPayload(data SkillEffectPayload) int {
	var totalLevels = 0
	for id, level := range p.learnedSkills {
		if p.availableSkills[id].Applies(data) {
			totalLevels += level
			break
		}
	}
	return totalLevels
}

func (p *PlayerSkillSystem) prereqsMet(skill Skill) bool {
	for _, prereq := range skill.Prerequesites {
		learned, ok := p.learnedSkills[prereq.SkillId]
		if !ok || learned < prereq.Level {
			return false
		}
	}
	return true
}

type GetPrerequesite struct {
	IdRequires    string `json:"idRequires"`
	IdRequired    string `json:"idRequired"`
	RequiredLevel int    `json:"requiredLevel"`
	PrereqMet     bool   `json:"prereqMet"`
	Source        int    `json:"source"`
	Target        int    `json:"target"`
}

type GetSkill struct {
	Id           string `json:"id"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	Difficulty   string `json:"difficulty"`
	Source       string `json:"source"`
	Medium       string `json:"medium"`
	CurrentLevel int    `json:"currentLevel"`
	MaximumLevel int    `json:"maximumLevel"`
	PrereqsMet   bool   `json:"prereqsMet"`
	XP           int    `json:"xp"`
	X            int    `json:"x"`
	Y            int    `json:"y"`
}

func (p *PlayerSkillSystem) newGetSkillsAndPrerequisites(s Skill) (*GetSkill, []*GetPrerequesite) {
	g := &GetSkill{}
	g.Id = s.Id
	g.Name = s.Name
	g.Description = s.Description
	g.Difficulty = s.Difficulty.String()
	g.Source = s.Source.String()
	g.Medium = s.Medium.String()
	currLevel, ok := p.learnedSkills[s.Id]
	if !ok {
		currLevel = 0
	}
	g.CurrentLevel = currLevel
	g.MaximumLevel = maxLevel
	g.PrereqsMet = p.prereqsMet(s)
	g.XP = p.getSkillCost(s.Difficulty, currLevel)
	g.X = s.X
	g.Y = s.Y
	pr := make([]*GetPrerequesite, 0, len(s.Prerequesites))
	for _, prereq := range s.Prerequesites {
		learnedLevel, ok := p.learnedSkills[prereq.SkillId]
		if !ok {
			learnedLevel = 0
		}
		pr = append(pr, &GetPrerequesite{
			IdRequires:    prereq.SkillId,
			IdRequired:    s.Id,
			RequiredLevel: prereq.Level,
			PrereqMet:     prereq.Level <= learnedLevel,
		})
	}
	return g, pr
}

func (p *PlayerSkillSystem) handleGetSkills(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	var skills []*GetSkill
	var prereqs []*GetPrerequesite
	skillIdxMap := make(map[string]int)
	for _, v := range p.availableSkills {
		skill, prerequisites := p.newGetSkillsAndPrerequisites(v)
		skills = append(skills, skill)
		prereqs = append(prereqs, prerequisites...)
		skillIdxMap[v.Id] = len(skills) - 1
	}
	for _, p := range prereqs {
		p.Source = skillIdxMap[p.IdRequires]
		p.Target = skillIdxMap[p.IdRequired]
	}
	return struct {
		Skills        []*GetSkill        `json:"skills"`
		Prerequesites []*GetPrerequesite `json:"prerequesites"`
	}{
		Skills:        skills,
		Prerequesites: prereqs,
	}, nil
}

func (p *PlayerSkillSystem) getSkillCost(d Difficulty, currentLevel int) int {
	return levelBase * int(d) * int(math.Floor(math.Pow(levelFactor, float64(currentLevel))+0.5))
}

func (p *PlayerSkillSystem) handleGetXP(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	return p.points, nil
}

func GetPlayerSkillSystem(s core.Systemer) (*PlayerSkillSystem, error) {
	system, err := s.System(PlayerSkillSystemId)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	casted, ok := system.(*PlayerSkillSystem)
	if !ok {
		return nil, errors.NewStackErrorString("could not cast to *PlayerSkillSystem")
	}
	return casted, nil
}

func GetTotalLevelsLearnedForPayload(s core.Systemer, data SkillEffectPayload) int {
	pSkill, err := GetPlayerSkillSystem(s)
	if err != nil {
		panic(err)
	}
	return pSkill.TotalLevelsLearnedForPayload(data)
}
