package skills

import (
	"encoding/json"
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/errors"
)

var _ core.Acter = &doSkillUp{}

type doSkillUp struct{}

type actionParams struct {
	SkillId   string `json:"skillId"`
	SourcePts int    `json:"sourcePts"`
	MediumPts int    `json:"mediumPts"`
}

func (doSkillUp) Act(vars core.RequestVariables, game core.Systemer) (map[string]interface{}, time.Duration, error) {
	if creature.GetReaperSystemOrPanic(game).IsPcDead() {
		return nil, 0, errors.NewStackErrorString("PC is dead")
	}
	skillSystem, err := GetPlayerSkillSystem(game)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	body, ok := vars.String("body")
	if !ok {
		return nil, 0, errors.NewStackErrorString("No body")
	}
	var actionSpec actionParams
	if err := json.Unmarshal([]byte(body), &actionSpec); err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	if ok, err := skillSystem.LevelUpSkill(game, actionSpec.SkillId, actionSpec.SourcePts, actionSpec.MediumPts); err != nil {
		return nil, 0, err
	} else if !ok {
		return nil, 0, errors.NewStackErrorString("Could not level up skill")
	}
	return nil, 0, nil
}
