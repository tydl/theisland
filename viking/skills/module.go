package skills

import (
	"html/template"
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/empty"
	"gitlab.com/tydl/theisland/viking/meta"
)

const (
	GetSkills    = "/player/skills"
	GetXP        = "/player/xp"
	DoLearnSkill = "/player/skills/learn"
)

var _ core.Moduler = &Module{}
var emptyTemplate *template.Template = template.Must(template.New("").Parse("{{.Content}}"))

type Module struct {
	empty.EmptyModule
	playerSystem  *PlayerSkillSystem
	passiveSystem *PassiveSkillSystem
	skillUp       *doSkillUp
}

func NewModule(availableSkills []Skill) *Module {
	return &Module{
		empty.EmptyModule{},
		newPlayerSkillSystem(availableSkills),
		&PassiveSkillSystem{time.Now(), time.Now()},
		&doSkillUp{},
	}
}

func (m Module) Name() string {
	return "Skills"
}

func (m Module) SaveLoaders() []core.SaveLoader {
	return []core.SaveLoader{
		m.playerSystem,
		m.passiveSystem,
	}
}

func (m *Module) DataHandlers() []core.DataHandlers {
	return []core.DataHandlers{
		{GetSkills, m.playerSystem.handleGetSkills},
		{GetXP, m.playerSystem.handleGetXP},
	}
}

func (m *Module) TimePassers() []core.TimePasser {
	return []core.TimePasser{m.passiveSystem}
}

func (m *Module) Observers() []core.ObserveEvent {
	return []core.ObserveEvent{
		{m.passiveSystem, meta.CreateGameEventName},
		{m.playerSystem, meta.CreateGameEventName},
	}
}

func (m *Module) Actions() []core.Actions {
	return []core.Actions{
		{DoLearnSkill, m.skillUp, emptyTemplate},
	}
}

func (m *Module) ManualEntries() []core.ManualEntry {
	return getManualEntries()
}
