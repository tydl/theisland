package skills

import "gitlab.com/tydl/theisland/viking/core"

type Difficulty int

const (
	Trivial   Difficulty = 1
	VeryEasy             = 2
	Easy                 = 4
	Medium               = 8
	Tough                = 12
	VeryTough            = 16
	Legendary            = 20
)

func (d Difficulty) String() string {
	switch d {
	case Trivial:
		return "Trivial"
	case VeryEasy:
		return "Very Easy"
	case Easy:
		return "Easy"
	case Medium:
		return "Medium"
	case Tough:
		return "Tough"
	case VeryTough:
		return "Very Tough"
	case Legendary:
		return "Legendary"
	default:
		return "Unknown"
	}
}

type Source string

const (
	Physical Source = "Physical"
	Mental          = "Mental"
	Magical         = "Magical"
)

func (d Source) String() string {
	switch d {
	case Physical:
		return "Physical"
	case Mental:
		return "Mental"
	case Magical:
		return "Magical"
	default:
		return "Unknown"
	}
}

type SkillMedium string

const (
	Self  SkillMedium = "Self"
	Terra             = "Terra"
	Fauna             = "Fauna"
)

func (d SkillMedium) String() string {
	switch d {
	case Self:
		return "Self"
	case Terra:
		return "Terra"
	case Fauna:
		return "Fauna"
	default:
		return "Unknown"
	}
}

type SkillEffectPayload interface{}

type Prerequesite struct {
	SkillId string
	Level   int
}

type Skill struct {
	Id            string
	Name          string
	Description   string
	Difficulty    Difficulty
	Prerequesites []Prerequesite
	Source        Source
	Medium        SkillMedium
	Applies       func(SkillEffectPayload) bool
	OnLevelUp     func(s core.Systemer) error
	// Starting position for GUI
	X int
	Y int
}
