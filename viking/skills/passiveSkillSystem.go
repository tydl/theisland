package skills

import (
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/meta"
)

const (
	PassiveSkillSystemId = "PassiveSkillSystem"
	saveLastTimeVersion  = 0
	timePeriod           = time.Hour
	pointGain            = 1
)

var _ core.SaveLoader = &PassiveSkillSystem{}
var _ core.TimePasser = &PassiveSkillSystem{}

type vPassiveZeroSave struct {
	lastEarned  time.Time
	currentTime time.Time
}

type PassiveSkillSystem struct {
	lastEarned  time.Time
	currentTime time.Time
}

func (*PassiveSkillSystem) Id() string {
	return PassiveSkillSystemId
}

func (p *PassiveSkillSystem) Save() (data interface{}, version int, err error) {
	return vPassiveZeroSave{p.lastEarned, p.currentTime}, saveLastTimeVersion, nil
}

func (p *PassiveSkillSystem) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	switch version {
	case saveLastTimeVersion:
		var vZero vPassiveZeroSave
		if err := loadFunction(&vZero); err != nil {
			return errors.StackWrap(err)
		}
		p.lastEarned = vZero.lastEarned
		p.currentTime = vZero.currentTime
	default:
		return errors.NewStackErrorStringf("Unknown version: %d", version)
	}
	return nil
}

func (p *PassiveSkillSystem) Observe(event core.Event, s core.Systemer) error {
	switch event.Name() {
	case meta.CreateGameEventName:
		p.lastEarned = time.Now()
		p.currentTime = p.lastEarned
	}
	return nil
}

func (p *PassiveSkillSystem) TimePass(duration time.Duration, systemer core.Systemer) error {
	pSkill, err := GetPlayerSkillSystem(systemer)
	if err != nil {
		return err
	}
	p.currentTime = p.currentTime.Add(duration)
	for p.currentTime.Sub(p.lastEarned) > timePeriod {
		pSkill.AddPhysicalPoints(pointGain)
		pSkill.AddMentalPoints(pointGain)
		pSkill.AddMagicalPoints(pointGain)
		pSkill.AddSelfPoints(pointGain)
		pSkill.AddTerraPoints(pointGain)
		pSkill.AddFaunaPoints(pointGain)
		p.lastEarned = p.lastEarned.Add(timePeriod)
	}
	return nil
}
