package storyteller

import (
	"fmt"
	"math/rand"
	"strings"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/templates"
)

func handleCreatureDescriptions(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	toShow := ""
	creatureDescs, err := creatureDescriptions(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	for _, desc := range creatureDescs {
		toShow = fmt.Sprintf("%s %s", toShow, desc)
	}
	return struct {
		Description string `json:"description"`
	}{
		Description: strings.TrimSpace(toShow),
	}, nil
}

func creatureDescriptions(s core.Systemer) ([]string, error) {
	playerSys, err := creature.GetPlayerSystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	npcSys, err := creature.GetNpcSystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	tempSys, err := templates.GetTemplateSystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}

	var toRet []string
	x, y := playerSys.GetPlayerLocation()
	nodes := npcSys.GetNpcsAt(x, y)
	for _, node := range nodes {
		idx := rand.Intn(len(node.DescriptionIds))
		desc, err := tempSys.Execute(node.DescriptionIds[idx])
		if err != nil {
			return nil, errors.StackWrap(err)
		}
		toRet = append(toRet, desc)
	}
	return toRet, nil
}
