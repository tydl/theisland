package storyteller

import (
	"fmt"
	"strings"
	"sync"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/encounter"
	"gitlab.com/tydl/theisland/viking/errors"
)

const (
	EventTellerSystemId = "EventTellerSystem"
)

var _ core.Observer = &EventTellerSystem{}

type EventTellerSystem struct {
	descriptionMutex *sync.Mutex
	description      string
}

func (EventTellerSystem) Id() string {
	return EventTellerSystemId
}

func (e *EventTellerSystem) Observe(event core.Event, s core.Systemer) error {
	switch event.Name() {
	case encounter.EncounterBeginEventName:
		encounterSys, err := encounter.GetEncounterSystem(s)
		if err != nil {
			return errors.StackWrap(err)
		}
		npcSys, err := creature.GetNpcSystem(s)
		if err != nil {
			return errors.StackWrap(err)
		}
		npc := npcSys.GetNpcForId(encounterSys.NpcId)
		if npc == nil {
			return errors.NewStackErrorStringf("No NPC for id %d", encounterSys.NpcId)
		}
		e.descriptionMutex.Lock()
		defer e.descriptionMutex.Unlock()
		e.description = strings.TrimSpace(fmt.Sprintf("%s You are now in an encounter with a %s!", e.description, npc.Kind))
	default:
	}
	return nil
}

func (e *EventTellerSystem) handleGetEvents(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	e.descriptionMutex.Lock()
	defer e.descriptionMutex.Unlock()
	toShow := e.description
	e.description = ""
	return struct {
		Description string `json:"description"`
	}{
		Description: toShow,
	}, nil
}
