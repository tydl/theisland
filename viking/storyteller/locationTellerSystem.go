package storyteller

import (
	"fmt"
	"math/rand"
	"strings"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/mapgen"
	"gitlab.com/tydl/theisland/viking/world"
)

const (
	LocationTellerSystemId = "LocationTellerSystem"
	lastBiomeToShowVersion = 0
)

var _ core.SaveLoader = &LocationTellerSystem{}
var _ core.Observer = &LocationTellerSystem{}

type LocationTellerSystem struct {
	lastBiome              mapgen.Biome
	biomeDescriptionToShow string
}

func (LocationTellerSystem) Id() string {
	return LocationTellerSystemId
}

type saveV0 struct {
	Biome  string
	ToShow string
}

func (l LocationTellerSystem) Save() (data interface{}, version int, err error) {
	return saveV0{
		Biome:  l.lastBiome.Name(),
		ToShow: l.biomeDescriptionToShow,
	}, lastBiomeToShowVersion, nil
}

func (l *LocationTellerSystem) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	switch version {
	case lastBiomeToShowVersion:
		var s saveV0
		if err := loadFunction(&s); err != nil {
			return errors.StackWrap(err)
		}
		l.lastBiome = mapgen.FromString(s.Biome)
		l.biomeDescriptionToShow = s.ToShow
	}
	return nil
}

func (l *LocationTellerSystem) Observe(e core.Event, s core.Systemer) error {
	pSys, err := creature.GetPlayerSystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	mapSys, err := world.GetMapSystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	switch e.Name() {
	case creature.PlayerMovedEventName:
		nextBiome := mapSys.GetBiome(pSys.GetPlayerLocation())
		if nextBiome.Name() != l.lastBiome.Name() {
			if err := l.setBiomeDescriptionToShow(nextBiome); err != nil {
				return errors.StackWrap(err)
			}
			l.lastBiome = nextBiome
		}
	case creature.PlayerSpawnedEventName:
		l.lastBiome = mapSys.GetBiome(pSys.GetPlayerLocation())
	}
	return nil
}

func (l *LocationTellerSystem) handleGetLocation(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	toShow := l.biomeDescriptionToShow
	if doPreserve, isPresent := vars.Bool("preserve"); !doPreserve || !isPresent {
		l.biomeDescriptionToShow = ""
	}
	return struct {
		Description string `json:"description"`
	}{
		Description: strings.TrimSpace(toShow),
	}, nil
}

func (l *LocationTellerSystem) setBiomeDescriptionToShow(new mapgen.Biome) error {
	var err error
	l.biomeDescriptionToShow, err = l.biomeTransitionDescription(l.lastBiome, new)
	return err
}

func (l *LocationTellerSystem) biomeTransitionDescription(from, to mapgen.Biome) (string, error) {
	fromDescs, err := l.biomeFromDescription(from)
	if err != nil {
		return "", errors.StackWrap(err)
	}
	toDescs, err := l.biomeToDescription(to)
	if err != nil {
		return "", errors.StackWrap(err)
	}
	fromDesc := fromDescs[rand.Intn(len(fromDescs))]
	toDesc := toDescs[rand.Intn(len(toDescs))]
	return fmt.Sprintf("%s %s.", fromDesc, toDesc), nil
}

func (l *LocationTellerSystem) biomeFromDescription(b mapgen.Biome) ([]string, error) {
	var desc []string
	switch b.Name() {
	case mapgen.OceanName:
		desc = []string{
			"The salty waves disappear into",
		}
	case mapgen.MarshName:
		desc = []string{
			"The waist-high water grassy landscape turns into",
		}
	case mapgen.IceName:
		desc = []string{
			"The slippery footing gives way to",
		}
	case mapgen.LakeName:
		desc = []string{
			"The gentle freshwater lake recedes into",
		}
	case mapgen.BeachName:
		desc = []string{
			"The sandy shore becomes",
		}
	case mapgen.SnowName:
		desc = []string{
			"The powdered snow fades into",
		}
	case mapgen.TundraName:
		desc = []string{
			"The cold, rocky landscape turns into",
		}
	case mapgen.BareName:
		desc = []string{
			"The barren, cold, rocky surroundings become",
		}
	case mapgen.ScorchedName:
		desc = []string{
			"The desolate and charred land turns into",
		}
	case mapgen.TaigaName:
		desc = []string{
			"The seemingly-endless snowy-tree landscape ends, becoming",
		}
	case mapgen.ShrublandName:
		desc = []string{
			"The arid and scruffy terrain becomes",
		}
	case mapgen.TemperateDesertName:
		desc = []string{
			"The hot, arid, and rocky ground gives way to",
		}
	case mapgen.TemperateRainforestName:
		desc = []string{
			"The warm, lush environment turns into",
		}
	case mapgen.TemperateDeciduousForestName:
		desc = []string{
			"The vibrant vegetation gives way to",
		}
	case mapgen.GrasslandName:
		desc = []string{
			"The grassy hills end, yielding",
		}
	case mapgen.TropicalRainforestName:
		desc = []string{
			"The hot and humid overgrowth of vegetation subsides, turning into",
		}
	case mapgen.TropicalSeasonalForestName:
		desc = []string{
			"The hot and humid environment becomes",
		}
	case mapgen.SubtropicalDesertName:
		desc = []string{
			"The arid desert fades into the background, giving way to",
		}
	default:
		return nil, errors.NewStackErrorStringf("unhandled biome '%s'", b.Name())
	}
	return desc, nil
}

func (l *LocationTellerSystem) biomeToDescription(b mapgen.Biome) ([]string, error) {
	var desc []string
	switch b.Name() {
	case mapgen.OceanName:
		desc = []string{
			"rolling salty waves",
		}
	case mapgen.MarshName:
		desc = []string{
			"a waist-high water grassy landscape",
		}
	case mapgen.IceName:
		desc = []string{
			"a landscape of ice and slippery footing",
		}
	case mapgen.LakeName:
		desc = []string{
			"a gentle freshwater lake",
		}
	case mapgen.BeachName:
		desc = []string{
			"a sandy shore",
		}
	case mapgen.SnowName:
		desc = []string{
			"powdered snow",
		}
	case mapgen.TundraName:
		desc = []string{
			"a cold, rocky landscape",
		}
	case mapgen.BareName:
		desc = []string{
			"a barren, cold, and rocky environment",
		}
	case mapgen.ScorchedName:
		desc = []string{
			"desolate and charred land",
		}
	case mapgen.TaigaName:
		desc = []string{
			"a seemingly-endless snowy-tree landscape",
		}
	case mapgen.ShrublandName:
		desc = []string{
			"an arid and scruffy terrain",
		}
	case mapgen.TemperateDesertName:
		desc = []string{
			"a hot, arid, and rocky environment",
		}
	case mapgen.TemperateRainforestName:
		desc = []string{
			"a warm, lush environment with heavy vegetation overgrowth",
		}
	case mapgen.TemperateDeciduousForestName:
		desc = []string{
			"a forest of vibrant vegetation",
		}
	case mapgen.GrasslandName:
		desc = []string{
			"many rolling grassy hills",
		}
	case mapgen.TropicalRainforestName:
		desc = []string{
			"a hot and humid overgrowth of vegetation",
		}
	case mapgen.TropicalSeasonalForestName:
		desc = []string{
			"a hot and humid environment",
		}
	case mapgen.SubtropicalDesertName:
		desc = []string{
			"an arid desert",
		}
	default:
		return nil, errors.NewStackErrorStringf("unhandled biome '%s'", b.Name())
	}
	return desc, nil
}
