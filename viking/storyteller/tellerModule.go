package storyteller

import (
	"sync"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/empty"
	"gitlab.com/tydl/theisland/viking/encounter"
)

const (
	GetLocation = "/describe/me/location"
	GetEvents   = "/describe/events"
	GetNodes    = "/describe/me/location/resourceNodes"
	GetCreature = "/describe/me/location/creatures"
)

var _ core.Moduler = &Module{}

type Module struct {
	empty.EmptyModule
	locTeller   *LocationTellerSystem
	eventTeller *EventTellerSystem
}

func NewModule() *Module {
	return &Module{
		empty.EmptyModule{},
		&LocationTellerSystem{},
		&EventTellerSystem{&sync.Mutex{}, ""},
	}
}

func (Module) Name() string {
	return "Storyteller"
}

func (w Module) SaveLoaders() []core.SaveLoader {
	return []core.SaveLoader{
		w.locTeller,
	}
}

func (w Module) DataHandlers() []core.DataHandlers {
	return []core.DataHandlers{
		{GetLocation, w.locTeller.handleGetLocation},
		{GetEvents, w.eventTeller.handleGetEvents},
		{GetNodes, handleNodeDescriptions},
		{GetCreature, handleCreatureDescriptions},
	}
}

func (w Module) Observers() []core.ObserveEvent {
	return []core.ObserveEvent{
		{w.locTeller, creature.PlayerSpawnedEventName},
		{w.locTeller, creature.PlayerMovedEventName},
		{w.eventTeller, encounter.EncounterBeginEventName},
	}
}
