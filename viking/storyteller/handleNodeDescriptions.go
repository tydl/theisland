package storyteller

import (
	"fmt"
	"math/rand"
	"strings"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/resources"
	"gitlab.com/tydl/theisland/viking/templates"
)

func handleNodeDescriptions(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	toShow := ""
	descs, err := resourceNodeDescriptions(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	for _, desc := range descs {
		toShow = fmt.Sprintf("%s %s", toShow, desc)
	}
	return struct {
		Description string `json:"description"`
	}{
		Description: strings.TrimSpace(toShow),
	}, nil
}

func resourceNodeDescriptions(s core.Systemer) ([]string, error) {
	playerSys, err := creature.GetPlayerSystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	tempSys, err := templates.GetTemplateSystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	resSys, err := resources.GetResourceSystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	var toRet []string
	x, y := playerSys.GetPlayerLocation()
	nodes := resSys.GetNodesAt(x, y)
	for _, node := range nodes {
		idx := rand.Intn(len(node.Text.DescriptionIds))
		desc, err := tempSys.Execute(node.Text.DescriptionIds[idx])
		if err != nil {
			return nil, errors.StackWrap(err)
		}
		toRet = append(toRet, desc)
	}
	return toRet, nil
}
