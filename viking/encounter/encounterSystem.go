package encounter

import (
	"math/rand"
	"strings"
	"time"

	"gitlab.com/tydl/theisland/viking/anatomy"
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/meta"
	"gitlab.com/tydl/theisland/viking/skills"
)

const (
	EncounterSystemId      = "EncounterSystem"
	saveActiveNpcIdVersion = 0
	runAwayBaseChance      = 20
)

var _ core.SaveLoader = &EncounterSystem{}
var _ core.TimePasser = &EncounterSystem{}

type Action interface {
	Name() string
	Description() string
	Execute(s core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (string, time.Duration, error)
	IsApplicableForPlayer(s core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (bool, error)
}

type CombatHandler interface {
	ShouldAct(timeSinceLastActed time.Duration, rng *rand.Rand) bool
	GetAction(self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head, rng *rand.Rand) Action
	OnVictory(self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head, rng *rand.Rand, s core.Systemer) error
	OnDefeat(self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head, rng *rand.Rand, s core.Systemer) error
	OnTie(self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head, rng *rand.Rand, s core.Systemer) error
}

type EncounterSystem struct {
	Active             bool
	NpcId              int
	TimeSinceLastActed time.Duration
	rng                *rand.Rand
	CombatAi           map[string]CombatHandler
	PlayerActions      []Action
	latestDescription  string
}

func newEncounterSystem(handlers map[string]CombatHandler, playerActions []Action) *EncounterSystem {
	return &EncounterSystem{
		Active:             false,
		NpcId:              -1,
		TimeSinceLastActed: 0,
		CombatAi:           handlers,
		PlayerActions:      playerActions,
	}
}

func (*EncounterSystem) Id() string {
	return EncounterSystemId
}

type saveV0 struct {
	Active             bool
	TimeSinceLastActed time.Duration
	NpcId              int
	RngSeed            int64
}

func (e *EncounterSystem) Save() (data interface{}, version int, err error) {
	return saveV0{
		Active:             e.Active,
		NpcId:              e.NpcId,
		TimeSinceLastActed: e.TimeSinceLastActed,
		RngSeed:            e.rng.Int63(),
	}, saveActiveNpcIdVersion, nil
}

func (e *EncounterSystem) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	switch version {
	case saveActiveNpcIdVersion:
		var s saveV0
		if err := loadFunction(&s); err != nil {
			return errors.StackWrap(err)
		}
		e.Active = s.Active
		e.NpcId = s.NpcId
		e.TimeSinceLastActed = s.TimeSinceLastActed
		e.rng = rand.New(rand.NewSource(s.RngSeed))
	default:
		return errors.NewStackErrorStringf("unknown version %d", version)
	}
	return nil
}

func (e *EncounterSystem) Observe(event core.Event, s core.Systemer) error {
	switch event.Name() {
	case meta.CreateGameEventName:
		event, ok := event.(*meta.CreateGameEvent)
		if !ok {
			return errors.NewStackErrorString("event not *meta.CreateGameEvent")
		}
		e.rng = rand.New(rand.NewSource(event.Seed))
		e.Active = false
		e.NpcId = -1
	}
	return nil
}

func (e *EncounterSystem) TimePass(time time.Duration, s core.Systemer) error {
	if !e.Active {
		return nil
	}
	npcSys, err := creature.GetNpcSystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	playerSys, err := creature.GetPlayerSystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	anatomySys, err := anatomy.GetPlayerAnatomySystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	npcAnatomySys, err := anatomy.GetNpcAnatomySystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	npc := npcSys.GetNpcForId(e.NpcId)
	if npc == nil {
		return errors.NewStackErrorStringf("No npc for id %d", e.NpcId)
	}
	e.TimeSinceLastActed += time
	combatAi, ok := e.CombatAi[npc.Kind]
	if !ok {
		return errors.NewStackErrorStringf("No combat AI for kind %s", npc.Kind)
	}
	if !combatAi.ShouldAct(e.TimeSinceLastActed, e.rng) {
		return nil
	}
	npcAnatomy := npcAnatomySys.GetAnatomyForInstance(npc.Id)
	if npcAnatomy == nil {
		return errors.NewStackErrorStringf("No anatomy for npc id %d", npc.Id)
	}
	action := combatAi.GetAction(&npc.Creature, playerSys.GetPlayerCreature(), npcAnatomy, anatomySys.Anatomy(), e.rng)
	description, _, err := action.Execute(s, &npc.Creature, playerSys.GetPlayerCreature(), npcAnatomy, anatomySys.Anatomy())
	if err != nil {
		return errors.StackWrap(err)
	}
	additionalDescription, err := e.evaluateEncounterEnd(playerSys.GetPlayerCreature(), npc, anatomySys.Anatomy(), npcAnatomy, combatAi, s)
	if err != nil {
		return errors.StackWrap(err)
	}
	e.latestDescription = strings.TrimSpace(strings.Join([]string{e.latestDescription, description}, " "))
	e.latestDescription = strings.TrimSpace(strings.Join([]string{e.latestDescription, additionalDescription}, " "))
	return nil
}

func (e *EncounterSystem) handleEncounterDescription(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	toRet := struct{ Description string }{Description: e.latestDescription}
	e.latestDescription = ""
	return toRet, nil
}

type playerAction struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

func (e *EncounterSystem) handleEncounterInformation(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	if !e.IsInEncounter() {
		return map[string]interface{}{
			"inEncounter": false,
		}, nil
	}
	npcSys, err := creature.GetNpcSystem(s)
	if err != nil {
		return map[string]interface{}{}, errors.StackWrap(err)
	}
	npc := npcSys.GetNpcForId(e.NpcId)
	if npc == nil {
		return map[string]interface{}{}, errors.NewStackErrorStringf("No npc for id %d", e.NpcId)
	}
	var playerActions []playerAction
	for _, pAction := range e.PlayerActions {
		playerActions = append(playerActions, playerAction{pAction.Name(), pAction.Description()})
	}
	return map[string]interface{}{
		"inEncounter":   true,
		"runChance":     e.getRunAwayChance(s, npc.Kind),
		"playerActions": playerActions,
		"opponent":      npc,
	}, nil
}

func (e *EncounterSystem) getRunAwayChance(s core.Systemer, npcKind string) int {
	level := skills.GetTotalLevelsLearnedForPayload(s, RunAwayOnePercentBonusPayload{
		npcKind,
	})
	return level + runAwayBaseChance
}

func (e *EncounterSystem) BeginEncounter(npcId int) error {
	if e.Active {
		return errors.NewStackErrorStringf("Already in encounter with %d, cannot start with %d", e.NpcId, npcId)
	}
	e.Active = true
	e.NpcId = npcId
	return nil
}

func (e *EncounterSystem) IsInEncounter() bool {
	return e.Active
}

func (e *EncounterSystem) PlayerAction(s core.Systemer, actionName string) (string, time.Duration, error) {
	npcSys, err := creature.GetNpcSystem(s)
	if err != nil {
		return "", 0, errors.StackWrap(err)
	}
	playerSys, err := creature.GetPlayerSystem(s)
	if err != nil {
		return "", 0, errors.StackWrap(err)
	}
	anatomySys, err := anatomy.GetPlayerAnatomySystem(s)
	if err != nil {
		return "", 0, errors.StackWrap(err)
	}
	npcAnatomySys, err := anatomy.GetNpcAnatomySystem(s)
	if err != nil {
		return "", 0, errors.StackWrap(err)
	}
	npc := npcSys.GetNpcForId(e.NpcId)
	if npc == nil {
		return "", 0, errors.NewStackErrorStringf("No npc for id %d", e.NpcId)
	}
	combatAi, ok := e.CombatAi[npc.Kind]
	if !ok {
		return "", 0, errors.NewStackErrorStringf("No combat AI for kind %s", npc.Kind)
	}
	var playerAction Action
	for _, action := range e.PlayerActions {
		if action.Name() == actionName {
			playerAction = action
			break
		}
	}
	if playerAction == nil {
		return "", 0, errors.NewStackErrorStringf("No action with name %s", actionName)
	}
	npcAnatomy := npcAnatomySys.GetAnatomyForInstance(npc.Id)
	if npcAnatomy == nil {
		return "", 0, errors.NewStackErrorStringf("No anatomy for npc id %d", npc.Id)
	}
	description, duration, err := playerAction.Execute(s, playerSys.GetPlayerCreature(), &npc.Creature, anatomySys.Anatomy(), npcAnatomy)
	if err != nil {
		return "", 0, errors.StackWrap(err)
	}
	additionalDescription, err := e.evaluateEncounterEnd(playerSys.GetPlayerCreature(), npc, anatomySys.Anatomy(), npcAnatomy, combatAi, s)
	return strings.TrimSpace(strings.Join([]string{description, additionalDescription}, " ")), duration, errors.StackWrap(err)
}

func (e *EncounterSystem) AttemptPcRunAway(s core.Systemer) (string, error) {
	npcSys, err := creature.GetNpcSystem(s)
	if err != nil {
		return "", errors.StackWrap(err)
	}
	playerSys, err := creature.GetPlayerSystem(s)
	if err != nil {
		return "", errors.StackWrap(err)
	}
	anatomySys, err := anatomy.GetPlayerAnatomySystem(s)
	if err != nil {
		return "", errors.StackWrap(err)
	}
	npcAnatomySys, err := anatomy.GetNpcAnatomySystem(s)
	if err != nil {
		return "", errors.StackWrap(err)
	}
	npc := npcSys.GetNpcForId(e.NpcId)
	if npc == nil {
		return "", errors.NewStackErrorStringf("No npc for id %d", e.NpcId)
	}
	combatAi, ok := e.CombatAi[npc.Kind]
	if !ok {
		return "", errors.NewStackErrorStringf("No combat AI for kind %s", npc.Kind)
	}
	npcAnatomy := npcAnatomySys.GetAnatomyForInstance(npc.Id)
	if npcAnatomy == nil {
		return "", errors.NewStackErrorStringf("No anatomy for npc id %d", npc.Id)
	}
	roll := e.rng.Intn(100)
	success := roll < e.getRunAwayChance(s, npc.Kind)
	if !success {
		return "You failed to run away.", nil
	}
	additionalDescription, err := e.evaluateEncounterEnd(playerSys.GetPlayerCreature(), npc, anatomySys.Anatomy(), npcAnatomy, combatAi, s)
	if err != nil {
		return "", errors.StackWrap(err)
	}
	if len(additionalDescription) != 0 {
		return additionalDescription, nil
	}
	e.endEncounter()
	return "You have successfully run away.", nil
}

func (e *EncounterSystem) evaluateEncounterEnd(player *creature.Creature, npc *creature.NpcCreature, pcAnat, npcAnat *anatomy.Head, npcCombatAi CombatHandler, s core.Systemer) (string, error) {
	playerLose := player.IsDead() ||
		player.IsUnconscious() ||
		player.IsSubmissive()
	npcLose := npc.IsDead() ||
		npc.IsUnconscious() ||
		npc.IsSubmissive()
	playerDesc := ""
	npcDesc := ""
	reaperSys := creature.GetReaperSystemOrPanic(s)
	if playerLose || npcLose {
		e.endEncounter()
		playerDesc = reaperSys.GetPcDeathDescription(player)
		npcDesc = reaperSys.GetNpcDeathDescription(&npc.Creature, npc.Kind)
		if !player.IsDead() {
			playerDesc = strings.TrimSpace(strings.Join([]string{playerDesc, reaperSys.GetPcSubmissiveDescription()}, " "))
			playerDesc = strings.TrimSpace(strings.Join([]string{playerDesc, reaperSys.GetPcUnconsciousDescription()}, " "))
		} else {
			reaperSys.SetPcDead()
		}
		if !npc.IsDead() {
			npcDesc = strings.TrimSpace(strings.Join([]string{npcDesc, reaperSys.GetNpcDeathDescription(&npc.Creature, npc.Kind)}, " "))
			npcDesc = strings.TrimSpace(strings.Join([]string{npcDesc, reaperSys.GetNpcDeathDescription(&npc.Creature, npc.Kind)}, " "))
		}
	}
	if playerLose && npcLose {
		err := npcCombatAi.OnTie(&npc.Creature, player, npcAnat, pcAnat, e.rng, s)
		return strings.TrimSpace(strings.Join([]string{npcDesc, playerDesc}, " ")), errors.StackWrap(err)
	} else if playerLose {
		err := npcCombatAi.OnVictory(&npc.Creature, player, npcAnat, pcAnat, e.rng, s)
		return playerDesc, errors.StackWrap(err)
	} else if npcLose {
		err := npcCombatAi.OnDefeat(&npc.Creature, player, npcAnat, pcAnat, e.rng, s)
		return npcDesc, errors.StackWrap(err)
	}
	return "", nil
}

func (e *EncounterSystem) endEncounter() {
	e.Active = false
	e.NpcId = -1
}

func GetEncounterSystem(s core.Systemer) (*EncounterSystem, error) {
	system, err := s.System(EncounterSystemId)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	casted, ok := system.(*EncounterSystem)
	if !ok {
		return nil, errors.NewStackErrorString("could not cast to *EncounterSystem")
	}
	return casted, nil
}
