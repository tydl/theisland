package encounter

import "gitlab.com/tydl/theisland/viking/core"

const (
	EncounterBeginEventName = "EncounterBeginEvent"
)

var _ core.Event = &EncounterBeginEvent{}

type EncounterBeginEvent struct {
}

func (EncounterBeginEvent) Name() string {
	return EncounterBeginEventName
}
