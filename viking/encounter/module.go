package encounter

import (
	"html/template"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/empty"
	"gitlab.com/tydl/theisland/viking/meta"
)

const (
	GetEncounterDescription = "/encounter/description"
	GetEncounterInformation = "/encounter"
	DoPlayerAction          = "/encounter/player/do"
	AttemptRunAway          = "/encounter/player/run"
)

var _ core.Moduler = &Module{}
var emptyTemplate *template.Template = template.Must(template.New("").Parse("{{.Content}}"))

type Module struct {
	empty.EmptyModule
	encounterSystem  *EncounterSystem
	doPlayerAction   *doAction
	attemptPcRunAway *attemptPcRunAwayAction
}

func NewModule(handlers map[string]CombatHandler, playerActions []Action) *Module {
	return &Module{
		empty.EmptyModule{},
		newEncounterSystem(handlers, playerActions),
		&doAction{},
		&attemptPcRunAwayAction{},
	}
}

func (m Module) Name() string {
	return "Encounter"
}

func (m Module) SaveLoaders() []core.SaveLoader {
	return []core.SaveLoader{
		m.encounterSystem,
	}
}

func (m Module) TimePassers() []core.TimePasser {
	return []core.TimePasser{
		m.encounterSystem,
	}
}

func (m Module) Observers() []core.ObserveEvent {
	return []core.ObserveEvent{
		{m.encounterSystem, meta.CreateGameEventName},
	}
}

func (m *Module) DataHandlers() []core.DataHandlers {
	return []core.DataHandlers{
		{GetEncounterDescription, m.encounterSystem.handleEncounterDescription},
		{GetEncounterInformation, m.encounterSystem.handleEncounterInformation},
	}
}

func (m *Module) Actions() []core.Actions {
	return []core.Actions{
		{DoPlayerAction, m.doPlayerAction, emptyTemplate},
		{AttemptRunAway, m.attemptPcRunAway, emptyTemplate},
	}
}

func (Module) ManualEntries() []core.ManualEntry {
	return getManualEntries()
}
