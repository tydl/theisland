package encounter

import (
	"encoding/json"
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/errors"
)

var _ core.Acter = &doAction{}

type doAction struct{}

type actionParams struct {
	ActionName string `json:"actionName"`
}

func (doAction) Act(vars core.RequestVariables, game core.Systemer) (map[string]interface{}, time.Duration, error) {
	if creature.GetReaperSystemOrPanic(game).IsPcDead() {
		return nil, 0, errors.NewStackErrorString("PC is dead")
	}
	encounterSystem, err := GetEncounterSystem(game)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	if !encounterSystem.IsInEncounter() {
		return nil, 0, errors.NewStackErrorString("Not in an encounter")
	}
	body, ok := vars.String("body")
	if !ok {
		return nil, 0, errors.NewStackErrorString("No body")
	}
	var actionSpec actionParams
	if err = json.Unmarshal([]byte(body), &actionSpec); err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	description, duration, err := encounterSystem.PlayerAction(game, actionSpec.ActionName)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	return map[string]interface{}{
		"Content": description,
	}, duration, nil
}
