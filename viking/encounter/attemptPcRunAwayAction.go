package encounter

import (
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/errors"
)

var _ core.Acter = &attemptPcRunAwayAction{}

type attemptPcRunAwayAction struct{}

func (attemptPcRunAwayAction) Act(vars core.RequestVariables, game core.Systemer) (map[string]interface{}, time.Duration, error) {
	if creature.GetReaperSystemOrPanic(game).IsPcDead() {
		return nil, 0, errors.NewStackErrorString("PC is dead")
	}
	encounterSystem, err := GetEncounterSystem(game)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	if !encounterSystem.IsInEncounter() {
		return nil, 0, errors.NewStackErrorString("Not in an encounter")
	}
	description, err := encounterSystem.AttemptPcRunAway(game)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	return map[string]interface{}{
		"Content": description,
	}, time.Second * 5, nil
}
