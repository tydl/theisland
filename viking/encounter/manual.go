package encounter

import "gitlab.com/tydl/theisland/viking/core"

func getManualEntries() []core.ManualEntry {
	return []core.ManualEntry{
		{
			Title: "Encounter",
			Text: "An encounter occurs between a PC and another creature when they " +
				"occupy the same location. The encounter continues until one side claims " +
				"victory over the other, who admits defeat. During an encounter, both the " +
				"PC and the opponent engage using encounter actions. The PC also has the " +
				"option to run away from the encounter if it proves to be too much of a risk to " +
				"survival. At the end of an encounter, the PC is usually rewarded for a " +
				"victory and sometimes penalized for a defeat.",
		},
		{
			Title: "Action",
			Text: "In an encounter both a PC and his or her opponent take actions to " +
				"affect one another. The actions usually help each to survive the encounter " +
				"and emerge in a superior position over the other. A player's action generally " +
				"takes time, which can affect how many and which actions an opponent takes in " +
				"an encounter. The player has the option to run away from an encounter as an " +
				"action.",
		},
		{
			Title: "Run Away",
			Text: "In an encounter the PC can run away to preserve him or herself. If " +
				"survival or success in an encounter with another creature seems slim, " +
				"there is no disgrace in turning tail and fleeing.",
		},
		{
			Title: "Opponent",
			Text: "In an encounter, the creature the PC is facing is his or her opponent. " +
				"Victory over the opponent usually has rewards, while defeat can sometimes " +
				"result in penalties. Opponents act after a certain amount of time, so the " +
				"longer a PC takes in an encounter the more often the opponent will retaliate.",
		},
		{
			Title: "Victory",
			Text: "In an encounter, one side is victorious when their opponent is either " +
				"dead, submissive, or unconscious. That opponent admits defeat.",
		},
		{
			Title: "Defeat",
			Text: "In an encounter, one side is defeated when they are either dead, " +
				"submissive, or unconscious. Their opponent is victorious.",
		},
	}
}
