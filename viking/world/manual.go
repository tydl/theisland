package world

import "gitlab.com/tydl/theisland/viking/core"

func getManualEntries() []core.ManualEntry {
	return []core.ManualEntry{
		{
			Title: "World",
			Text: "The world is a procedurally generated island based off of a seed. " +
				"The procedural process includes calculating island elevation, moisture, " +
				"rivers, and biomes. The procedural process ensures that " +
				"biomes occur realistically, in a repeatable way with the same seed, and " +
				"differently across different seeds. A biome is perhaps the most significant " +
				"part to come out of the world's generation. It can affect skills, exposure " +
				"to elements, spawns, resources, and many other aspects to survival. Once a " +
				"world is generated, the biome and general climate of the island remains " +
				"constant throughout the game.",
		},
		{
			Title: "Biome",
			Text: "A biome determines the general climate for a particular area. It is " +
				"determined at game creation time and remains constant throughout the game. " +
				"Specific biomes can be for either land or water. Water biomes include ocean, " +
				"lake, and marsh. Land biomes can be ice, beach, snow, tundra, bare, scorched, " +
				"taiga, shrubland, temperate desert, temperate rainforest, temperate deciduous " +
				"forest, grassland, tropical rainforest, tropical seasonal forest, or subtropical " +
				"desert. Biomes can affect many aspects of survival including resource availability, " +
				"exposure to elements, and difficulty of movement.",
		},
		{
			Title: "Elevation",
			Text: "Elevation is the height of the island above sea level. It is generated " +
				"procedurally such that the center of the island is at a higher elevation than " +
				"the surrounding landscape. Higher elevations directly correlate to cooler " +
				"temperatures, impacting the kinds of biome a section of land will receive. " +
				"Elevation is determined independently of moisture.",
		},
		{
			Title: "Moisture",
			Text: "Moisture is the amount of water contained in the climate. It includes " +
				"water vapor as well as precipitation. Moisture is procedurally generated on " +
				"the island, and is provided by a nearby water biome or river. The presence or " +
				"absence of mositure impacts the kind of biome a section of land will receive. " +
				"Moisture is determined independently of elevation, but the island's elevation " +
				"does impact the flow of rivers.",
		},
		{
			Title: "River",
			Text: "A source of fresh water found flowing from high elevations to low " +
				"elevations, eventually ending in a water biome. Rivers provide moisture to " +
				"surrounding sections of land and therefore impact the kind of biomes nearby.",
		},
		{
			Title: "Ocean",
			Text:  "A salt water biome dominated by the big blue salty endless waves of water.",
		},
		{
			Title: "Marsh",
			Text: "A fresh water biome at very low elevation. It contains a wet grassy " +
				"underfooting and, unlike the saltwater marsh, contains no peat and no " +
				"active tides.",
		},
		{
			Title: "Ice",
			Text: "A fresh water biome at very high elevation. It typically contains " +
				"very little plant and animal life. The endless white sheet of ice dominates " +
				"the landscape.",
		},
		{
			Title: "Lake",
			Text: "A fresh water biome at moderate elevations. It can contain animals " +
				"in the body of water itself and supports life in the surrounding area.",
		},
		{
			Title: "Beach",
			Text: "A land biome only present where the land meets the ocean. It is subject " +
				"to the tides of the ocean and is dominated by fine sandy shores as well as rocky " +
				"outcrops.",
		},
		{
			Title: "Snow",
			Text: "A wet land biome found at high elevations. It contains vegetation " +
				"and wildlife native to very cold habitats, but the vegetation generally does not " +
				"include deep-rooted plants such as trees due to the elevation. The ground " +
				"is generally covered by snow year round.",
		},
		{
			Title: "Tundra",
			Text: "A land biome found at high elevations. It contains vegetation and " +
				"wildlife native to very cold habitats, but the vegetation generally does not " +
				"include deep-rooted plants such as trees due to the elevation. The ground " +
				"is a mixture of rocky and snowy, as it is neither a wet nor dry biome. " +
				"However, the ground does generally remain in a state of permafrost.",
		},
		{
			Title: "Bare",
			Text: "A dry land biome found at high elevations. It is sparsely populated " +
				"with vegetation and wildlife native to very cold habitats. The vegetation does not " +
				"generally include deep-rooted plants such as trees due to the elevation. The " +
				"ground is a rocky permafrost with the rare patch of snow.",
		},
		{
			Title: "Scorched",
			Text: "An extremely dry land biome found at high elevations. It contains " +
				"almost no vegetation and wildlife due to the extreme elevation and lack of " +
				"moisture. The ground is rocky permafrost.",
		},
		{
			Title: "Taiga",
			Text: "A wet land biome found at moderately high elevations. It contains tall " +
				"deep rooted vegetation and wildlife native to cold habitats. The ground can " +
				"occasionally thaw but is otherwise covered in snow.",
		},
		{
			Title: "Shrubland",
			Text: "A dry land biome found at moderately high elevations. It contains short " +
				"deep rooted vegetation and wildlife native to cold habitats. The ground is rocky " +
				"with the occasional patch of snow, and does thaw.",
		},
		{
			Title: "Temperate Desert",
			Text: "A very dry land biome found at moderate elevations. It contains very " +
				"little vegetation and wildlife due to the lack of moisture, but short vegetation " +
				"can be deep rooted. While hot during the day, it can get very cold at night. " +
				"Unlike subtropical deserts, temperate deserts receive slightly more moisture " +
				"but are subject to extreme temperature swings.",
		},
		{
			Title: "Temperate Rainforest",
			Text: "A wet land biome found at moderately low elevations. It contains " +
				"extensive vegetation and wildlife, including deep-rooted vegetation. It is " +
				"generally cooler than the tropical rainforest, but no less wet. Vegetation " +
				"consists of an extensive forest canopy as well as significant undergrowth.",
		},
		{
			Title: "Temperate Deciduous Forest",
			Text: "A land biome found at moderately low elevations. It contains deep-rooted " +
				"vegetation and wildlife. It does not have extreme temperatures nor is too " +
				"wet nor dry, allowing large forests without significant undergrowth. Temperate " +
				"deciduous forests tend to be cooler than their tropical seasonal forest " +
				"counterparts, with the undergrowth closely mirroring that of a cool meadow.",
		},
		{
			Title: "Grassland",
			Text: "A dry land biome found at low and moderately low elevations. It contains " +
				"extensive vegetation and wildlife, but the deep-rooted vegetation is usually " +
				"small. It is generally warm but is subject to temperature swings, though not " +
				"as extreme as in a temperate desert.",
		},
		{
			Title: "Tropical Rainforest",
			Text: "A wet land biome found at low elevations. It contains extensive " +
				"vegetation and wildlife, including deep-rooted vegetation. It is hotter " +
				"than the temperate rainforest, but no less wet. Vegetation consists of " +
				"an extensive forested canopy as well as significant undergrowth.",
		},
		{
			Title: "Tropical Seasonal Forest",
			Text: "A land biome found at low elevations. It contains deep-rooted " +
				"vegetation and wildlife. It does not have extreme temperatures nor is too " +
				"wet nor dry, allowing large forests without significant undergrowth. Tropical " +
				"seasonal forests tend to be hotter than their temperate deciduous forest " +
				"counterparts, with the undergrowth closely mirroring that of dry grassland.",
		},
		{
			Title: "Subtropical Desert",
			Text: "A very dry land biome found at low elevations. It contains very " +
				"little vegetation and wildlife due to the lack of moisture and extreme heat. " +
				"Vegetation is not deep rooted. Unlike temperate deserts, subtropical deserts " +
				"receive even less moisture and have hotter average temperatures.",
		},
	}
}
