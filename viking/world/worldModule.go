package world

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/empty"
	"gitlab.com/tydl/theisland/viking/meta"
)

const (
	GetElevation        = "/world/elevation"
	GetBiome            = "/world/biome"
	GetRiverSize        = "/world/riverSize"
	GetWorldImage       = "/world/image"
	GetDownslopeImage   = "/world/downslope/image"
	GetCoastPointsImage = "/world/coast/points/image"
)

var _ core.Moduler = &WorldModule{}

type WorldModule struct {
	empty.EmptyModule
	mapSys *MapSystem
}

func NewWorldModule() *WorldModule {
	return &WorldModule{
		empty.EmptyModule{},
		&MapSystem{},
	}
}

func (WorldModule) Name() string {
	return "World"
}

func (w WorldModule) SaveLoaders() []core.SaveLoader {
	return []core.SaveLoader{
		w.mapSys,
	}
}

func (w WorldModule) DataHandlers() []core.DataHandlers {
	return []core.DataHandlers{
		{GetElevation, w.mapSys.handleGetElevation},
		{GetBiome, w.mapSys.handleGetBiome},
		{GetRiverSize, w.mapSys.handleGetRiverSize},
		{GetWorldImage, w.mapSys.handleWorldImage},
		{GetDownslopeImage, w.mapSys.handleDownslopeImage},
		{GetCoastPointsImage, w.mapSys.handleCoastPointsImage},
	}
}

func (w WorldModule) Observers() []core.ObserveEvent {
	return []core.ObserveEvent{
		{w.mapSys, meta.CreateGameEventName},
	}
}

func (WorldModule) ManualEntries() []core.ManualEntry {
	return getManualEntries()
}
