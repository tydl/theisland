package world

import "gitlab.com/tydl/theisland/viking/core"

const (
	GeneratedWorldEventName = "GeneratedWorldEvent"
)

var _ core.Event = &GeneratedWorldEvent{}

type GeneratedWorldEvent struct {
	XMax int
	YMax int
}

func (GeneratedWorldEvent) Name() string {
	return GeneratedWorldEventName
}
