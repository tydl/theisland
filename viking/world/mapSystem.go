package world

import (
	"bytes"
	"image/png"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/mapgen"
	"gitlab.com/tydl/theisland/viking/meta"
)

const (
	MapSystemId     = "MapSystem"
	versionJustSeed = 0
)

var _ core.SaveLoader = &MapSystem{}
var _ core.Observer = &MapSystem{}

type MapSystem struct {
	world *mapgen.GeneratedMap
	seed  int64
}

func (m MapSystem) Id() string {
	return MapSystemId
}

func (m MapSystem) Save() (data interface{}, version int, err error) {
	return map[string]interface{}{
		"seed": m.seed,
	}, versionJustSeed, nil
}

func (m *MapSystem) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	if version == versionJustSeed {
		toLoad := struct {
			Seed int64 `json:"seed"`
		}{}
		if err := loadFunction(&toLoad); err != nil {
			return errors.StackWrap(err)
		}
		m.seed = toLoad.Seed
		pipeline := mapgen.DefaultGenerationPipeline
		pipeline.RiverRandSeed = m.seed
		pipeline.PointSelecter = mapgen.NewDefaultVoronoiPointSelecter(500, 500, 2000, m.seed)
		m.world = pipeline.Generate()
	} else {
		return errors.NewStackErrorStringf("unknown version %d", version)
	}
	return nil
}

func (m *MapSystem) Observe(e core.Event, s core.Systemer) error {
	if e.Name() == meta.CreateGameEventName {
		event, ok := e.(*meta.CreateGameEvent)
		if !ok {
			return errors.NewStackErrorString("event not *meta.CreateGameEvent")
		}
		pipeline := mapgen.DefaultGenerationPipeline
		pipeline.PointSelecter = mapgen.NewDefaultVoronoiPointSelecter(500, 500, 2000, int64(event.Seed))
		m.world = pipeline.Generate()
		m.seed = int64(event.Seed)
		eventer, err := core.GetAddEventer(s)
		if err != nil {
			return errors.StackWrap(err)
		}
		eventer.AddEvent(&GeneratedWorldEvent{500, 500})
	}
	return nil
}

func (m *MapSystem) GetElevation(x, y int) float64 {
	return m.world.ElevationAt(float64(x)+0.5, float64(y)+0.5)
}

func (m *MapSystem) GetBiome(x, y int) mapgen.Biome {
	return m.world.BiomeAt(float64(x)+0.5, float64(y)+0.5)
}

func (m *MapSystem) RiverSize(x, y int) float64 {
	return m.world.RiverSizeAt(float64(x)+0.5, float64(y)+0.5)
}

func (m *MapSystem) handleGetElevation(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	if m.world == nil {
		return nil, errors.NewStackErrorString("No world to query")
	}
	x, y := m.getXYQuery(vars)
	return map[string]interface{}{
		"elevation": m.GetElevation(x, y),
	}, nil
}

func (m *MapSystem) handleGetBiome(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	if m.world == nil {
		return nil, errors.NewStackErrorString("No world to query")
	}
	x, y := m.getXYQuery(vars)
	return map[string]interface{}{
		"biome": m.GetBiome(x, y).Name(),
	}, nil
}

func (m *MapSystem) handleGetRiverSize(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	if m.world == nil {
		return nil, errors.NewStackErrorString("No world to query")
	}
	x, y := m.getXYQuery(vars)
	return map[string]interface{}{
		"riverSize": m.RiverSize(x, y),
	}, nil
}

func (m *MapSystem) handleWorldImage(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	if m.world == nil {
		return nil, errors.NewStackErrorString("No world to display")
	}
	img := mapgen.ToImage(m.world, mapgen.DefaultGenerationPipeline.PointSelecter.BBox(), 1, true, false, false)
	buff := bytes.NewBuffer(make([]byte, 0))
	if err := png.Encode(buff, img); err != nil {
		return nil, errors.StackWrap(err)
	}
	return map[string]interface{}{
		"image": buff.Bytes(),
	}, nil
}

func (m *MapSystem) handleDownslopeImage(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	if m.world == nil {
		return nil, errors.NewStackErrorString("No world to display")
	}
	img := mapgen.ToImage(m.world, mapgen.DefaultGenerationPipeline.PointSelecter.BBox(), 1, false, true, false)
	buff := bytes.NewBuffer(make([]byte, 0))
	if err := png.Encode(buff, img); err != nil {
		return nil, errors.StackWrap(err)
	}
	return map[string]interface{}{
		"image": buff.Bytes(),
	}, nil
}

func (m *MapSystem) handleCoastPointsImage(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	if m.world == nil {
		return nil, errors.NewStackErrorString("No world to display")
	}
	img := mapgen.ToImage(m.world, mapgen.DefaultGenerationPipeline.PointSelecter.BBox(), 1, false, false, true)
	buff := bytes.NewBuffer(make([]byte, 0))
	if err := png.Encode(buff, img); err != nil {
		return nil, errors.StackWrap(err)
	}
	return map[string]interface{}{
		"image": buff.Bytes(),
	}, nil
}

func (m MapSystem) getXYQuery(vars core.RequestVariables) (x, y int) {
	ok := false
	x, ok = vars.Int("x")
	if !ok {
		x = 0
	}
	y, ok = vars.Int("y")
	if !ok {
		y = 0
	}
	return
}

func GetMapSystem(s core.Systemer) (*MapSystem, error) {
	system, err := s.System(MapSystemId)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	casted, ok := system.(*MapSystem)
	if !ok {
		return nil, errors.NewStackErrorString("could not cast to *MapSystem")
	}
	return casted, nil
}
