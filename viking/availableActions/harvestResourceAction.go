package availableActions

import (
	"fmt"
	"html/template"
	"math"
	"time"

	"gitlab.com/tydl/theisland/viking/anatomy"
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/inventory"
	"gitlab.com/tydl/theisland/viking/resources"
	"gitlab.com/tydl/theisland/viking/skills"
	"gitlab.com/tydl/theisland/viking/templates"
)

const (
	defaultBufferSize = 1024
)

var harvestResourceActionTemplate *template.Template = template.Must(template.New("").Parse("{{.text}}"))

type harvestResourceAction struct {
	descMap map[resources.ResourceNodeId]HarvestResourceNodeAction
}

type HarvestResourceNodeAction struct {
	NodeId        resources.ResourceNodeId
	DescriptionId string
	HasAnatomy    func(*anatomy.Head) bool
}

func newHarvestResourceAction(descs []HarvestResourceNodeAction) *harvestResourceAction {
	h := &harvestResourceAction{
		make(map[resources.ResourceNodeId]HarvestResourceNodeAction),
	}
	for _, desc := range descs {
		h.descMap[desc.NodeId] = desc
	}
	return h
}

func (h harvestResourceAction) Act(vars core.RequestVariables, s core.Systemer) (map[string]interface{}, time.Duration, error) {
	if creature.GetReaperSystemOrPanic(s).IsPcDead() {
		return nil, 0, errors.NewStackErrorString("PC is dead")
	}
	if err := cannotBeInEncounter(s); err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	instanceId := 0
	if reqInstanceId, present := vars.Int("node"); present {
		instanceId = reqInstanceId
	} else {
		return nil, 0, errors.NewStackErrorString("No node parameter present")
	}

	tempSys, err := templates.GetTemplateSystem(s)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	playerSys, err := creature.GetPlayerSystem(s)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	resSys, err := resources.GetResourceSystem(s)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	invSys, err := inventory.GetPlayerInventorySystem(s)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	anatSys, err := anatomy.GetPlayerAnatomySystem(s)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	nodeData, err := resSys.GetNodeInstance(instanceId)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}

	x, y := playerSys.GetPlayerLocation()
	if nodeData.X != x || nodeData.Y != y {
		return nil, 0, errors.NewStackErrorStringf("Cannot harvest: Resource node %d is at (%d, %d), not at (%d, %d)", instanceId, nodeData.X, nodeData.Y, x, y)
	}

	descData, ok := h.descMap[nodeData.Id]
	if !ok {
		return nil, 0, errors.NewStackErrorStringf("No description for resource node %s", nodeData.Id)
	}
	if !descData.HasAnatomy(anatSys.Anatomy()) {
		return nil, 0, errors.NewStackErrorStringf("Cannot harvest: Player does not meet anatomy requirements for resource node %d", instanceId)
	}

	harvested := make(map[resources.ResourceId]int)
	if nodeData.IsEmpty() {
		return nil, 0, errors.NewStackErrorString("Attempting to harvest empty node")
	}
	for kind, prototype := range nodeData.Harvest.ResourceQuantity {
		invItem := &inventory.InventoryItem{
			Kind:          string(kind),
			Id:            prototype.Id,
			Name:          prototype.Name,
			DescriptionId: prototype.DescriptionId,
			Metadata:      copyMetadata(prototype.Metadata),
		}
		quantity := prototype.Quantity
		level := skills.GetTotalLevelsLearnedForPayload(s, HarvestNodeExtraTwoPercentQuantityPayload{
			nodeData.Id,
			kind,
		})
		quantity = int(math.Floor(float64(quantity) * (1.0 + float64(level)/50.0)))
		if current, ok := nodeData.ResourcesInNode[kind]; ok {
			if prototype.Quantity > current.Current {
				quantity = current.Current
				nodeData.ResourcesInNode[kind].Current = 0
			} else {
				nodeData.ResourcesInNode[kind].Current -= quantity
			}
		} else {
			return nil, 0, errors.NewStackErrorStringf("Attempting to harvest unknown resource %s for node %s", kind, nodeData.Id)
		}
		harvested[kind] = quantity
		invSys.AddQuantity(invItem, quantity)
	}
	if err := resSys.Update(nodeData); err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	if err != resSys.OnHarvested(s, nodeData.Harvest.Id) {
		return nil, 0, errors.StackWrap(err)
	}

	toDisplay, err := tempSys.ExecuteWithData(descData.DescriptionId, struct {
		Harvested     map[resources.ResourceId]int
		PlayerAnatomy *anatomy.Head
	}{
		harvested,
		anatSys.Anatomy(),
	})
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}

	return map[string]interface{}{
		"text": toDisplay,
	}, nodeData.Harvest.Time, nil
}

func (h harvestResourceAction) getHarvestableResourceNodes(s core.Systemer) ([]*availableAction, error) {
	playerSys, err := creature.GetPlayerSystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	resSys, err := resources.GetResourceSystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	anatSys, err := anatomy.GetPlayerAnatomySystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	var available []*availableAction
	nodes := resSys.GetNodesAt(playerSys.GetPlayerLocation())
	for _, node := range nodes {
		if node.IsEmpty() {
			continue
		}
		descData, ok := h.descMap[node.Id]
		if !ok {
			return nil, errors.NewStackErrorStringf("No description for resource node %s", node.Id)
		}
		if descData.HasAnatomy(anatSys.Anatomy()) {
			available = append(available, resourceNodeDataToAvailableAction(node, node.Harvest.Time))
		}
	}
	return available, nil
}

func resourceNodeDataToAvailableAction(r resources.ResourceNodeData, t time.Duration) *availableAction {
	return &availableAction{
		Description: "Harvest a " + r.Text.Name,
		Duration:    t,
		Endpoint:    fmt.Sprintf("%s?node=%d", HarvestResource, r.Instance),
	}
}

func copyMetadata(toCopy map[string]interface{}) map[string]interface{} {
	copy := make(map[string]interface{})
	for k, v := range toCopy {
		copy[k] = v
	}
	return copy
}
