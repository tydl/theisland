package availableActions

import (
	"encoding/json"
	"math"
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/encounter"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/mapgen"
	"gitlab.com/tydl/theisland/viking/skills"
	"gitlab.com/tydl/theisland/viking/world"
)

const (
	baseMoveTime   = time.Minute
	baseThirstCost = 15
	baseHungerCost = 10
)

var _ core.Acter = &movePlayer{}

type movePlayer struct{}

func (m movePlayer) Act(vars core.RequestVariables, game core.Systemer) (map[string]interface{}, time.Duration, error) {
	if creature.GetReaperSystemOrPanic(game).IsPcDead() {
		return nil, 0, errors.NewStackErrorString("PC is dead")
	}
	if err := cannotBeInEncounter(game); err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	body, ok := vars.String("body")
	if !ok {
		return nil, 0, errors.NewStackErrorString("body required")
	}
	bodyStruct := struct {
		Dir string `json:"dir"`
	}{}
	err := json.Unmarshal([]byte(body), &bodyStruct)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	dir := bodyStruct.Dir
	ps, err := creature.GetPlayerSystem(game)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	mapSys, err := world.GetMapSystem(game)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	npcSys, err := creature.GetNpcSystem(game)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	encounterSys, err := encounter.GetEncounterSystem(game)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	pc := ps.GetPlayerCreature()
	x := pc.LocationX
	y := pc.LocationY
	direction := ""
	switch dir {
	case "n":
		y++
		direction = "north"
	case "s":
		y--
		direction = "south"
	case "e":
		x++
		direction = "east"
	case "w":
		x--
		direction = "west"
	default:
		return nil, 0, errors.NewStackErrorStringf("invalid dir '%s'", dir)
	}
	biome := mapSys.GetBiome(x, y)
	riverSize := mapSys.RiverSize(x, y)
	if !CanMove(riverSize, biome) {
		return nil, 0, errors.NewStackErrorString("cannot move into water")
	}
	pc.LocationX = x
	pc.LocationY = y
	eventer, err := core.GetAddEventer(game)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	eventer.AddEvent(&creature.PlayerMovedEvent{})
	npcs := npcSys.GetNpcsAt(x, y)
	if len(npcs) > 0 && !encounterSys.IsInEncounter() {
		err := encounterSys.BeginEncounter(npcs[0].Id)
		if err != nil {
			return nil, 0, errors.StackWrap(err)
		}
		eventer.AddEvent(&encounter.EncounterBeginEvent{})
	}
	ps.RemoveHunger(m.getHungerCost(game, biome), game)
	ps.RemoveThirst(m.getThirstCost(game, biome), game)
	return map[string]interface{}{
		"direction": direction,
	}, m.getMoveTime(game, biome), nil
}

func (m movePlayer) getHungerCost(s core.Systemer, b mapgen.Biome) int {
	level := skills.GetTotalLevelsLearnedForPayload(s, LessHungerToMovePayload{b})
	return baseHungerCost - int(math.Floor(float64(level)/3.0))
}

func (m movePlayer) getThirstCost(s core.Systemer, b mapgen.Biome) int {
	level := skills.GetTotalLevelsLearnedForPayload(s, LessThirstToMovePayload{b})
	return baseThirstCost - int(math.Floor(float64(level)/3.0))
}

func (m movePlayer) getMoveTime(s core.Systemer, b mapgen.Biome) time.Duration {
	level := skills.GetTotalLevelsLearnedForPayload(s, MoveOneSecondFasterPayload{b})
	return baseMoveTime - (time.Duration(level) * time.Second)
}

func CanMove(riverSize float64, biome mapgen.Biome) bool {
	return !(riverSize > 0 || biome.IsWater())
}
