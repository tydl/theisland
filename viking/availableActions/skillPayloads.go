package availableActions

import (
	"gitlab.com/tydl/theisland/viking/mapgen"
	"gitlab.com/tydl/theisland/viking/resources"
)

type LessHungerToMovePayload struct {
	Biome mapgen.Biome
}

type LessThirstToMovePayload struct {
	Biome mapgen.Biome
}

type MoveOneSecondFasterPayload struct {
	Biome mapgen.Biome
}

type HarvestNodeExtraTwoPercentQuantityPayload struct {
	ResourceNodeId resources.ResourceNodeId
	ResourceId     resources.ResourceId
}
