package availableActions

import (
	"fmt"
	"html/template"
	"sync"
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/crafting"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/empty"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/inventory"
	"gitlab.com/tydl/theisland/viking/world"
)

const (
	GetAvailableActions  = "/player/actions/available"
	GetAvailableMoves    = "/player/moves/available"
	GetAvailableCrafting = "/player/crafting/available"
	HarvestResource      = "/player/resource/harvest"
	MovePlayer           = "/player/move"
)

var playerMoveTemplate *template.Template = template.Must(template.New("").Parse("You go {{.direction}}."))

var _ core.Moduler = &Module{}

type Module struct {
	empty.EmptyModule
	harvestResourceAction *harvestResourceAction
	movePlayer            *movePlayer
	// mutable
	mutex                  sync.Mutex
	canGetAvailableActions bool
}

func NewModule(descs []HarvestResourceNodeAction) *Module {
	return &Module{
		empty.EmptyModule{},
		newHarvestResourceAction(descs),
		&movePlayer{},
		sync.Mutex{},
		false,
	}
}

func (*Module) Name() string {
	return "Available Actions Module"
}

func (*Module) Id() string {
	return "AvailableActionsModuleId"
}

func (m *Module) DataHandlers() []core.DataHandlers {
	return []core.DataHandlers{
		{GetAvailableActions, m.handleGetAvailableActions},
		{GetAvailableMoves, m.handleGetAvailableMoves},
		{GetAvailableCrafting, m.handleGetAvailableCrafting},
	}
}

func (m *Module) Actions() []core.Actions {
	return []core.Actions{
		{HarvestResource, m.harvestResourceAction, harvestResourceActionTemplate},
		{MovePlayer, m.movePlayer, playerMoveTemplate},
	}
}

func (m *Module) Observers() []core.ObserveEvent {
	return []core.ObserveEvent{
		{m, creature.PlayerSpawnedEventName},
		{m, core.LoadedGameEventName},
	}
}

func (m *Module) Observe(e core.Event, s core.Systemer) error {
	switch e.Name() {
	case creature.PlayerSpawnedEventName:
		fallthrough
	case core.LoadedGameEventName:
		m.mutex.Lock()
		m.canGetAvailableActions = true
		m.mutex.Unlock()
	}
	return nil
}

type availableAction struct {
	Description string        `json:"description"`
	Duration    time.Duration `json:"duration"`
	Endpoint    string        `json:"endpoint"`
}

func (m *Module) enforceSpawned() error {
	count := 0
	m.mutex.Lock()
	for count < 10 && !m.canGetAvailableActions {
		m.mutex.Unlock()
		count++
		time.Sleep(time.Millisecond * 100)
		m.mutex.Lock()
	}
	m.mutex.Unlock()
	if m.canGetAvailableActions {
		return nil
	}
	return errors.NewStackErrorString("Cannot get actions if no game in progress")
}

func (m *Module) handleGetAvailableActions(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	if creature.GetReaperSystemOrPanic(s).IsPcDead() {
		return nil, errors.NewStackErrorString("PC is dead")
	}
	if err := m.enforceSpawned(); err != nil {
		return nil, errors.StackWrap(err)
	}
	available, err := m.harvestResourceAction.getHarvestableResourceNodes(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	return available, nil
}

func (m *Module) handleGetAvailableCrafting(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	if creature.GetReaperSystemOrPanic(s).IsPcDead() {
		return nil, errors.NewStackErrorString("PC is dead")
	}
	recipeSys, err := crafting.GetRecipeSystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	availableRecipes, err := recipeSys.GetAvailableRecipes(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	invSys, err := inventory.GetPlayerInventorySystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	var available []*availableAction
	for _, recipe := range availableRecipes {
		available = append(available, &availableAction{
			Description: createCraftDescription(invSys, recipe),
			Duration:    recipe.CraftTime,
			Endpoint:    fmt.Sprintf("%s?recipe=%d", crafting.CraftItem, recipe.Id),
		})
	}
	return available, nil
}

func createCraftDescription(invSys *inventory.PlayerInventory, recipe crafting.CraftRecipe) string {
	description := "Craft " + createQuantityName(recipe.Produced[0].Quantity, recipe.Produced[0].Item)
	for i := 1; i < len(recipe.Produced); i++ {
		description = fmt.Sprintf("%s, %s", description, createQuantityName(recipe.Produced[i].Quantity, recipe.Produced[i].Item))
	}
	item := invSys.GetInventoryItemsForKindId(recipe.Consumed[0].Kind, recipe.Consumed[0].Id)
	description += " from " + createQuantityName(recipe.Consumed[0].Quantity, *item.Item)
	for i := 1; i < len(recipe.Consumed); i++ {
		item := invSys.GetInventoryItemsForKindId(recipe.Consumed[i].Kind, recipe.Consumed[i].Id)
		description = fmt.Sprintf("%s, %s", description, createQuantityName(recipe.Consumed[i].Quantity, *item.Item))
	}
	description += " (" + recipe.CraftTime.String() + ")"
	return description
}

func createQuantityName(qty int, i inventory.InventoryItem) string {
	if qty == 1 {
		return i.Name
	}
	return fmt.Sprintf("%dx %s", qty, i.Name)
}

func (m *Module) handleGetAvailableMoves(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	if creature.GetReaperSystemOrPanic(s).IsPcDead() {
		return nil, errors.NewStackErrorString("PC is dead")
	}
	if err := m.enforceSpawned(); err != nil {
		return nil, errors.StackWrap(err)
	}
	ps, err := creature.GetPlayerSystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	mapSys, err := world.GetMapSystem(s)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	if ps == nil {
		fmt.Println("PS IS NULL")
	}
	x, y := ps.GetPlayerLocation()
	biome := mapSys.GetBiome(x+1, y)
	riverSize := mapSys.RiverSize(x+1, y)
	canEast := CanMove(riverSize, biome)
	biome = mapSys.GetBiome(x, y+1)
	riverSize = mapSys.RiverSize(x, y+1)
	canNorth := CanMove(riverSize, biome)
	biome = mapSys.GetBiome(x-1, y)
	riverSize = mapSys.RiverSize(x-1, y)
	canWest := CanMove(riverSize, biome)
	biome = mapSys.GetBiome(x, y-1)
	riverSize = mapSys.RiverSize(x, y-1)
	canSouth := CanMove(riverSize, biome)
	return map[string]bool{
		"east":  canEast,
		"north": canNorth,
		"west":  canWest,
		"south": canSouth,
	}, nil
}
