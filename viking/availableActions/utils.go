package availableActions

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/encounter"
	"gitlab.com/tydl/theisland/viking/errors"
)

func cannotBeInEncounter(game core.Systemer) error {
	encounterSystem, err := encounter.GetEncounterSystem(game)
	if err != nil {
		return errors.StackWrap(err)
	}
	if encounterSystem.IsInEncounter() {
		return errors.NewStackErrorString("in an encounter")
	}
	return nil
}

func NpcCannotBeInEncounter(game core.Systemer, npcId int) error {
	encounterSystem, err := encounter.GetEncounterSystem(game)
	if err != nil {
		return errors.StackWrap(err)
	}
	if encounterSystem.IsInEncounter() && encounterSystem.NpcId == npcId {
		return errors.NewStackErrorString("in an encounter")
	}
	return nil
}
