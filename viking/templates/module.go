package templates

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/empty"
)

type Module struct {
	empty.EmptyModule
	templateSys *TemplateSystem
}

func NewModule(templateSys *TemplateSystem) *Module {
	return &Module{
		empty.EmptyModule{},
		templateSys,
	}
}

func (Module) Name() string {
	return "Templates"
}

func (m Module) SaveLoaders() []core.SaveLoader {
	return []core.SaveLoader{
		m.templateSys,
	}
}
