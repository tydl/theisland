package templates

import (
	"bytes"
	"html/template"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
)

const (
	noSaveVersion    = 0
	TemplateSystemId = "TemplateSystem"
	bufferSize       = 1024
)

type TemplateEntry struct {
	Name    string
	Content string
}

var _ core.SaveLoader = &TemplateSystem{}

type TemplateSystem struct {
	master *template.Template
}

func NewTemplateSystem(entries []TemplateEntry) *TemplateSystem {
	var err error
	topLevel := template.New("")
	for _, entry := range entries {
		topLevel, err = topLevel.New(entry.Name).Parse(entry.Content)
		if err != nil {
			// Very rare case where we want to panic outright.
			panic(errors.NewStackErrorStringf("error parsing template '%s': %s", entry.Name, err.Error()))
		}
	}
	return &TemplateSystem{
		topLevel,
	}
}

func (t *TemplateSystem) Id() string {
	return TemplateSystemId
}

func (t *TemplateSystem) Save() (data interface{}, version int, err error) {
	return nil, noSaveVersion, nil
}

func (t *TemplateSystem) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	return nil
}

func (t *TemplateSystem) Execute(name string) (string, error) {
	buffer := bytes.NewBuffer(make([]byte, 0, bufferSize))
	err := t.master.ExecuteTemplate(buffer, name, nil)
	return buffer.String(), err
}

func (t *TemplateSystem) ExecuteWithData(name string, data interface{}) (string, error) {
	buffer := bytes.NewBuffer(make([]byte, 0, bufferSize))
	err := t.master.ExecuteTemplate(buffer, name, data)
	return buffer.String(), err
}

func GetTemplateSystem(s core.Systemer) (*TemplateSystem, error) {
	system, err := s.System(TemplateSystemId)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	casted, ok := system.(*TemplateSystem)
	if !ok {
		return nil, errors.NewStackErrorString("could not cast to *TemplateSystem")
	}
	return casted, nil
}
