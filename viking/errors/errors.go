package errors

import (
	e0 "errors"
	"fmt"
	"runtime"
)

const (
	StackErrorBufferSize = 2048
)

type StackError struct {
	err      string
	stack    []byte
	nWritten int
}

func StackWrap(err error) error {
	if err == nil {
		return nil
	} else {
		if se, ok := err.(*StackError); ok {
			return se
		}
		s := &StackError{
			err:   err.Error(),
			stack: make([]byte, StackErrorBufferSize),
		}
		s.nWritten = runtime.Stack(s.stack, false)
		return s
	}
}

func NewStackError(err error) error {
	if se, ok := err.(*StackError); ok {
		return se
	}
	s := &StackError{
		err:   err.Error(),
		stack: make([]byte, StackErrorBufferSize),
	}
	s.nWritten = runtime.Stack(s.stack, false)
	return s
}

func NewStackErrorString(err string) *StackError {
	s := &StackError{
		err:   err,
		stack: make([]byte, StackErrorBufferSize),
	}
	s.nWritten = runtime.Stack(s.stack, false)
	return s
}

func NewStackErrorStringf(format string, a ...interface{}) *StackError {
	s := &StackError{
		err:   fmt.Sprintf(format, a),
		stack: make([]byte, StackErrorBufferSize),
	}
	s.nWritten = runtime.Stack(s.stack, false)
	return s
}

func (s StackError) Error() string {
	return s.err + "\n" + string(s.stack[:s.nWritten])
}

func New(msg string) error {
	return e0.New(msg)
}
