package gui

import (
	"html/template"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/empty"
)

const (
	guiModuleName = "GUI"
	gameFile      = "viking/gui/game.tmpl"
	gameFileName  = "game"
)

var _ core.Moduler = &GuiModule{}
var game = template.Must(template.New("game").ParseFiles(gameFile))

type GuiModule struct {
	empty.EmptyModule
}

func (GuiModule) Name() string {
	return guiModuleName
}

func (GuiModule) Templates() []*template.Template {
	return []*template.Template{
		game,
	}
}
