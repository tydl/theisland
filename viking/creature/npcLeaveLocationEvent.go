package creature

import "gitlab.com/tydl/theisland/viking/core"

const (
	NpcLeaveLocationEventName = "NpcLeaveLocationEvent"
)

var _ core.Event = &NpcLeaveLocationEvent{}

type NpcLeaveLocationEvent struct {
	X   int
	Y   int
	Npc *NpcCreature
}

func (NpcLeaveLocationEvent) Name() string {
	return NpcLeaveLocationEventName
}
