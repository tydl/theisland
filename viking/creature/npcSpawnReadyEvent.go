package creature

import "gitlab.com/tydl/theisland/viking/core"

const (
	NpcSpawnReadyName = "NpcSpawnReady"
)

var _ core.Event = &NpcSpawnReady{}

type NpcSpawnReady struct{}

func (NpcSpawnReady) Name() string {
	return NpcSpawnReadyName
}
