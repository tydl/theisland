package creature

import "gitlab.com/tydl/theisland/viking/core"

const (
	PlayerSpawnedEventName = "PlayerSpawnedEvent"
)

var _ core.Event = &PlayerSpawnedEvent{}

type PlayerSpawnedEvent struct {
}

func (PlayerSpawnedEvent) Name() string {
	return PlayerSpawnedEventName
}
