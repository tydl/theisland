package creature

import "gitlab.com/tydl/theisland/viking/core"

const (
	NpcEnterLocationEventName = "NpcEnterLocationEvent"
)

var _ core.Event = &NpcEnterLocationEvent{}

type NpcEnterLocationEvent struct {
	X   int
	Y   int
	Npc *NpcCreature
}

func (NpcEnterLocationEvent) Name() string {
	return NpcEnterLocationEventName
}
