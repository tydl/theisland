package creature

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/empty"
	"gitlab.com/tydl/theisland/viking/meta"
	"gitlab.com/tydl/theisland/viking/world"
)

const (
	GetPlayerData = "/creature/player"
	GetPlayerDead = "/creature/player/isDead"
)

var _ core.Moduler = &CreatureModule{}

type CreatureModule struct {
	empty.EmptyModule
	player     *PlayerSystem
	npcSystem  *NpcSystem
	npcFactory *AddNpcSystem
	reapSys    *ReaperSystem
}

func NewCreatureModule(factory *AddNpcSystem) *CreatureModule {
	return &CreatureModule{
		empty.EmptyModule{},
		&PlayerSystem{},
		newNpcSystem(),
		factory,
		&ReaperSystem{},
	}
}

func (CreatureModule) Name() string {
	return "Creature"
}

func (c CreatureModule) SaveLoaders() []core.SaveLoader {
	return []core.SaveLoader{
		c.player,
		c.npcSystem,
		c.reapSys,
	}
}

func (c CreatureModule) TimePassers() []core.TimePasser {
	return []core.TimePasser{c.npcSystem}
}

func (c CreatureModule) DataHandlers() []core.DataHandlers {
	return []core.DataHandlers{
		{GetPlayerData, c.player.handleGetPlayer},
		{GetPlayerDead, c.reapSys.handleGetDead},
	}
}

func (c CreatureModule) Observers() []core.ObserveEvent {
	return []core.ObserveEvent{
		{c.player, meta.CreateGameEventName},
		{c.player, world.GeneratedWorldEventName},
		{c.npcSystem, meta.CreateGameEventName},
		{c.npcSystem, world.GeneratedWorldEventName},
		{c.npcFactory, NpcSpawnReadyName},
		{c.npcFactory, core.InitializeGameEventName},
		{c.reapSys, meta.CreateGameEventName},
		{c.reapSys, core.LoadedGameEventName},
	}
}
