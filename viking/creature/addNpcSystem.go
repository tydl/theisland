package creature

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
)

const (
	AddNpcSystemId = "AddNpcSystem"
)

var _ core.Observer = &AddNpcSystem{}

type AddNpcSystem struct {
	Factories []NpcFactory
}

func NewAddNpcSystem(factories []NpcFactory) *AddNpcSystem {
	return &AddNpcSystem{
		factories,
	}
}

func (r *AddNpcSystem) Id() string {
	return AddNpcSystemId
}

func (r *AddNpcSystem) Observe(e core.Event, s core.Systemer) error {
	switch e.Name() {
	case core.InitializeGameEventName:
		return r.doRegisterAi(s)
	case NpcSpawnReadyName:
		return r.doSpawn(s)
	}
	return nil
}

func (r *AddNpcSystem) doRegisterAi(s core.Systemer) error {
	npcSys, err := GetNpcSystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	for _, factory := range r.Factories {
		npcSys.SetNpcAi(factory)
	}
	return nil
}

func (r *AddNpcSystem) doSpawn(s core.Systemer) error {
	npcSys, err := GetNpcSystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	if err := npcSys.SpawnNpcs(r.Factories, s); err != nil {
		return errors.StackWrap(err)
	}
	return nil
}
