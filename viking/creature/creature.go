package creature

import "time"

const (
	initialStat      = 750
	initialMaxStat   = 1000
	initialAttribute = 10
)

type Creature struct {
	LocationX         int
	LocationY         int
	Hunger            int
	MaxHunger         int
	Thirst            int
	MaxThirst         int
	Consciousness     int
	MaxConsciousness  int
	Submissiveness    int
	MaxSubmissiveness int
	Breath            int
	MaxBreath         int
	Will              int
	MaxWill           int
	// Attributes fall into two different dimensions which can affect how a
	// creature can impact the surrounding world around them. The first dimension
	// is the Source of a character's strengths and weaknesses. The second is the
	// Medium through which a character expresses their strengths and weaknesses.
	//
	// Dimension 1: Source
	Strength   int // Physical, which beats Mental
	Acumen     int // Mental, which beats Magical
	Attunement int // Magical, which beats Physical
	// Dimension 2: Medium
	Introspection int // Self, beats Terra
	Meditation    int // Terra (land and flora), beats Fauna
	Compassion    int // Fauna (other creatures), beats Self
}

func (c Creature) IsDead() bool {
	return c.Hunger <= 0 ||
		c.Thirst <= 0
}

func (c Creature) IsUnconscious() bool {
	return c.Consciousness <= 0
}

func (c Creature) IsSubmissive() bool {
	return c.Submissiveness <= 0
}

type NpcCreature struct {
	Creature
	Kind           string
	Id             int
	DescriptionIds []string
	Meta           Meta
}

type Meta struct {
	HomeBiome    string
	NextMove     time.Time
	MoveDistance int
	State        string
}

func NewPlayerCreature(x, y int) *Creature {
	return &Creature{
		LocationX:         x,
		LocationY:         y,
		Hunger:            initialStat,
		MaxHunger:         initialMaxStat,
		Thirst:            initialStat,
		MaxThirst:         initialMaxStat,
		Consciousness:     initialStat,
		MaxConsciousness:  initialMaxStat,
		Submissiveness:    initialStat,
		MaxSubmissiveness: initialMaxStat,
		Breath:            initialStat,
		MaxBreath:         initialMaxStat,
		Will:              initialStat,
		MaxWill:           initialMaxStat,
		Strength:          initialAttribute,
		Acumen:            initialAttribute,
		Attunement:        initialAttribute,
		Introspection:     initialAttribute,
		Meditation:        initialAttribute,
		Compassion:        initialAttribute,
	}
}
