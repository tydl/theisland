package creature

import (
	"math/rand"
	"sync"
	"time"

	"gitlab.com/tydl/theisland/viking/anatomy"
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/daynight"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/mapgen"
	"gitlab.com/tydl/theisland/viking/meta"
	"gitlab.com/tydl/theisland/viking/world"
)

const (
	NpcSystemId                = "NpcSystem"
	versionNpcSystemCollection = 0
	npcCreatureDataBuffer      = 1024
	consecutiveAttemptsMax     = 500
)

var _ core.SaveLoader = &NpcSystem{}
var _ core.TimePasser = &NpcSystem{}

type TimePassHandler interface {
	Handle(c *NpcCreature, rng *rand.Rand, t time.Duration, s core.Systemer) error
}

type NpcFactory interface {
	NumberSpawnsInBiome(mapgen.Biome) int
	Creature(mapgen.Biome, *rand.Rand) *Creature
	Kind() string
	DescriptionIds() []string
	TimePassHandler() TimePassHandler
	Anatomy(*rand.Rand) *anatomy.Head
}

type NpcSystem struct {
	instances   []*NpcCreature
	rng         *rand.Rand
	kindHandler map[string]TimePassHandler
	// Only needed for creating a game -- not loading one
	xMax int
	yMax int
	// Event syncing
	eventMutex    sync.Mutex
	genEvent      bool
	createEvent   bool
	instanceMutex sync.Mutex
	instanceIndex int
}

func newNpcSystem() *NpcSystem {
	return &NpcSystem{
		kindHandler: make(map[string]TimePassHandler, 0),
	}
}

func (n *NpcSystem) Id() string {
	return NpcSystemId
}

type saveV0 struct {
	Instances []*NpcCreature
	RngSeed   int64
}

func (n *NpcSystem) Save() (data interface{}, version int, err error) {
	return saveV0{n.instances, n.rng.Int63()}, versionNpcSystemCollection, nil
}

func (n *NpcSystem) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	if version == versionNpcSystemCollection {
		var saveData saveV0
		err := loadFunction(&saveData)
		if err != nil {
			return errors.StackWrap(err)
		}
		n.instances = saveData.Instances
		n.rng = rand.New(rand.NewSource(saveData.RngSeed))
	} else {
		return errors.NewStackErrorStringf("unknown version %d", version)
	}
	return nil
}

func (n *NpcSystem) TimePass(time time.Duration, s core.Systemer) error {
	for _, creature := range n.instances {
		handler, ok := n.kindHandler[creature.Kind]
		if !ok {
			return errors.NewStackErrorStringf("Unknown kind for npc time pass: %s", creature.Kind)
		}
		// Cannot be parallel and deterministic because of access to RNG
		if err := handler.Handle(creature, n.rng, time, s); err != nil {
			return errors.StackWrap(err)
		}
	}
	return nil
}

func (n *NpcSystem) Observe(e core.Event, s core.Systemer) error {
	switch e.Name() {
	case world.GeneratedWorldEventName:
		event, ok := e.(*world.GeneratedWorldEvent)
		if !ok {
			return errors.NewStackErrorString("event not *meta.CreateGameEvent")
		}
		n.xMax = event.XMax
		n.yMax = event.YMax
		n.setFlagSafely(&n.genEvent, true)
	case meta.CreateGameEventName:
		event, ok := e.(*meta.CreateGameEvent)
		if !ok {
			return errors.NewStackErrorString("event not *meta.CreateGameEvent")
		}
		n.rng = rand.New(rand.NewSource(event.Seed))
		n.setFlagSafely(&n.createEvent, true)
	}
	if n.allFlagsTrue() {
		eventer, err := core.GetAddEventer(s)
		if err != nil {
			return errors.StackWrap(err)
		}
		eventer.AddEvent(&NpcSpawnReady{})
	}
	return nil
}

func (n *NpcSystem) setFlagSafely(toSet *bool, b bool) {
	n.eventMutex.Lock()
	defer n.eventMutex.Unlock()
	*toSet = b
}

func (n *NpcSystem) allFlagsTrue() bool {
	n.eventMutex.Lock()
	defer n.eventMutex.Unlock()
	return n.genEvent && n.createEvent
}

func (n *NpcSystem) SetNpcAi(factory NpcFactory) {
	n.kindHandler[factory.Kind()] = factory.TimePassHandler()
}

func (n *NpcSystem) SpawnNpcs(factories []NpcFactory, systemer core.Systemer) error {
	if !n.allFlagsTrue() {
		return errors.NewStackErrorString("SpawnNpcs called when not all setup events done")
	}
	n.instanceMutex.Lock()
	defer n.instanceMutex.Unlock()
	n.createEvent = false
	n.genEvent = false

	mapSys, err := world.GetMapSystem(systemer)
	if err != nil {
		return errors.StackWrap(err)
	}
	daynightSys, err := daynight.GetDayNightSystem(systemer)
	if err != nil {
		return errors.StackWrap(err)
	}
	npcAnatomySys, err := anatomy.GetNpcAnatomySystem(systemer)
	if err != nil {
		return errors.StackWrap(err)
	}
	n.instances = make([]*NpcCreature, 0, npcCreatureDataBuffer)
	npcAnatomies := make(map[int]*anatomy.Head, 0)

	for _, factory := range factories {
		occurrences := make(map[string]struct {
			Count  int
			Target int
		})
		for _, biome := range mapgen.AllBiomes {
			occurrences[biome.Name()] = struct {
				Count  int
				Target int
			}{
				0,
				factory.NumberSpawnsInBiome(biome),
			}
		}

		currAttempts := 0
		for currAttempts < consecutiveAttemptsMax {
			x, y := n.rng.Intn(n.xMax), n.rng.Intn(n.yMax)
			biome := mapSys.GetBiome(x, y)
			targetData, ok := occurrences[biome.Name()]
			if !ok || targetData.Target == targetData.Count {
				currAttempts++
				continue
			}
			targetData.Count++
			occurrences[biome.Name()] = targetData

			instanceId := n.instanceIndex
			n.instanceIndex++
			npcAnatomies[instanceId] = factory.Anatomy(n.rng)
			creature := factory.Creature(biome, n.rng)
			creature.LocationX = x
			creature.LocationY = y
			instance := &NpcCreature{
				*creature,
				factory.Kind(),
				instanceId,
				factory.DescriptionIds(),
				Meta{
					biome.Name(),
					daynightSys.Now(),
					0,
					"",
				},
			}
			n.instances = append(n.instances, instance)
			currAttempts = 0
			bDone := true
			for _, val := range occurrences {
				if val.Count != val.Target {
					bDone = false
					break
				}
			}
			if bDone {
				break
			}
		}
	}
	npcAnatomySys.SetNpcAnatomies(npcAnatomies)
	return nil
}

func (n *NpcSystem) GetNpcsAt(x, y int) []NpcCreature {
	var toReturn []NpcCreature
	for _, data := range n.instances {
		if data.LocationX == x && data.LocationY == y {
			toReturn = append(toReturn, *data)
		}
	}
	return toReturn
}

func (n *NpcSystem) GetNpcForId(id int) *NpcCreature {
	for _, data := range n.instances {
		if data.Id == id {
			return data
		}
	}
	return nil
}

func GetNpcSystem(s core.Systemer) (*NpcSystem, error) {
	system, err := s.System(NpcSystemId)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	casted, ok := system.(*NpcSystem)
	if !ok {
		return nil, errors.NewStackErrorString("could not cast to *NpcSystem")
	}
	return casted, nil
}
