package creature

import "gitlab.com/tydl/theisland/viking/core"

const (
	PlayerMovedEventName = "PlayerMovedEvent"
)

var _ core.Event = &PlayerMovedEvent{}

type PlayerMovedEvent struct {
}

func (PlayerMovedEvent) Name() string {
	return PlayerMovedEventName
}
