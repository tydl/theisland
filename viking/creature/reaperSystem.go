package creature

import (
	"fmt"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/meta"
)

const (
	ReaperSystemId = "ReaperSystem"
)

var _ core.SaveLoader = &ReaperSystem{}

type ReaperSystem struct {
	isPcDead bool
}

func (ReaperSystem) Id() string {
	return ReaperSystemId
}

func (*ReaperSystem) Save() (data interface{}, version int, err error) {
	return nil, 0, nil
}

func (*ReaperSystem) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	return nil
}

func (r *ReaperSystem) Observe(e core.Event, s core.Systemer) error {
	switch e.Name() {
	case meta.CreateGameEventName:
		r.isPcDead = false
	case core.LoadedGameEventName:
		r.isPcDead = false
	}
	return nil
}

func (r *ReaperSystem) handleGetDead(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	return struct {
		IsDead bool `json:"isDead"`
	}{
		r.isPcDead,
	}, nil
}

func (r *ReaperSystem) IsPcDead() bool {
	return r.isPcDead
}

func (r *ReaperSystem) SetPcDead() {
	r.isPcDead = true
}

func (ReaperSystem) GetPcDeathDescription(c *Creature) string {
	if c.Hunger <= 0 {
		return fmt.Sprintf("You have died of hunger.")
	}
	if c.Thirst <= 0 {
		return fmt.Sprintf("You have died of thirst.")
	}
	return ""
}

func (ReaperSystem) GetNpcDeathDescription(c *Creature, kind string) string {
	if c.Hunger <= 0 {
		return fmt.Sprintf("The %s has died of hunger.", kind)
	}
	if c.Thirst <= 0 {
		return fmt.Sprintf("The %s has died of thirst.", kind)
	}
	return ""
}

func (ReaperSystem) GetNpcUnconsciousDescription(kind string) string {
	return fmt.Sprintf("The %s has lost consciousness.", kind)
}

func (ReaperSystem) GetPcUnconsciousDescription() string {
	return fmt.Sprintf("You have lost consciousness.")
}

func (ReaperSystem) GetNpcSubmissiveDescription(kind string) string {
	return fmt.Sprintf("The %s has lost the will to resist.", kind)
}

func (ReaperSystem) GetPcSubmissiveDescription() string {
	return fmt.Sprintf("You have lost the will to resist.")
}

func GetReaperSystemOrPanic(s core.Systemer) *ReaperSystem {
	system, err := s.System(ReaperSystemId)
	if err != nil {
		panic(errors.StackWrap(err))
	}
	casted, ok := system.(*ReaperSystem)
	if !ok {
		panic(errors.NewStackErrorString("could not cast to *ReaperSystem"))
	}
	return casted
}
