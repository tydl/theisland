package creature

import (
	"math/rand"
	"sync"
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/mapgen"
	"gitlab.com/tydl/theisland/viking/meta"
	"gitlab.com/tydl/theisland/viking/world"
)

const (
	PlayerSystemId  = "PlayerSystem"
	versionCreature = 0
)

var _ core.SaveLoader = &PlayerSystem{}

type PlayerSystem struct {
	rand                *rand.Rand
	xMax                int
	yMax                int
	playerCreature      *Creature
	flagMutex           sync.Mutex
	createGameEvent     bool
	generatedWorldEvent bool
}

func newPlayerSystem() *PlayerSystem {
	return &PlayerSystem{
		rand:                nil,
		playerCreature:      nil,
		flagMutex:           sync.Mutex{},
		createGameEvent:     false,
		generatedWorldEvent: false,
	}
}

func (p *PlayerSystem) Id() string {
	return PlayerSystemId
}

func (p *PlayerSystem) Save() (data interface{}, version int, err error) {
	return p.playerCreature, versionCreature, nil
}

func (p *PlayerSystem) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	switch version {
	case versionCreature:
		p.playerCreature = &Creature{}
		if err := loadFunction(p.playerCreature); err != nil {
			return errors.StackWrap(err)
		}
	default:
		return errors.NewStackErrorStringf("unknown version %d", version)
	}
	return nil
}

func (p *PlayerSystem) Observe(e core.Event, s core.Systemer) error {
	switch e.Name() {
	case meta.CreateGameEventName:
		p.flagMutex.Lock()
		p.createGameEvent = true
		p.flagMutex.Unlock()
		createGame, ok := e.(*meta.CreateGameEvent)
		if !ok {
			return errors.NewStackErrorString("Not a *meta.CreateGameEvent")
		}
		p.rand = rand.New(rand.NewSource(createGame.Seed))
	case world.GeneratedWorldEventName:
		p.flagMutex.Lock()
		p.generatedWorldEvent = true
		p.flagMutex.Unlock()
		genWorld, ok := e.(*world.GeneratedWorldEvent)
		if !ok {
			return errors.NewStackErrorString("Not a *world.GeneratedWorldEvent")
		}
		p.xMax = genWorld.XMax
		p.yMax = genWorld.YMax
	default:
	}
	if !p.allFlagsValid() {
		return nil
	}
	mapSys, err := world.GetMapSystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	// Put player adjacent to ocean.
	x := 0
	y := 0
	adjacentToOcean := false
	for !adjacentToOcean {
		x = p.rand.Intn(p.xMax)
		y = p.rand.Intn(p.yMax)
		if b := mapSys.GetBiome(x, y); b.Name() == mapgen.BeachName {
			adjacentToOcean =
				mapSys.GetBiome(x+1, y).Name() == mapgen.OceanName ||
					mapSys.GetBiome(x, y+1).Name() == mapgen.OceanName ||
					mapSys.GetBiome(x-1, y).Name() == mapgen.OceanName ||
					mapSys.GetBiome(x, y-1).Name() == mapgen.OceanName
		}
	}
	p.playerCreature = NewPlayerCreature(x, y)
	eventer, err := core.GetAddEventer(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	eventer.AddEvent(&PlayerSpawnedEvent{})
	return nil
}

func (p *PlayerSystem) GetPlayerLocation() (x int, y int) {
	x = p.playerCreature.LocationX
	y = p.playerCreature.LocationY
	return
}

func (p *PlayerSystem) GetPlayerCreature() *Creature {
	return p.playerCreature
}

func (p *PlayerSystem) AddHunger(h int) *PlayerSystem {
	p.playerCreature.Hunger += h
	if p.playerCreature.MaxHunger < p.playerCreature.Hunger {
		p.playerCreature.Hunger = p.playerCreature.MaxHunger
	}
	return p
}

func (p *PlayerSystem) AddThirst(t int) *PlayerSystem {
	p.playerCreature.Thirst += t
	if p.playerCreature.MaxThirst < p.playerCreature.Thirst {
		p.playerCreature.Thirst = p.playerCreature.MaxThirst
	}
	return p
}

func (p *PlayerSystem) RemoveHunger(h int, s core.Systemer) *PlayerSystem {
	p.playerCreature.Hunger -= h
	if 0 > p.playerCreature.Hunger {
		p.playerCreature.Hunger = 0
	}
	if p.playerCreature.IsDead() {
		GetReaperSystemOrPanic(s).SetPcDead()
	}
	return p
}

func (p *PlayerSystem) RemoveThirst(t int, s core.Systemer) *PlayerSystem {
	p.playerCreature.Thirst -= t
	if 0 > p.playerCreature.Thirst {
		p.playerCreature.Thirst = 0
	}
	if p.playerCreature.IsDead() {
		GetReaperSystemOrPanic(s).SetPcDead()
	}
	return p
}

func (p *PlayerSystem) allFlagsValid() bool {
	p.flagMutex.Lock()
	defer p.flagMutex.Unlock()
	toReturn := p.createGameEvent && p.generatedWorldEvent
	if toReturn {
		p.createGameEvent = false
		p.generatedWorldEvent = false
	}
	return toReturn
}

func (p *PlayerSystem) handleGetPlayer(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	p.flagMutex.Lock()
	for p.createGameEvent != p.generatedWorldEvent {
		p.flagMutex.Unlock()
		time.Sleep(time.Millisecond * 100)
		p.flagMutex.Lock()
	}
	p.flagMutex.Unlock()
	return p.playerCreature, nil
}

func GetPlayerSystem(s core.Systemer) (*PlayerSystem, error) {
	system, err := s.System(PlayerSystemId)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	casted, ok := system.(*PlayerSystem)
	if !ok {
		return nil, errors.NewStackErrorString("could not cast to *PlayerSystem")
	}
	return casted, nil
}
