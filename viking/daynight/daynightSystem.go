package daynight

import (
	"fmt"
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/meta"
)

const (
	daynightSystem = "daynightSystem"
	versionZero    = 0
	timeLayout     = "15:04:05"
)

var _ core.SaveLoader = &DayNightSystem{}
var _ core.TimePasser = &DayNightSystem{}

type vDayNightZero struct {
	StartDate   time.Time
	CurrentTime time.Time
}

type DayNightSystem struct {
	startDate   time.Time
	currentTime time.Time
}

func (s *DayNightSystem) Id() string {
	return daynightSystem
}

func (s *DayNightSystem) Save() (data interface{}, version int, err error) {
	return vDayNightZero{s.startDate, s.currentTime}, versionZero, nil
}

func (s *DayNightSystem) Load(loadFunction func(data interface{}) error, version int, observerManager core.AddRemoveObserver) error {
	switch version {
	case versionZero:
		var vDayNightZero vDayNightZero
		if err := loadFunction(&vDayNightZero); err != nil {
			return errors.StackWrap(err)
		}
		s.startDate = vDayNightZero.StartDate
		s.currentTime = vDayNightZero.CurrentTime
	default:
		return errors.NewStackErrorStringf("unsupported version %d", version)
	}
	return nil
}

func (s *DayNightSystem) Observe(event core.Event, systemer core.Systemer) error {
	switch event.Name() {
	case meta.CreateGameEventName:
		s.currentTime = time.Now()
		s.startDate = time.Now()
	}
	return nil
}

func (s *DayNightSystem) TimePass(duration time.Duration, systemer core.Systemer) error {
	s.currentTime = s.currentTime.Add(duration)
	return nil
}

// IsNight returns true if the time is within:
//         20:00 <= time or time < 6 (10 hours)
func (s *DayNightSystem) IsNight() bool {
	return s.currentTime.Hour() < 6 || s.currentTime.Hour() >= 20
}

// IsSunrise returns true if the time is within:
//         06:00 <= time < 08:00 (2 hours)
func (s *DayNightSystem) IsSunrise() bool {
	return s.currentTime.Hour() >= 6 && s.currentTime.Hour() < 8
}

// IsDay returns true if the time is within:
//         08:00 <= time < 18:00 (10 hours)
func (s *DayNightSystem) IsDay() bool {
	return s.currentTime.Hour() >= 8 && s.currentTime.Hour() < 18
}

// IsDay returns true if the time is within:
//         18:00 <= time < 20:00 (2 hours)
func (s *DayNightSystem) IsSunset() bool {
	return s.currentTime.Hour() >= 18 && s.currentTime.Hour() < 20
}

// Now gets the current game time.
func (s *DayNightSystem) Now() time.Time {
	return s.currentTime
}

func (s *DayNightSystem) withinString() string {
	if s.IsNight() {
		return "Night"
	} else if s.IsSunrise() {
		return "Sunrise"
	} else if s.IsDay() {
		return "Day"
	} else if s.IsSunset() {
		return "Sunset"
	} else {
		return "Unknown"
	}
}

func (s *DayNightSystem) formatCurrentTime() string {
	d1 := time.Date(s.startDate.Year(), s.startDate.Month(), s.startDate.Day(), 0, 0, 0, 0, time.UTC)
	d2 := time.Date(s.currentTime.Year(), s.currentTime.Month(), s.currentTime.Day(), 0, 0, 0, 0, time.UTC)
	dayNum := int(d2.Sub(d1) / (24 * time.Hour))
	return fmt.Sprintf("Day %d, %s", dayNum, s.currentTime.Format(timeLayout))
}

func GetDayNightSystem(s core.Systemer) (*DayNightSystem, error) {
	system, err := s.System(daynightSystem)
	if err != nil {
		return nil, errors.StackWrap(err)
	}
	casted, ok := system.(*DayNightSystem)
	if !ok {
		return nil, errors.NewStackErrorString("could not cast to *DayNightSystem")
	}
	return casted, nil
}
