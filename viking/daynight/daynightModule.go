package daynight

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/empty"
	"gitlab.com/tydl/theisland/viking/meta"
)

const (
	dayNightModuleName = "Day Night Module"
	GetCurrentTime     = "/time"
)

var _ core.Moduler = &DayNightModule{}

type DayNightModule struct {
	empty.EmptyModule
	system *DayNightSystem
}

func NewDayNightModule() *DayNightModule {
	return &DayNightModule{
		empty.EmptyModule{},
		&DayNightSystem{},
	}
}

func (m *DayNightModule) Name() string {
	return dayNightModuleName
}

func (m *DayNightModule) SaveLoaders() []core.SaveLoader {
	return []core.SaveLoader{m.system}
}

func (m *DayNightModule) TimePassers() []core.TimePasser {
	return []core.TimePasser{m.system}
}

func (m *DayNightModule) DataHandlers() []core.DataHandlers {
	return []core.DataHandlers{
		{GetCurrentTime, m.handleTimeRequest},
	}
}

func (m *DayNightModule) Observers() []core.ObserveEvent {
	return []core.ObserveEvent{
		{m.system, meta.CreateGameEventName},
	}
}

func (m *DayNightModule) ManualEntries() []core.ManualEntry {
	return getManualEntries()
}

func (m *DayNightModule) handleTimeRequest(vars core.RequestVariables, s core.Systemer) (interface{}, error) {
	return struct {
		Time      string `json:"time"`
		TimeOfDay string `json:"timeOfDay"`
	}{
		m.system.formatCurrentTime(),
		m.system.withinString(),
	}, nil
}
