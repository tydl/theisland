package daynight

import (
	"gitlab.com/tydl/theisland/viking/core"
)

func getManualEntries() []core.ManualEntry {
	return []core.ManualEntry{
		{Title: "Time", Text: "Time must flow in order for a player to carry out his or her will. A specific date and time allow a player to plan out activities and better manipulate the surounding environment. Specific hours of the day determine what time of day it is."},
		{Title: "Time Of Day", Text: "There are four periods of time throughout the day: night, sunrise, day, and sunset. Many activities and interactions in the world are governed by the time of day instead of specific times."},
		{Title: "Night", Text: "Night is the period of time marked by the absence of the sun for ten hours from 20:00 and 06:00."},
		{Title: "Sunrise", Text: "Sunrise is the period of time the sun rises from the eastern horizon for two hours from 06:00 to 08:00."},
		{Title: "Day", Text: "Day is the period of time the sun is present overhead for ten hours from 08:00 to 18:00."},
		{Title: "Sunset", Text: "Sunset is the period of time the sun sets on the western horizon for two hours from 18:00 to 20:00."},
	}
}
