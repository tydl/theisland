package meta

import (
	"encoding/json"
	"time"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/errors"
)

var _ core.Acter = &StartNewGameAction{}

type StartNewGameAction struct{}

func (StartNewGameAction) Act(vars core.RequestVariables, game core.Systemer) (map[string]interface{}, time.Duration, error) {
	eventSystem, err := game.System(core.AddEventerId)
	if err != nil {
		return nil, 0, errors.StackWrap(err)
	}
	eventer, ok := eventSystem.(core.AddEventer)
	if !ok {
		return nil, 0, errors.NewStackErrorStringf("not core.AddEventer: %T", eventSystem)
	}
	body, ok := vars.String(core.RequestVariablePostBody)
	createGameEvent := &CreateGameEvent{}
	if ok {
		if err := json.Unmarshal([]byte(body), &createGameEvent); err != nil {
			return nil, 0, errors.StackWrap(err)
		}
	} else {
		return nil, 0, errors.NewStackErrorString("no post body")
	}
	eventer.AddEvent(createGameEvent)
	return nil, 0, nil
}
