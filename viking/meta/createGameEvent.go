package meta

import (
	"gitlab.com/tydl/theisland/viking/core"
)

const (
	CreateGameEventName = "CreateGameEvent"
)

var _ core.Event = &CreateGameEvent{}

type CreateGameEvent struct {
	Seed int64
	Sex  string
}

func (CreateGameEvent) Name() string {
	return CreateGameEventName
}
