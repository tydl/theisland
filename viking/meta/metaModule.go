package meta

import (
	"html/template"

	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/empty"
)

const (
	metaModuleName        = "Meta"
	StartNewGameActionURI = "/StartNewGame"
)

var _ core.Moduler = &MetaModule{}
var startNewGameTemplate *template.Template = template.Must(template.New("").Parse("Starting a new game."))

type MetaModule struct {
	empty.EmptyModule
}

func (MetaModule) Name() string {
	return metaModuleName
}

func (MetaModule) Actions() []core.Actions {
	return []core.Actions{
		{StartNewGameActionURI, &StartNewGameAction{}, startNewGameTemplate},
	}
}
