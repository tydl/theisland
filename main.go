package main

import (
	"bufio"
	"fmt"
	"log"
	"os"

	"gitlab.com/tydl/theisland/phalanx"
	"gitlab.com/tydl/theisland/viking/anatomy"
	"gitlab.com/tydl/theisland/viking/availableActions"
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/crafting"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/daynight"
	"gitlab.com/tydl/theisland/viking/encounter"
	"gitlab.com/tydl/theisland/viking/gui"
	"gitlab.com/tydl/theisland/viking/inventory"
	"gitlab.com/tydl/theisland/viking/meta"
	"gitlab.com/tydl/theisland/viking/procedural"
	"gitlab.com/tydl/theisland/viking/resources"
	"gitlab.com/tydl/theisland/viking/skills"
	"gitlab.com/tydl/theisland/viking/storyteller"
	"gitlab.com/tydl/theisland/viking/templates"
	"gitlab.com/tydl/theisland/viking/world"
)

// Injector injects configuration data for the game. The game server provides
// mechanics governing gameplay. Conversely, the injector provides the game
// content. More content can be added by modifying the injector and entire new
// games can be created by using an entirely different injector.
type Injector interface {
	GetTemplateEntries() []templates.TemplateEntry
	GetResourceNodeFactories() []resources.ResourceNodeFactory
	GetHarvestResourceNodeActions() []availableActions.HarvestResourceNodeAction
	GetCraftingRecipes() []crafting.CraftRecipe
	GetNpcFactories() []creature.NpcFactory
	GetCombatHandlers() map[string]encounter.CombatHandler
	GetEncounterActions() []encounter.Action
	GetGameSkills() []skills.Skill
	GetItemMetadata() []inventory.ItemMetadata
}

func main() {
	startWithInjector(phalanx.NewInjector())
}

func startWithInjector(injector Injector) {
	file, err := os.OpenFile("viking_error.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	errorLogger := log.New(file, "", log.LstdFlags)
	server := core.NewServer(errorLogger)

	server.RegisterModule(&gui.GuiModule{})
	server.RegisterModule(templates.NewModule(templates.NewTemplateSystem(injector.GetTemplateEntries())))
	server.RegisterModule(daynight.NewDayNightModule())
	server.RegisterModule(procedural.NewProceduralModule())
	server.RegisterModule(&meta.MetaModule{})
	server.RegisterModule(world.NewWorldModule())
	server.RegisterModule(creature.NewCreatureModule(creature.NewAddNpcSystem(injector.GetNpcFactories())))
	server.RegisterModule(storyteller.NewModule())
	server.RegisterModule(resources.NewModule(resources.NewAddResourceNodeSystem(injector.GetResourceNodeFactories())))
	server.RegisterModule(inventory.NewModule(injector.GetItemMetadata()))
	server.RegisterModule(anatomy.NewModule())
	server.RegisterModule(availableActions.NewModule(injector.GetHarvestResourceNodeActions()))
	server.RegisterModule(crafting.NewModule(injector.GetCraftingRecipes()))
	server.RegisterModule(encounter.NewModule(injector.GetCombatHandlers(), injector.GetEncounterActions()))
	server.RegisterModule(skills.NewModule(injector.GetGameSkills()))

	err = server.Start()
	if err != nil {
		panic(err)
	}
	cmdLine := bufio.NewReader(os.Stdin)
	fmt.Print("Press enter to stop the server. Be sure to save your game!")
	_, _ = cmdLine.ReadString('\n')
	server.Stop()
}
