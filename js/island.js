var Island = function () {
  var Button = ReactBootstrap.Button;
  var ButtonGroup = ReactBootstrap.ButtonGroup;
  var Grid = ReactBootstrap.Grid;
  var Row = ReactBootstrap.Row;
  var Col = ReactBootstrap.Col;
  var Nav = ReactBootstrap.Nav;
  var NavItem = ReactBootstrap.NavItem;
  var Panel = ReactBootstrap.Panel;
  var Table = ReactBootstrap.Table;
  var Modal = ReactBootstrap.Modal;
  var ButtonInput = ReactBootstrap.ButtonInput;
  var Input = ReactBootstrap.Input;
  var Radio = ReactBootstrap.Radio;
  var Alert = ReactBootstrap.Alert;

  var postJSONCallback = function (endpoint, body, callback) {
    var startGame = {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify(body)
    };
    fetch(endpoint, startGame).then(function (response) {
      if (!response.ok) {
        console.log("postJSON request not ok");
      } else {
        response.json().then(function (responseBlob) {
          return callback(responseBlob);
        });
      }
    }).catch(function (error) {
      console.log("postJSON error: " + error.message);
    });
  };

  var postJSON = function (endpoint, body) {
    postJSONCallback(endpoint, body, function (dummy) {});
  };

  var getJSON = function (endpoint, callback) {
    var startGame = {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "GET"
    };
    fetch(endpoint, startGame).then(function (response) {
      if (!response.ok) {
        console.log("getJSON request not ok");
      } else {
        response.json().then(function (responseBlob) {
          return callback(responseBlob);
        });
      }
    }).catch(function (error) {
      console.log("getJSON error: " + error.message);
    });
  };

  var getFile = function (endpoint, callback) {
    var payload = {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "GET"
    };
    fetch(endpoint, payload).then(function (response) {
      if (!response.ok) {
        console.log("getFile request not ok: " + response.statusText);
      } else if (response.status != 200 && response.status != 0) {
        console.log("getFile response not 200 nor 0: " + response.statusText);
      } else {
        return response.blob().then(function (responseBlob) {
          return callback(responseBlob);
        });
      }
    }).catch(function (error) {
      console.log("getFile error: " + error.message);
    });
  };

  var postFile = function (endpoint, file) {
    var data = new FormData();
    data.append("file", file);
    var payload = {
      method: "POST",
      body: data
    };
    fetch(endpoint, payload).then(function (response) {
      if (!response.ok) {
        console.log("postFile request not ok: " + response.statusText);
      }
    }).catch(function (error) {
      console.log("postFile error: " + error.message);
    });
  };

  ////////////////////////////////////////////////////////////////////////////////
  // Game Tab
  ////////////////////////////////////////////////////////////////////////////////
  var MovePanel = React.createClass({
    displayName: 'MovePanel',

    getInitialState: function () {
      return { availableMoves: { north: false, south: false, east: false, west: false } };
    },
    componentWillMount: function () {
      var me = this;
      cheet('w', function () {
        if (me.state.availableMoves.north) {
          me.props.goNorth();
        }
      });
      cheet('a', function () {
        if (me.state.availableMoves.west) {
          me.props.goWest();
        }
      });
      cheet('s', function () {
        if (me.state.availableMoves.south) {
          me.props.goSouth();
        }
      });
      cheet('d', function () {
        if (me.state.availableMoves.east) {
          me.props.goEast();
        }
      });
      this.updateAvailableMoves();
    },
    componentWillReceiveProps: function () {
      this.updateAvailableMoves();
    },
    updateAvailableMoves: function () {
      var me = this;
      getJSON("/encounter", function (resultBlob) {
        if (!resultBlob.inEncounter) {
          getJSON("/player/moves/available", function (resultBlob) {
            me.setState({ availableMoves: resultBlob });
          });
        } else {
          me.setState({ availableMoves: { north: false, south: false, east: false, west: false } });
        }
      });
    },
    render: function () {
      return React.createElement(
        Grid,
        null,
        React.createElement(
          Row,
          null,
          React.createElement(
            Col,
            { sm: 4 },
            React.createElement(
              Button,
              { block: true, disabled: this.props.isDead || !this.state.availableMoves.north, onClick: this.props.goNorth },
              'North'
            )
          )
        ),
        React.createElement(
          Row,
          null,
          React.createElement(
            Col,
            { sm: 4 },
            React.createElement(
              ButtonGroup,
              { justified: true },
              React.createElement(
                ButtonGroup,
                null,
                React.createElement(
                  Button,
                  { disabled: this.props.isDead || !this.state.availableMoves.west, onClick: this.props.goWest },
                  'West'
                )
              ),
              React.createElement(
                ButtonGroup,
                null,
                React.createElement(
                  Button,
                  { disabled: this.props.isDead || !this.state.availableMoves.south, onClick: this.props.goSouth },
                  'South'
                )
              ),
              React.createElement(
                ButtonGroup,
                null,
                React.createElement(
                  Button,
                  { disabled: this.props.isDead || !this.state.availableMoves.east, onClick: this.props.goEast },
                  'East'
                )
              )
            )
          )
        )
      );
    }
  });

  var Options = React.createClass({
    displayName: 'Options',

    getInitialState: function () {
      return { actions: new Array() };
    },
    componentWillMount: function () {
      this.updateAvailableActions();
    },
    componentWillReceiveProps: function () {
      this.updateAvailableActions();
    },
    updateAvailableActions: function () {
      var me = this;
      getJSON("/encounter", function (resultBlob) {
        if (!resultBlob.inEncounter) {
          getJSON("/player/actions/available", function (resultBlob) {
            if (resultBlob == null) {
              me.setState({ actions: new Array() });
            } else {
              me.setState({ actions: resultBlob });
            }
          });
        } else {
          var encounterActions = new Array();
          encounterActions.push({ name: "Run", description: "Run Away (" + resultBlob.runChance + "% chance)", endpoint: "/encounter/player/run" });
          for (var i = 0; resultBlob.playerActions != null && i < resultBlob.playerActions.length; i++) {
            encounterActions.push({ name: resultBlob.playerActions[i].name, description: resultBlob.playerActions[i].description, endpoint: "/encounter/player/do" });
          }
          me.setState({ actions: encounterActions });
        }
      });
    },
    getOnClick: function (i, endpoint) {
      var me = this;
      if (endpoint == "/encounter/player/do") {
        return function () {
          postJSONCallback(endpoint, { actionName: me.state.actions[i].name }, function (callback) {
            me.props.onActionText(callback.text);
          });
        };
      }
      return function () {
        postJSONCallback(endpoint, {}, function (callback) {
          me.props.onActionText(callback.text);
        });
      };
    },
    render: function () {
      var buttonData = new Array();
      for (var i = 0; i < this.state.actions.length; i++) {
        buttonData.push({
          key: i,
          text: this.state.actions[i].description,
          onClickFunc: this.getOnClick(i, this.state.actions[i].endpoint),
          disabled: this.props.isDead
        });
      }
      for (var i = 0; i < 12 - this.state.actions.length; i++) {
        buttonData.push({ key: 12 - i, text: "N/A", disabled: true });
      }
      var buttons = buttonData.map(function (button) {
        return React.createElement(
          Button,
          { key: button.key, disabled: button.disabled, onClick: button.onClickFunc },
          button.text
        );
      });
      return React.createElement(
        ButtonGroup,
        { vertical: true, block: true },
        buttons
      );
    }
  });

  var CharacterPanel = React.createClass({
    displayName: 'CharacterPanel',

    getInitialState: function () {
      return {
        time: "",
        playerData: {},
        enemyData: null,
        biomeData: {
          biome: "N/A"
        }
      };
    },
    componentWillMount: function () {
      this.update();
    },
    componentWillReceiveProps: function () {
      this.update();
    },
    render: function () {
      if (this.state.playerData == null || this.state.biomeData == null) {
        return React.createElement('div', null);
      }
      var opponentTable = null;
      if (this.state.enemyData != null) {
        opponentTable = React.createElement(
          'div',
          null,
          React.createElement(
            'h2',
            { className: 'text-center' },
            this.state.enemyData.Kind
          ),
          React.createElement(
            Table,
            { condensed: true },
            React.createElement(
              'tbody',
              null,
              React.createElement(
                'tr',
                null,
                React.createElement(
                  'td',
                  null,
                  React.createElement(
                    'b',
                    null,
                    'Consciousness'
                  )
                ),
                React.createElement(
                  'td',
                  null,
                  this.state.enemyData.Consciousness,
                  '/',
                  this.state.enemyData.MaxConsciousness
                )
              ),
              React.createElement(
                'tr',
                null,
                React.createElement(
                  'td',
                  null,
                  React.createElement(
                    'b',
                    null,
                    'Submissiveness'
                  )
                ),
                React.createElement(
                  'td',
                  null,
                  this.state.enemyData.Submissiveness,
                  '/',
                  this.state.enemyData.MaxSubmissiveness
                )
              ),
              React.createElement(
                'tr',
                null,
                React.createElement(
                  'td',
                  null,
                  React.createElement(
                    'b',
                    null,
                    'Breath'
                  )
                ),
                React.createElement(
                  'td',
                  null,
                  this.state.enemyData.Breath,
                  '/',
                  this.state.enemyData.MaxBreath
                )
              ),
              React.createElement(
                'tr',
                null,
                React.createElement(
                  'td',
                  null,
                  React.createElement(
                    'b',
                    null,
                    'Will'
                  )
                ),
                React.createElement(
                  'td',
                  null,
                  this.state.enemyData.Will,
                  '/',
                  this.state.enemyData.MaxWill
                )
              )
            )
          )
        );
      }
      return React.createElement(
        'div',
        null,
        React.createElement(
          'h2',
          { className: 'text-center' },
          'Player'
        ),
        React.createElement(
          Table,
          { condensed: true },
          React.createElement(
            'tbody',
            null,
            React.createElement(
              'tr',
              null,
              React.createElement(
                'td',
                null,
                React.createElement(
                  'b',
                  null,
                  'Time'
                )
              ),
              React.createElement(
                'td',
                null,
                this.state.time
              )
            ),
            React.createElement(
              'tr',
              null,
              React.createElement(
                'td',
                null,
                React.createElement(
                  'b',
                  null,
                  'Biome'
                )
              ),
              React.createElement(
                'td',
                null,
                this.state.biomeData.biome
              )
            ),
            React.createElement(
              'tr',
              null,
              React.createElement(
                'td',
                null,
                React.createElement(
                  'b',
                  null,
                  'Location'
                )
              ),
              React.createElement(
                'td',
                null,
                '(',
                this.state.playerData.LocationX,
                ', ',
                this.state.playerData.LocationY,
                ')'
              )
            ),
            React.createElement(
              'tr',
              null,
              React.createElement(
                'td',
                null,
                React.createElement(
                  'b',
                  null,
                  'Hunger'
                )
              ),
              React.createElement(
                'td',
                null,
                this.state.playerData.Hunger,
                '/',
                this.state.playerData.MaxHunger
              )
            ),
            React.createElement(
              'tr',
              null,
              React.createElement(
                'td',
                null,
                React.createElement(
                  'b',
                  null,
                  'Thirst'
                )
              ),
              React.createElement(
                'td',
                null,
                this.state.playerData.Thirst,
                '/',
                this.state.playerData.MaxThirst
              )
            ),
            React.createElement(
              'tr',
              null,
              React.createElement(
                'td',
                null,
                React.createElement(
                  'b',
                  null,
                  'Consciousness'
                )
              ),
              React.createElement(
                'td',
                null,
                this.state.playerData.Consciousness,
                '/',
                this.state.playerData.MaxConsciousness
              )
            ),
            React.createElement(
              'tr',
              null,
              React.createElement(
                'td',
                null,
                React.createElement(
                  'b',
                  null,
                  'Submissiveness'
                )
              ),
              React.createElement(
                'td',
                null,
                this.state.playerData.Submissiveness,
                '/',
                this.state.playerData.MaxSubmissiveness
              )
            ),
            React.createElement(
              'tr',
              null,
              React.createElement(
                'td',
                null,
                React.createElement(
                  'b',
                  null,
                  'Breath'
                )
              ),
              React.createElement(
                'td',
                null,
                this.state.playerData.Breath,
                '/',
                this.state.playerData.MaxBreath
              )
            ),
            React.createElement(
              'tr',
              null,
              React.createElement(
                'td',
                null,
                React.createElement(
                  'b',
                  null,
                  'Will'
                )
              ),
              React.createElement(
                'td',
                null,
                this.state.playerData.Will,
                '/',
                this.state.playerData.MaxWill
              )
            )
          )
        ),
        opponentTable
      );
    },
    update: function () {
      this.updateCharacterPanel();
      this.updateEncounterData();
      this.updateTime();
    },
    updateEncounterData: function () {
      var me = this;
      getJSON("/encounter", function (resultBlob) {
        if (!resultBlob.inEncounter) {
          me.setState({ enemyData: null });
        } else {
          me.setState({ enemyData: resultBlob.opponent });
        }
      });
    },
    updateCharacterPanel: function () {
      var me = this;
      getJSON("/creature/player", function (resultBlob) {
        me.updateBiome(resultBlob);
        me.setState({ playerData: resultBlob });
      });
    },
    updateBiome: function (pData) {
      if (pData == null) {
        return;
      }
      var me = this;
      getJSON("/world/biome?x=" + pData.LocationX + "&y=" + pData.LocationY, function (resultBlob) {
        me.setState({ biomeData: resultBlob });
      });
    },
    updateTime: function () {
      var me = this;
      getJSON("/time", function (resultBlob) {
        me.setState({ time: resultBlob.time });
      });
    }
  });

  var TextPanel = React.createClass({
    displayName: 'TextPanel',

    getInitialState: function () {
      return {
        displayText: GlobalGameText
      };
    },
    componentDidMount: function () {
      this.scrollToBottom();
    },
    render: function () {
      toDisplay = "";
      for (var idx in this.state.displayText) {
        if (idx > 0) {
          toDisplay += "\n";
          toDisplay += "\n";
        }
        toDisplay += this.state.displayText[idx];
      }
      return React.createElement(Input, { type: 'textarea', style: { height: '500px', resize: 'none' }, value: toDisplay, readOnly: true });
    },
    componentDidUpdate: function () {
      this.scrollToBottom();
    },
    getLocDescs: function (textToAdd) {
      var me = this;
      var toPrint = textToAdd.trim();
      getJSON("/describe/me/location", function (returnInfo) {
        // Ugh fine...
        if (returnInfo.description != "") {
          toPrint += " " + returnInfo.description;
        }
        getJSON("/describe/me/location/resourceNodes", function (returnInfo) {
          // OK wtf nesting fetish?
          if (returnInfo.description != "") {
            toPrint += " " + returnInfo.description;
          }
          getJSON("/describe/me/location/creatures", function (returnInfo) {
            // Who did this?!
            if (returnInfo.description != "") {
              toPrint += " " + returnInfo.description;
            }
            getJSON("/describe/events", function (returnInfo) {
              // Fucking stupid shit.
              if (returnInfo.description != "") {
                toPrint += " " + returnInfo.description;
              }
              if (toPrint != "") {
                me.addDisplayText(toPrint);
                me.props.onUpdate();
              }
            });
          });
        });
      });
    },
    addDisplayText: function (text) {
      this.state.displayText.push(text);
      GlobalGameText = this.state.displayText.slice(0);
      this.setState({ displayText: this.state.displayText });
    },
    scrollToBottom: function () {
      // This works but is fucking stupid.
      var node = ReactDOM.findDOMNode(this).firstChild;
      node.scrollTop = node.scrollHeight;
    }
  });

  var GlobalGameText = new Array();

  var GamePanel = React.createClass({
    displayName: 'GamePanel',

    getInitialState: function () {
      return {
        isDead: false
      };
    },
    render: function () {
      return React.createElement(
        Grid,
        null,
        React.createElement(
          Row,
          null,
          React.createElement(
            Col,
            { sm: 3 },
            React.createElement(CharacterPanel, { ref: 'charPanel' })
          ),
          React.createElement(
            Col,
            { sm: 7 },
            React.createElement(TextPanel, { ref: 'textPanel', onUpdate: this.update })
          )
        ),
        React.createElement(
          Row,
          null,
          React.createElement(
            Col,
            { sm: 4 },
            React.createElement(MovePanel, { ref: 'movePanel', isDead: this.state.isDead, goNorth: this.goNorth, goEast: this.goEast, goSouth: this.goSouth, goWest: this.goWest })
          ),
          React.createElement(
            Col,
            { sm: 6 },
            React.createElement(Options, { ref: 'optionPanel', isDead: this.state.isDead, onActionText: this.addDisplayText })
          )
        )
      );
    },
    update: function () {
      this.ensureNotDead();
      this.refs.movePanel.updateAvailableMoves();
      this.refs.optionPanel.updateAvailableActions();
      this.refs.charPanel.update();
    },
    ensureNotDead: function () {
      var me = this;
      getJSON("/creature/player/isDead", function (responseBlob) {
        if (responseBlob.isDead != this.state.isDead) {
          me.setState({ isDead: responseBlob.isDead });
        }
        if (responseBlob.isDead) {
          me.addDisplayText("You are dead");
        }
      });
    },
    addDisplayText: function (text) {
      this.refs.textPanel.addDisplayText(text);
      this.update();
    },
    goDir: function (dir) {
      var myTextPanel = this.refs.textPanel;
      var me = this;
      postJSONCallback("/player/move", { dir: dir }, function (callback) {
        me.ensureNotDead();
        myTextPanel.getLocDescs(callback.text);
      });
    },
    goNorth: function () {
      this.goDir("n");
    },
    goEast: function () {
      this.goDir("e");
    },
    goSouth: function () {
      this.goDir("s");
    },
    goWest: function () {
      this.goDir("w");
    }
  });

  ////////////////////////////////////////////////////////////////////////////////
  // Inventory Tab
  ////////////////////////////////////////////////////////////////////////////////

  var InventoryPanel = React.createClass({
    displayName: 'InventoryPanel',

    refreshPanel: function () {
      this.forceUpdate();
    },
    getInitialState: function () {
      return { isDead: false };
    },
    componentWillMount: function () {
      this.update();
    },
    componentWillReceiveProps: function () {
      this.update();
    },
    update: function () {
      var me = this;
      getJSON("/creature/player/isDead", function (responseBlob) {
        if (responseBlob.isDead != this.state.isDead) {
          me.setState({ isDead: responseBlob.isDead });
        }
      });
    },
    render: function () {
      return React.createElement(
        Panel,
        null,
        React.createElement(
          Grid,
          null,
          React.createElement(
            Row,
            null,
            React.createElement(
              Col,
              { sm: 12 },
              React.createElement(InventoryTable, { isDead: this.state.isDead })
            )
          ),
          React.createElement(
            Row,
            null,
            React.createElement(
              Col,
              { sm: 12 },
              React.createElement(CraftingPanel, { isDead: this.state.isDead, onActionText: this.refreshPanel })
            )
          )
        )
      );
    }
  });

  var InventoryTable = React.createClass({
    displayName: 'InventoryTable',

    getInitialState: function () {
      return { data: [] };
    },
    componentDidMount: function () {
      getJSON("player/inventory", this.serverCallback);
    },
    componentWillReceiveProps: function () {
      getJSON("player/inventory", this.serverCallback);
    },
    serverCallback: function (inventory) {
      if (inventory === null) {
        this.setState({ data: [] });
      } else {
        this.setState({ data: inventory });
      }
    },
    consumeItem: function (idx) {
      var me = this;
      var item = this.state.data[idx];
      return function () {
        postJSONCallback("/player/inventory/consume", { kind: item.kind, id: item.id }, function (callback) {
          getJSON("player/inventory", me.serverCallback);
        });
      };
    },
    render: function () {
      if (this.state.data.length === 0) {
        return React.createElement(
          'div',
          null,
          React.createElement(
            'h2',
            null,
            'Inventory'
          ),
          React.createElement(
            'p',
            null,
            'You have no items in your inventory'
          )
        );
      }
      var statsValues = [];
      for (var i = 0; i < this.state.data.length; i++) {
        var row = null;
        if (this.state.data[i].consumable) {
          row = React.createElement(
            'tr',
            { key: i },
            React.createElement(
              'td',
              null,
              this.state.data[i].name
            ),
            React.createElement(
              'td',
              null,
              this.state.data[i].quantity
            ),
            React.createElement(
              'td',
              null,
              this.state.data[i].description
            ),
            React.createElement(
              'td',
              null,
              React.createElement(
                Button,
                { onClick: this.consumeItem(i) },
                'Consume'
              )
            )
          );
        } else {
          row = React.createElement(
            'tr',
            { key: i },
            React.createElement(
              'td',
              null,
              this.state.data[i].name
            ),
            React.createElement(
              'td',
              null,
              this.state.data[i].quantity
            ),
            React.createElement(
              'td',
              null,
              this.state.data[i].description
            ),
            React.createElement('td', null)
          );
        }
        statsValues.push(row);
      }
      return React.createElement(
        'div',
        null,
        React.createElement(
          'h2',
          null,
          'Inventory'
        ),
        React.createElement(
          Table,
          { condensed: true },
          React.createElement(
            'thead',
            null,
            React.createElement(
              'tr',
              null,
              React.createElement(
                'th',
                null,
                'Name'
              ),
              React.createElement(
                'th',
                null,
                'Quantity'
              ),
              React.createElement(
                'th',
                null,
                'Description'
              ),
              React.createElement(
                'th',
                null,
                'Action'
              )
            )
          ),
          React.createElement(
            'tbody',
            null,
            statsValues
          )
        )
      );
    }
  });

  var CraftingPanel = React.createClass({
    displayName: 'CraftingPanel',

    getInitialState: function () {
      return { actions: new Array() };
    },
    componentWillMount: function () {
      this.updateAvailableCraftingActions();
    },
    componentWillReceiveProps: function () {
      this.updateAvailableCraftingActions();
    },
    updateAvailableCraftingActions: function () {
      var me = this;
      getJSON("/player/crafting/available", function (resultBlob) {
        if (resultBlob == null) {
          me.setState({ actions: new Array() });
        } else {
          me.setState({ actions: resultBlob });
        }
      });
    },
    getOnClick: function (endpoint) {
      var me = this;
      return function () {
        postJSONCallback(endpoint, {}, function (callback) {
          me.props.onActionText(callback.text);
        });
      };
    },
    render: function () {
      if (this.state.actions.length === 0) {
        return React.createElement(
          'div',
          null,
          React.createElement(
            'h2',
            null,
            'Crafting'
          ),
          React.createElement(
            'p',
            null,
            'No recipes are available for crafting'
          )
        );
      }
      var buttonData = new Array();
      for (var i = 0; i < this.state.actions.length; i++) {
        buttonData.push({
          key: i,
          text: this.state.actions[i].description,
          onClickFunc: this.getOnClick(this.state.actions[i].endpoint),
          disabled: this.props.isDead
        });
      }
      var buttons = buttonData.map(function (button) {
        return React.createElement(
          Button,
          { key: button.key, onClick: button.onClickFunc },
          button.text
        );
      });
      return React.createElement(
        'div',
        null,
        React.createElement(
          'h2',
          null,
          'Crafting'
        ),
        React.createElement(
          ButtonGroup,
          { vertical: true, block: true },
          buttons
        )
      );
    }
  });

  ////////////////////////////////////////////////////////////////////////////////
  // Skills Tab
  ////////////////////////////////////////////////////////////////////////////////

  var SkillPanel = React.createClass({
    displayName: 'SkillPanel',

    getInitialState: function () {
      return {
        showSkillModal: false,
        showAlert: false,
        xp: {
          physical: 0,
          mental: 0,
          magical: 0,
          self: 0,
          terra: 0,
          fauna: 0
        },
        skill: {
          name: "Unknown",
          xp: 0
        }
      };
    },
    onSkillClicked: function (skill) {
      if (!skill.prereqsMet) {
        return;
      }
      var stateToSet = {
        showSkillModal: true,
        skill: skill
      };
      var getSourceFn = null;
      var sourceName = "";
      var getMediumFn = null;
      var mediumName = "";
      if (skill.source == "Physical") {
        getSourceFn = function (xp) {
          return xp.physical;
        };
        sourceName = "Physical";
      } else if (skill.source == "Mental") {
        getSourceFn = function (xp) {
          return xp.mental;
        };
        sourceName = "Mental";
      } else if (skill.source == "Magical") {
        getSourceFn = function (xp) {
          return xp.magical;
        };
        sourceName = "Magical";
      } else {
        console.log("Unknown source " + skill.source);return;
      }
      if (skill.medium == "Self") {
        getMediumFn = function (xp) {
          return xp.self;
        };
        mediumName = "Self";
      } else if (skill.medium == "Terra") {
        getMediumFn = function (xp) {
          return xp.terra;
        };
        mediumName = "Terra";
      } else if (skill.medium == "Fauna") {
        getMediumFn = function (xp) {
          return xp.fauna;
        };
        mediumName = "Fauna";
      } else {
        console.log("Unknown medium " + skill.medium);return;
      }
      var me = this;
      getJSON("/player/xp", function (resultBlob) {
        if (resultBlob != null) {
          resultBlob.source = getSourceFn(resultBlob);
          resultBlob.medium = getMediumFn(resultBlob);
          stateToSet.xp = resultBlob;
          if (skill.xp > resultBlob.source + resultBlob.medium) {
            me.onNotEnoughSkillPoints(skill, resultBlob.source, sourceName, resultBlob.medium, mediumName);
            stateToSet.showSkillModal = false;
          }
          me.setState(stateToSet);
        }
      });
    },
    onClose: function () {
      this.setState({ showSkillModal: false });
    },
    onNotEnoughSkillPoints: function (skill, sourceHave, sourceName, mediumHave, mediumName) {
      this.setState({
        showAlert: true,
        alertReason: "Not enough XP to buy. Have " + sourceHave + " " + sourceName + " and " + mediumHave + " " + mediumName + " but requires a total of " + skill.xp + " XP."
      });
    },
    onBuySkill: function (skill, sourcePts, mediumPts) {
      if (skill.xp != sourcePts + mediumPts) {
        this.setState({
          showAlert: true,
          alertReason: "Incorrect amount of XP to spend, need " + skill.xp + "."
        });
        this.onClose();
        return;
      }
      postJSON("/player/skills/learn", {
        skillId: skill.id,
        sourcePts: parseInt(sourcePts),
        mediumPts: parseInt(mediumPts)
      });
      this.onClose();
      this.updateXp();
    },
    handleAlertDismiss: function () {
      this.setState({ showAlert: false });
    },
    componentWillMount: function () {
      this.updateXp();
    },
    componentWillReceiveProps: function () {
      this.updateXp();
    },
    updateXp: function () {
      var me = this;
      getJSON("/player/xp", function (resultBlob) {
        if (resultBlob != null) {
          me.setState({ xp: resultBlob });
        }
      });
    },
    render: function () {
      var alert = React.createElement('div', null);
      if (this.state.showAlert) {
        alert = React.createElement(
          Alert,
          { bsStyle: 'danger', onDismiss: this.handleAlertDismiss },
          React.createElement(
            'h4',
            null,
            'Cannot Buy Skill'
          ),
          React.createElement(
            'p',
            null,
            this.state.alertReason
          )
        );
      }
      return React.createElement(
        Panel,
        null,
        React.createElement(SkillPayModal, { show: this.state.showSkillModal,
          xp: this.state.xp,
          skill: this.state.skill,
          source: this.state.source,
          medium: this.state.medium,
          onClose: this.onClose,
          onBuySkill: this.onBuySkill }),
        React.createElement(
          Grid,
          null,
          React.createElement(
            Row,
            null,
            React.createElement(
              Col,
              { sm: 4, smOffset: 4 },
              React.createElement(
                'p',
                null,
                'Navigate the skill tree below to buy new skills and view prerequisites.'
              )
            )
          ),
          React.createElement(
            Row,
            null,
            React.createElement(
              Col,
              { sm: 4, smOffset: 4 },
              React.createElement(
                Table,
                { condensed: true, width: "200px" },
                React.createElement(
                  'tbody',
                  null,
                  React.createElement(
                    'tr',
                    null,
                    React.createElement(
                      'td',
                      null,
                      React.createElement(
                        'b',
                        null,
                        'Physical'
                      )
                    ),
                    React.createElement(
                      'td',
                      null,
                      this.state.xp.physical
                    )
                  ),
                  React.createElement(
                    'tr',
                    null,
                    React.createElement(
                      'td',
                      null,
                      React.createElement(
                        'b',
                        null,
                        'Mental'
                      )
                    ),
                    React.createElement(
                      'td',
                      null,
                      this.state.xp.mental
                    )
                  ),
                  React.createElement(
                    'tr',
                    null,
                    React.createElement(
                      'td',
                      null,
                      React.createElement(
                        'b',
                        null,
                        'Magical'
                      )
                    ),
                    React.createElement(
                      'td',
                      null,
                      this.state.xp.magical
                    )
                  ),
                  React.createElement(
                    'tr',
                    null,
                    React.createElement(
                      'td',
                      null,
                      React.createElement(
                        'b',
                        null,
                        'Self'
                      )
                    ),
                    React.createElement(
                      'td',
                      null,
                      this.state.xp.self
                    )
                  ),
                  React.createElement(
                    'tr',
                    null,
                    React.createElement(
                      'td',
                      null,
                      React.createElement(
                        'b',
                        null,
                        'Terra'
                      )
                    ),
                    React.createElement(
                      'td',
                      null,
                      this.state.xp.terra
                    )
                  ),
                  React.createElement(
                    'tr',
                    null,
                    React.createElement(
                      'td',
                      null,
                      React.createElement(
                        'b',
                        null,
                        'Fauna'
                      )
                    ),
                    React.createElement(
                      'td',
                      null,
                      this.state.xp.fauna
                    )
                  )
                )
              )
            )
          ),
          React.createElement(
            Row,
            null,
            React.createElement(
              Col,
              { sm: 12 },
              alert
            )
          ),
          React.createElement(
            Row,
            null,
            React.createElement(
              Col,
              { sm: 12 },
              React.createElement(SkillGraph, { onSkillClicked: this.onSkillClicked, width: 1140, height: 720 })
            )
          )
        )
      );
    }
  });

  var SkillPayModal = React.createClass({
    displayName: 'SkillPayModal',

    render: function () {
      return React.createElement(
        Modal,
        { show: this.props.show },
        React.createElement(
          Modal.Header,
          null,
          React.createElement(
            Modal.Title,
            null,
            'Spend ',
            this.props.skill.xp,
            'XP for ',
            this.props.skill.name
          )
        ),
        React.createElement(
          Modal.Body,
          null,
          React.createElement(
            'h2',
            null,
            'Source'
          ),
          React.createElement(Input, { ref: 'source', type: 'number', label: this.props.skill.source + " (" + this.props.xp.source + ")", defaultValue: this.props.skill.xp / 2 }),
          React.createElement(
            'h2',
            null,
            'Medium'
          ),
          React.createElement(Input, { ref: 'medium', type: 'number', label: this.props.skill.medium + " (" + this.props.xp.medium + ")", defaultValue: this.props.skill.xp / 2 }),
          React.createElement(
            Button,
            { value: 'Spend Button', onClick: this.onSubmit },
            'Buy'
          ),
          React.createElement(
            Button,
            { value: 'Close Button', onClick: this.props.onClose },
            'Close'
          )
        )
      );
    },
    onSubmit: function (e) {
      e.preventDefault();
      this.props.onBuySkill(this.props.skill, parseInt(this.refs.source.getValue()), parseInt(this.refs.medium.getValue()));
    }
  });

  var SkillGraph = React.createClass({
    displayName: 'SkillGraph',

    getInitialState: function () {
      return {
        invalidColor: "red",
        availableColor: "blue",
        completedColor: "green",
        data: {
          nodes: [],
          links: []
        }
      };
    },
    componentWillMount: function () {
      this.updateSkills();
    },
    componentWillReceiveProps: function () {
      this.updateSkills();
    },
    updateSkills: function () {
      var me = this;
      getJSON("/player/skills", function (resultBlob) {
        if (resultBlob != null) {
          me.setState({ data: {
              nodes: resultBlob.skills,
              links: resultBlob.prerequesites
            } });
        }
      });
    },
    addTextNode: function (node) {
      var refNode = node.append("g").attr("id", function (d) {
        return d.id;
      });
      refNode.append("text").attr("dy", "-30px").attr("id", function (d) {
        return d.id + "SkillName";
      }).style("text-anchor", "middle").text(function (d) {
        return d.name;
      });
      refNode.append("text").attr("dy", "-20px").attr("font-size", "0.7em").style("text-anchor", "middle").text(function (d) {
        return d.description;
      });
      refNode.append("text").attr("dy", "5px").style("text-anchor", "middle").text(function (d) {
        return d.difficulty + ": " + d.source + " & " + d.medium;
      });
      refNode.append("text").attr("dy", "20px").style("text-anchor", "middle").text(function (d) {
        return "Level: " + d.currentLevel + " / " + d.maximumLevel;
      });
      refNode.append("text").attr("dy", "35px").style("text-anchor", "middle").text(function (d) {
        var xpString = "Requires " + d.xp + " XP";
        if (d.currentLevel == d.maximumLevel) {
          xpString = "At Maximum Level";
        }
        return xpString;
      });
    },
    onNodeClicked: function (node) {
      this.props.onSkillClicked(node);
    },
    addLinkTextNode: function (linkText) {
      linkText.append("text").attr("id", function (d) {
        return d.idRequired + d.idRequires;
      }).attr("dy", ".35em").attr("text-anchor", "middle").text(function (d) {
        return "" + d.requiredLevel;
      });
    },
    render: function () {
      var data = this.state.data;
      var svg = d3.select("svg");
      if (!svg.empty()) {
        svg.remove();
      }
      svg = d3.select(ReactDOM.findDOMNode(this)).append("svg");
      svg.attr("width", this.props.width).attr("height", this.props.height).attr("style", "outline: thin solid black;");
      svg.append("svg:defs").selectAll("marker").data(["end", "end-invalid", "end-active", "end-completed"]).enter().append("svg:marker").attr("id", String).attr("refX", 8).attr("viewBox", "0 -5 10 10").attr("markerWidth", 6).attr("markerHeight", 6).attr("orient", "auto").style("fill", "black").append("svg:path").attr("d", "M0,-5L10,0L0,5");
      var masterGroupSvg = svg.append("g");
      svg.call(d3.zoom().scaleExtent([1, 1]).on("zoom", function () {
        masterGroupSvg.attr("transform", d3.event.transform);
      }));
      svg.select("#end-invalid").style("fill", this.state.invalidColor);
      svg.select("#end-active").style("fill", this.state.availableColor);
      svg.select("#end-completed").style("fill", this.state.completedColor);
      var simulation = d3.forceSimulation(data.nodes).force("link", d3.forceLink(data.links)).force("charge", d3.forceManyBody()).force("center", d3.forceCenter(this.props.width / 2, this.props.height / 2)).force("collide", d3.forceCollide(110));
      var link = masterGroupSvg.append("g").attr("class", "links").selectAll("line").data(data.links).enter().append("line").attr("stroke", "black").attr("stroke-width", 1).attr("stroke-dasharray", function (d) {
        if (d.prereqMet) {
          return "0";
        }
        return "5, 5";
      }).attr("marker-end", "url(#end)");
      var linkText = masterGroupSvg.append("g").attr("class", "linkText").selectAll("text").data(data.links).enter().append("g").attr("class", "node").attr("transform", function (d) {
        return "translate(" + (d.source.x + d.target.x) / 2 + "," + (d.source.y + d.target.y) / 2 + ")";
      });
      // Text Link Node
      this.addLinkTextNode(linkText);
      linkText.append("rect").attr("x", function (d) {
        var el = document.getElementById(d.idRequired + d.idRequires);
        return -(Math.ceil(el.getBBox().width) + 5) / 2;
      }).attr("y", function (d) {
        var el = document.getElementById(d.idRequired + d.idRequires);
        return -(Math.ceil(el.getBBox().height) + 5) / 2;
      }).attr("width", function (d) {
        var el = document.getElementById(d.idRequired + d.idRequires);
        return Math.ceil(el.getBBox().width) + 5;
      }).attr("height", function (d) {
        var el = document.getElementById(d.idRequired + d.idRequires);
        return Math.ceil(el.getBBox().height) + 5;
      }).attr("fill", function (d) {
        return "white";
      }).attr("stroke-width", 1).attr("stroke", "black");
      this.addLinkTextNode(linkText);

      var node = masterGroupSvg.append("g").attr("class", "nodes").selectAll("circle").data(data.nodes).enter().append("g").attr("class", "node").attr("x", function (d) {
        return d.x;
      }).attr("y", function (d) {
        return d.y;
      }).attr("vx", function (d) {
        return 0;
      }).attr("vy", function (d) {
        return 0;
      }).on("click", this.onNodeClicked);
      // Text Node
      this.addTextNode(node);
      node.append("rect").attr("x", function (d) {
        var el = document.getElementById(d.id);
        return -(Math.ceil(el.getBBox().width) + 5) / 2;
      }).attr("y", function (d) {
        var el = document.getElementById(d.id);
        return -(Math.ceil(el.getBBox().height) + 20) / 2;
      }).attr("width", function (d) {
        var el = document.getElementById(d.id);
        return Math.ceil(el.getBBox().width) + 5;
      }).attr("height", function (d) {
        var el = document.getElementById(d.id);
        return Math.ceil(el.getBBox().height) + 20;
      }).attr("fill", function (d) {
        return "white";
      }).attr("stroke-width", 1).attr("shape-rendering", "crispEdges").attr("stroke-dasharray", function (d) {
        if (d.prereqsMet) {
          return "0";
        }
        return "5, 5";
      }).attr("stroke", "black");
      // Actually viewable
      this.addTextNode(node);

      var me = this;
      node.on("mouseover", function (d) {
        d3.select(this).selectAll("rect").attr("stroke", function (d) {
          if (d.prereqsMet) {
            if (d.maximumLevel == d.currentLevel) {
              return me.state.completedColor;
            }
            return me.state.availableColor;
          }
          return me.state.invalidColor;
        });
        d3.select(this).selectAll("rect").attr("stroke-width", 2);
        linkText.selectAll("rect").attr("stroke", function (l) {
          if (d === l.source) {
            if (l.prereqMet) {
              if (l.source.maximumLevel == l.source.currentLevel) {
                return me.state.completedColor;
              }
              return me.state.availableColor;
            }
            return me.state.invalidColor;
          }
          return "black";
        });
        link.style("stroke", function (l) {
          if (d === l.source) {
            if (l.prereqMet) {
              if (l.source.maximumLevel == l.source.currentLevel) {
                return me.state.completedColor;
              }
              return me.state.availableColor;
            }
            return me.state.invalidColor;
          }
          return "black";
        });
        link.attr("marker-end", function (l) {
          if (d === l.source) {
            if (l.prereqMet) {
              if (l.source.maximumLevel == l.source.currentLevel) {
                return "url(#end-completed)";
              }
              return "url(#end-active)";
            }
            return "url(#end-invalid)";
          }
          return "url(#end)";
        });
        link.attr("stroke-width", function (l) {
          if (d === l.source) {
            return 2;
          }
          return 1;
        });
      });

      node.on("mouseout", function () {
        d3.select(this).selectAll("rect").attr("stroke", "black");
        d3.select(this).selectAll("rect").attr("stroke-width", 1);
        linkText.selectAll("rect").attr("stroke", "black");
        link.style("stroke", "black");
        link.attr("stroke-width", 1);
        link.attr("marker-end", "url(#end)");
      });

      simulation.nodes(data.nodes).on("tick", function () {
        link.each(function (d) {
          var x1 = d.source.x,
              y1 = d.source.y,
              x2 = d.target.x,
              y2 = d.target.y;
          var currentAngle = Math.atan((y1 - y2) / (x1 - x2));
          var thisRect;
          node.each(function (n) {
            if (d.target.id == n.id) {
              thisRect = d3.select(this).select("rect");
            }
          });
          var halfWidth = thisRect.attr("width") / 2,
              halfHeight = thisRect.attr("height") / 2;
          var cornerAngle = Math.atan(halfHeight / halfWidth);
          var cornerDist = Math.sqrt(halfHeight * halfHeight + halfWidth * halfWidth);
          var targetDX = halfWidth;
          var targetDY = halfHeight;
          if (currentAngle < 0) {
            cornerAngle *= -1;
            targetDY *= -1;
          }
          if (currentAngle > 0) {
            if (currentAngle < cornerAngle) {
              // Right side: y = r*sin(th)
              targetDY = targetDX / Math.cos(currentAngle) * Math.sin(currentAngle);
            } else {
              // Top side
              targetDX = targetDY / Math.sin(currentAngle) * Math.cos(currentAngle);
            }
          } else if (currentAngle < 0) {
            if (currentAngle > cornerAngle) {
              // Right side
              targetDY = targetDX / Math.cos(currentAngle) * Math.sin(currentAngle);
            } else {
              // Bottom side
              targetDX = targetDY / Math.sin(currentAngle) * Math.cos(currentAngle);
            }
          }
          if (x2 > x1) {
            targetDX *= -1;
            targetDY *= -1;
          }
          d3.select(this).attr("x1", function (d) {
            return d.source.x;
          }).attr("y1", function (d) {
            return d.source.y;
          }).attr("x2", function (d) {
            return d.target.x + targetDX;
          }).attr("y2", function (d) {
            return d.target.y + targetDY;
          });
        });
        node.attr('transform', function (d, i) {
          return 'translate(' + d.x + ', ' + d.y + ')';
        });
        linkText.attr("transform", function (d) {
          return "translate(" + (d.source.x + d.target.x) / 2 + "," + (d.source.y + d.target.y) / 2 + ")";
        });
      });

      simulation.force("link").links(data.links);

      return React.createElement('div', null);
    }
  });

  ////////////////////////////////////////////////////////////////////////////////
  // Main Menu Tab
  ////////////////////////////////////////////////////////////////////////////////

  var CreateGameModal = React.createClass({
    displayName: 'CreateGameModal',

    render: function () {
      return React.createElement(
        Modal,
        { show: this.props.show },
        React.createElement(
          Modal.Header,
          null,
          React.createElement(
            Modal.Title,
            null,
            'Start New Game'
          )
        ),
        React.createElement(
          Modal.Body,
          null,
          React.createElement(Input, { ref: 'seed', type: 'number', label: 'Seed', placeholder: 'Enter a number' }),
          React.createElement(
            Input,
            { ref: 'sex', label: 'Sex' },
            React.createElement(Input, { name: 'sex', type: 'radio', ref: 'male', value: 'male', label: 'Male' }),
            React.createElement(Input, { name: 'sex', type: 'radio', ref: 'female', value: 'female', label: 'Female' })
          ),
          React.createElement(ButtonInput, { type: 'submit', value: 'New Game', onClick: this.onSubmit }),
          React.createElement(
            Button,
            { value: 'Close Button', onClick: this.props.onClose },
            'Close'
          )
        )
      );
    },
    onSubmit: function (e) {
      e.preventDefault();
      var seed = this.refs.seed.getValue();
      var sex = "";
      if (this.refs.male.getChecked()) {
        sex = "Male";
      } else if (this.refs.female.getChecked()) {
        sex = "Female";
      }
      GlobalGameText = new Array();
      this.props.onCreateGame(seed, sex);
    }
  });

  var SaveGameModal = React.createClass({
    displayName: 'SaveGameModal',

    render: function () {
      return React.createElement(
        Modal,
        { show: this.props.show },
        React.createElement(
          Modal.Header,
          null,
          React.createElement(
            Modal.Title,
            null,
            'Save Game'
          )
        ),
        React.createElement(
          Modal.Body,
          null,
          React.createElement(ButtonInput, { type: 'submit', value: 'Save', onClick: this.onSubmit }),
          React.createElement(
            Button,
            { value: 'Close Button', onClick: this.props.onClose },
            'Close'
          )
        )
      );
    },
    onSubmit: function (e) {
      e.preventDefault();
      this.props.onSaveGame();
    }
  });

  var LoadGameModal = React.createClass({
    displayName: 'LoadGameModal',

    render: function () {
      return React.createElement(
        Modal,
        { show: this.props.show },
        React.createElement(
          Modal.Header,
          null,
          React.createElement(
            Modal.Title,
            null,
            'Load Game'
          )
        ),
        React.createElement(
          Modal.Body,
          null,
          React.createElement(Input, { id: 'loadGameFile', ref: 'path', type: 'file', label: 'Load File' }),
          React.createElement(ButtonInput, { type: 'submit', value: 'Load', onClick: this.onSubmit }),
          React.createElement(
            Button,
            { value: 'Close Button', onClick: this.props.onClose },
            'Close'
          )
        )
      );
    },
    onSubmit: function (e) {
      e.preventDefault();
      var whatAStupidHackForGettingFiles = document.getElementById("loadGameFile").files[0];
      this.props.onLoadGame(whatAStupidHackForGettingFiles);
    }
  });

  var MenuPanel = React.createClass({
    displayName: 'MenuPanel',

    getInitialState: function () {
      return {
        showNewGameModal: false,
        saveGameModal: false,
        loadGameModal: false
      };
    },
    render: function () {
      return React.createElement(
        Panel,
        null,
        React.createElement(CreateGameModal, { show: this.state.showNewGameModal, onCreateGame: this.onNewGame, onClose: this.closeNewGameModal }),
        React.createElement(SaveGameModal, { show: this.state.saveGameModal, onSaveGame: this.onSaveGame, onClose: this.closeSaveGameModal }),
        React.createElement(LoadGameModal, { show: this.state.loadGameModal, onLoadGame: this.onLoadGame, onClose: this.closeLoadGameModal }),
        React.createElement(
          Grid,
          null,
          React.createElement(
            Row,
            null,
            React.createElement(
              Col,
              { sm: 4, smOffset: 4 },
              React.createElement(
                'h1',
                { className: 'text-center' },
                'The Island'
              ),
              React.createElement(
                ButtonGroup,
                { vertical: true, block: true },
                React.createElement(
                  Button,
                  { onClick: this.showNewGameModal },
                  'New Game'
                ),
                React.createElement(
                  Button,
                  { onClick: this.showSaveGameModal },
                  'Save Game'
                ),
                React.createElement(
                  Button,
                  { onClick: this.showLoadGameModal },
                  'Load Game'
                )
              )
            )
          ),
          React.createElement(
            Row,
            null,
            React.createElement(
              Col,
              { sm: 12 },
              React.createElement(
                'h2',
                { className: 'text-center' },
                'Changelog'
              ),
              'See the CHANGELOG file.'
            )
          )
        )
      );
    },
    showNewGameModal: function () {
      this.setState({ showNewGameModal: true });
    },
    closeNewGameModal: function () {
      this.setState({ showNewGameModal: false });
    },
    showSaveGameModal: function () {
      this.setState({ saveGameModal: true });
    },
    closeSaveGameModal: function () {
      this.setState({ saveGameModal: false });
    },
    showLoadGameModal: function () {
      this.setState({ loadGameModal: true });
    },
    closeLoadGameModal: function () {
      this.setState({ loadGameModal: false });
    },
    onNewGame: function (seed, sex) {
      postJSON("StartNewGame", { seed: parseInt(seed, 10), sex: sex });
      this.props.selectGameTab();
    },
    onSaveGame: function () {
      document.getElementById('stupidSaveHack').click();
      this.props.selectGameTab();
    },
    onLoadGame: function (file) {
      postFile("load", file);
      GlobalGameText = new Array();
      this.props.selectGameTab();
    }
  });

  ////////////////////////////////////////////////////////////////////////////////
  // Status Tab
  ////////////////////////////////////////////////////////////////////////////////

  var StatusPanel = React.createClass({
    displayName: 'StatusPanel',

    getInitialState: function () {
      return {
        serverState: {}
      };
    },
    componentDidMount: function () {
      getJSON("status", this.getServerStateCallback);
    },
    render: function () {
      var statsValues = [];
      for (var key in this.state.serverState.Stats) {
        if (this.state.serverState.Stats.hasOwnProperty(key)) {
          statsValues.push(React.createElement(
            'tr',
            { key: key },
            React.createElement(
              'td',
              null,
              key
            ),
            React.createElement(
              'td',
              null,
              this.state.serverState.Stats[key]
            )
          ));
        }
      }
      var registerStatsValues = [];
      for (var key in this.state.serverState.RegisterStats) {
        if (this.state.serverState.RegisterStats.hasOwnProperty(key)) {
          var row = React.createElement(
            'tr',
            { key: key },
            React.createElement(
              'td',
              null,
              this.state.serverState.RegisterStats[key].Endpoint
            ),
            React.createElement(
              'td',
              null,
              this.state.serverState.RegisterStats[key].Method
            ),
            React.createElement(
              'td',
              null,
              this.state.serverState.RegisterStats[key].Module
            ),
            React.createElement(
              'td',
              null,
              this.state.serverState.RegisterStats[key].Times
            )
          );
          registerStatsValues.push(row);
        }
      }
      return React.createElement(
        Panel,
        null,
        React.createElement(
          'h2',
          null,
          'Server Stats'
        ),
        React.createElement(
          Table,
          { condensed: true },
          React.createElement(
            'thead',
            null,
            React.createElement(
              'tr',
              null,
              React.createElement(
                'th',
                null,
                'Statistic'
              ),
              React.createElement(
                'th',
                null,
                'Value'
              )
            )
          ),
          React.createElement(
            'tbody',
            null,
            statsValues
          )
        ),
        React.createElement(
          'h2',
          null,
          'Plugin Endpoints Registered Stats'
        ),
        React.createElement(
          Table,
          { condensed: true },
          React.createElement(
            'thead',
            null,
            React.createElement(
              'tr',
              null,
              React.createElement(
                'th',
                null,
                'Endpoint'
              ),
              React.createElement(
                'th',
                null,
                'Method'
              ),
              React.createElement(
                'th',
                null,
                'Module'
              ),
              React.createElement(
                'th',
                null,
                'Times'
              )
            )
          ),
          React.createElement(
            'tbody',
            null,
            registerStatsValues
          )
        )
      );
    },
    getServerStateCallback: function (statusState) {
      var newState = this.state;
      newState.serverState = statusState;
      this.setState(newState);
    }
  });

  ////////////////////////////////////////////////////////////////////////////////
  // Manual Tab
  ////////////////////////////////////////////////////////////////////////////////

  var ManualButton = React.createClass({
    displayName: 'ManualButton',

    render: function () {
      return React.createElement(
        Button,
        { onClick: this.onClicked },
        this.props.title
      );
    },
    onClicked: function () {
      this.props.notifyClicked(this.props.title);
    }
  });

  var ManualPanel = React.createClass({
    displayName: 'ManualPanel',

    getInitialState: function () {
      return {
        activeTitle: "",
        activeEntry: {},
        manualTitles: []
      };
    },
    componentWillMount: function () {
      // Hail Lucifer
      if (document.addEventListener) {
        document.addEventListener('click', this.interceptClickEvent);
      } else if (document.attachEvent) {
        document.attachEvent('onclick', this.interceptClickEvent);
      }
    },
    componentWillUnmount: function () {
      // Hail Lucifer
      if (document.removeEventListener) {
        document.removeEventListener('click', this.interceptClickEvent);
      } else if (document.detachEvent) {
        document.detachEvent('onclick', this.interceptClickEvent);
      }
    },
    interceptClickEvent: function (e) {
      // Don't go to manual page directly b/c its in JSON.
      // Intercept and trigger load here instead.
      var target = e.target || e.srcElement;
      if (target.tagName === 'A') {
        var href = target.getAttribute('href');
        if (href.startsWith("/manual/")) {
          this.onTitleClicked(href.replace(/^\/manual\//, ''));
          e.preventDefault();
        }
      }
    },
    componentDidMount: function () {
      getJSON("manual", this.getManualTitlesCallback);
    },
    getManualEntry: function () {
      return { __html: this.state.activeEntry.Entry };
    },
    render: function () {
      var me = this;
      var manualTitleButtons = this.state.manualTitles.map(function (title) {
        return React.createElement(ManualButton, { key: title, notifyClicked: me.onTitleClicked, title: title });
      });
      var manualLabel = "";
      if (this.state.activeTitle !== "") {
        manualLabel = "Module: ";
      }
      return React.createElement(
        Panel,
        null,
        React.createElement(
          Grid,
          null,
          React.createElement(
            Row,
            null,
            React.createElement(
              Col,
              { sm: 4 },
              React.createElement(
                'h2',
                { className: 'text-center' },
                'Table of Contents'
              ),
              React.createElement(
                ButtonGroup,
                { vertical: true, block: true },
                manualTitleButtons
              )
            ),
            React.createElement(
              Col,
              { sm: 8 },
              React.createElement(
                'h2',
                null,
                this.state.activeTitle
              ),
              React.createElement(
                'p',
                null,
                manualLabel,
                React.createElement(
                  'i',
                  null,
                  this.state.activeEntry.Module
                )
              ),
              React.createElement('div', { dangerouslySetInnerHTML: this.getManualEntry() })
            )
          )
        )
      );
    },
    getManualTitlesCallback: function (manualTitles) {
      var newState = this.state;
      newState.manualTitles = manualTitles;
      this.setState(newState);
    },
    onTitleClicked: function (title) {
      var newState = this.state;
      newState.activeTitle = title;
      this.setState(newState);
      getJSON("manual/" + title, this.onContentFetched);
    },
    onContentFetched: function (entry) {
      var newState = this.state;
      newState.activeEntry = entry;
      this.setState(newState);
    }
  });

  ////////////////////////////////////////////////////////////////////////////////
  // Debug Panel
  ////////////////////////////////////////////////////////////////////////////////

  var DebugPanel = React.createClass({
    displayName: 'DebugPanel',

    getInitialState: function () {
      return {
        imageData: ""
      };
    },
    render: function () {
      var imgSrc = "data:image/png;base64," + this.state.imageData;
      return React.createElement(
        Panel,
        null,
        React.createElement(
          'h1',
          null,
          'Debug'
        ),
        React.createElement(
          Grid,
          null,
          React.createElement(
            Row,
            null,
            React.createElement(
              Col,
              { sm: 4 },
              React.createElement(
                ButtonGroup,
                { vertical: true },
                React.createElement(
                  Button,
                  { onClick: this.onShowMap },
                  'Map Image'
                ),
                React.createElement(
                  Button,
                  { onClick: this.onShowDownslope },
                  'Downslope Image'
                ),
                React.createElement(
                  Button,
                  { onClick: this.onShowCoast },
                  'Coast Points Image'
                )
              )
            ),
            React.createElement(
              Col,
              { sm: 7 },
              this.state.imageData != "" ? React.createElement('img', { src: imgSrc }) : null
            )
          )
        )
      );
    },
    onShowMap: function () {
      this.onShowImage("/world/image");
    },
    onShowDownslope: function () {
      this.onShowImage("/world/downslope/image");
    },
    onShowCoast: function () {
      this.onShowImage("/world/coast/points/image");
    },
    onShowImage: function (endpoint) {
      getJSON(endpoint, this.onImageFetched);
    },
    onImageFetched: function (imageData) {
      var newState = this.state;
      newState.imageData = imageData.image;
      this.setState(newState);
    }
  });

  ////////////////////////////////////////////////////////////////////////////////
  // Main Panel & Nav Bar
  ////////////////////////////////////////////////////////////////////////////////

  var MainPanel = React.createClass({
    displayName: 'MainPanel',

    render: function () {
      var toReturn = {};
      if (this.props.activeKey == 1) {
        // Menu
        toReturn = React.createElement(MenuPanel, { selectGameTab: this.props.selectGameTab });
      } else if (this.props.activeKey == 2) {
        // Game
        toReturn = React.createElement(
          Panel,
          null,
          React.createElement(GamePanel, null)
        );
      } else if (this.props.activeKey == 3) {
        // Status
        toReturn = React.createElement(StatusPanel, null);
      } else if (this.props.activeKey == 4) {
        // Manual
        toReturn = React.createElement(ManualPanel, null);
      } else if (this.props.activeKey == 5) {
        // Debug
        toReturn = React.createElement(DebugPanel, null);
      } else if (this.props.activeKey == 6) {
        // Inventory
        toReturn = React.createElement(InventoryPanel, null);
      } else if (this.props.activeKey == 7) {
        // Skills
        toReturn = React.createElement(SkillPanel, null);
      }
      return toReturn;
    }
  });

  var Game = React.createClass({
    displayName: 'Game',

    getInitialState: function () {
      return {
        active: 1,
        debug: false
      };
    },
    componentWillMount: function () {
      var me = this;
      cheet('↑ ↑ ↓ ↓ ← → ← → b a', function () {
        var newState = me.state;
        newState.debug = !newState.debug;
        if (!newState.debug && newState.active == 5) {
          newState.active = 1;
        } else if (newState.debug) {
          newState.active = 5;
        }
        me.setState(newState);
      });
    },
    render: function () {
      return React.createElement(
        'div',
        null,
        React.createElement(
          Nav,
          { bsStyle: 'tabs', activeKey: this.state.active, onSelect: this.selected },
          React.createElement(
            NavItem,
            { eventKey: 1 },
            'Main Menu'
          ),
          React.createElement(
            NavItem,
            { eventKey: 2 },
            'Game'
          ),
          React.createElement(
            NavItem,
            { eventKey: 6 },
            'Inventory'
          ),
          React.createElement(
            NavItem,
            { eventKey: 7 },
            'Skills'
          ),
          React.createElement(
            NavItem,
            { eventKey: 4 },
            'Manual'
          ),
          React.createElement(
            NavItem,
            { eventKey: 3 },
            'Status'
          ),
          this.state.debug ? React.createElement(
            NavItem,
            { eventKey: 5 },
            'Debug'
          ) : null
        ),
        React.createElement(MainPanel, { selectGameTab: this.selectGame, gameState: this.props.gameState, activeKey: this.state.active })
      );
    },
    selected: function (key) {
      var newState = this.state;
      newState.active = key;
      this.setState(newState);
    },
    selectGame: function (reloadData) {
      this.selected(2, reloadData);
    }
  });

  var FetchWarning = React.createClass({
    displayName: 'FetchWarning',

    getInitialState: function () {
      return { warning: "You are using a browser that does not support the fetch API." + " Please use a real browser like Chrome or Firefox." };
    },
    render: function () {
      return React.createElement(
        'p',
        null,
        this.state.warning
      );
    }
  });

  return {
    Island: React.createClass({
      displayName: 'Island',

      render: function () {
        return React.createElement(Game, null);
      }
    })
  };
}();

if (self.fetch) {
  ReactDOM.render(React.createElement(
    Island.Island,
    null,
    'Test'
  ), document.getElementById("island"));
} else {
  ReactDOM.render(React.createElement(
    Island.FetchWarning,
    null,
    'Use A Real Browser'
  ), document.getElementById("island"));
}