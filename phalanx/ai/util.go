package ai

import (
	"math/rand"
	"time"
)

const (
	StateEmpty      = ""
	StateMoving     = "mo"
	StateStationary = "st"
)

type TimeDist interface {
	Duration(r *rand.Rand) time.Duration
}

type IntDist interface {
	Int(r *rand.Rand) int
}

type UniformTimeDist struct {
	Min time.Duration
	Max time.Duration
}

func (u UniformTimeDist) Duration(r *rand.Rand) time.Duration {
	return time.Duration(r.Int63n(int64(u.Max-u.Min))) + u.Min
}

type UniformIntDist struct {
	Min int
	Max int
}

func (u UniformIntDist) Int(r *rand.Rand) int {
	return r.Intn(u.Max-u.Min) + u.Min
}
