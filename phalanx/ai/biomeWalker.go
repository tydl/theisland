package ai

import (
	"math/rand"
	"time"

	"gitlab.com/tydl/theisland/viking/availableActions"
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/daynight"
	"gitlab.com/tydl/theisland/viking/encounter"
	"gitlab.com/tydl/theisland/viking/errors"
	"gitlab.com/tydl/theisland/viking/world"
)

const (
	maxAttempts = 24
)

var _ creature.TimePassHandler = &BiomeWalker{}

type BiomeWalker struct {
	MoveTick       time.Duration
	TimeNotMoving  TimeDist
	DistanceMoving IntDist
}

func (b BiomeWalker) Handle(c *creature.NpcCreature, rng *rand.Rand, t time.Duration, s core.Systemer) error {
	if err := availableActions.NpcCannotBeInEncounter(s, c.Id); err != nil {
		return nil
	}
	dayNightSys, err := daynight.GetDayNightSystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	now := dayNightSys.Now()
	if c.Meta.State == StateEmpty {
		b.setStationary(c, now, rng)
	}
	if now.After(c.Meta.NextMove) {
		if err := b.handleMove(c, now, rng, s); err != nil {
			return errors.StackWrap(err)
		}
	}
	return nil
}

func (b BiomeWalker) setStationary(c *creature.NpcCreature, now time.Time, rng *rand.Rand) {
	c.Meta.State = StateStationary
	c.Meta.NextMove = now.Add(b.TimeNotMoving.Duration(rng))
	c.Meta.MoveDistance = b.DistanceMoving.Int(rng)
}

func (b BiomeWalker) handleMove(c *creature.NpcCreature, now time.Time, rng *rand.Rand, s core.Systemer) error {
	mapSys, err := world.GetMapSystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	eventer, err := core.GetAddEventer(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	encounterSys, err := encounter.GetEncounterSystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	pcSys, err := creature.GetPlayerSystem(s)
	if err != nil {
		return errors.StackWrap(err)
	}
	c.Meta.State = StateMoving
	attempt := 0 // Procedural gen could put lone creature on 1-square space one day
	for now.After(c.Meta.NextMove) && c.Meta.MoveDistance > 0 && attempt < maxAttempts {
		nextX, nextY := b.getRandomDirection(c.Creature.LocationX, c.Creature.LocationY, rng)
		biome := mapSys.GetBiome(nextX, nextY)
		if biome.Name() != c.Meta.HomeBiome {
			attempt++
			continue
		}
		eventer.AddEvent(&creature.NpcLeaveLocationEvent{
			X:   c.Creature.LocationX,
			Y:   c.Creature.LocationY,
			Npc: c,
		})
		c.Meta.NextMove = c.Meta.NextMove.Add(b.MoveTick)
		c.Meta.MoveDistance = c.Meta.MoveDistance - 1
		c.Creature.LocationX, c.Creature.LocationY = nextX, nextY
		eventer.AddEvent(&creature.NpcLeaveLocationEvent{
			X:   c.Creature.LocationX,
			Y:   c.Creature.LocationY,
			Npc: c,
		})
		playerX, playerY := pcSys.GetPlayerLocation()
		if !encounterSys.IsInEncounter() && playerX == nextX && playerY == nextY {
			if err := encounterSys.BeginEncounter(c.Id); err != nil {
				return errors.StackWrap(err)
			}
			b.setStationary(c, now, rng)
			eventer.AddEvent(&encounter.EncounterBeginEvent{})
			break
		}
		attempt = 0
	}
	if c.Meta.MoveDistance == 0 {
		b.setStationary(c, now, rng)
	}
	return nil
}

func (b BiomeWalker) getRandomDirection(x, y int, rng *rand.Rand) (int, int) {
	dir := rng.Intn(8)
	switch dir {
	case 0:
		return x + 1, y
	case 1:
		return x + 1, y + 1
	case 2:
		return x, y + 1
	case 3:
		return x - 1, y + 1
	case 4:
		return x - 1, y
	case 5:
		return x - 1, y - 1
	case 6:
		return x, y - 1
	case 7:
		return x + 1, y - 1
	}
	return x, y
}
