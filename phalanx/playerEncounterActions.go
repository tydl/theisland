package phalanx

import (
	"time"

	"gitlab.com/tydl/theisland/viking/anatomy"
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/encounter"
)

var _ encounter.Action = &SimpleAction{}

type SimpleAction struct {
	name            string
	description     string
	executeFn       func(game core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (string, time.Duration, error)
	applicabilityFn func(s core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (bool, error)
}

func (s SimpleAction) Name() string {
	return s.name
}

func (s SimpleAction) Description() string {
	return s.description
}

func (s SimpleAction) Execute(game core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (string, time.Duration, error) {
	return s.executeFn(game, self, other, selfAnat, otherAnat)
}

func (s SimpleAction) IsApplicableForPlayer(game core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (bool, error) {
	return s.applicabilityFn(game, self, other, selfAnat, otherAnat)
}

func getPlayerEncounterActions() []encounter.Action {
	return []encounter.Action{
		&SimpleAction{
			name:        "laugh",
			description: "Laugh and giggle",
			executeFn: func(game core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (string, time.Duration, error) {
				other.Submissiveness -= 50
				return "You laugh and giggle", time.Second, nil
			},
			applicabilityFn: func(s core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (bool, error) {
				return true, nil
			},
		},
	}
}
