package phalanx

import (
	"gitlab.com/tydl/theisland/viking/availableActions"
	"gitlab.com/tydl/theisland/viking/crafting"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/encounter"
	"gitlab.com/tydl/theisland/viking/inventory"
	"gitlab.com/tydl/theisland/viking/resources"
	"gitlab.com/tydl/theisland/viking/skills"
	"gitlab.com/tydl/theisland/viking/templates"
)

type Injector struct {
	templateEntries            []templates.TemplateEntry
	resourceNodeFactories      []resources.ResourceNodeFactory
	harvestResourceNodeActions []availableActions.HarvestResourceNodeAction
	craftingRecipes            []crafting.CraftRecipe
	npcFactories               []creature.NpcFactory
	npcCombatAi                map[string]encounter.CombatHandler
	playerEncounterActions     []encounter.Action
	gameSkills                 []skills.Skill
	itemMetadata               []inventory.ItemMetadata
}

func NewInjector() *Injector {
	return (&Injector{}).init()
}

func (i *Injector) init() *Injector {
	var entries []templates.TemplateEntry
	i.fromResourceNodeDefinitions(getResNodes())
	i.craftingRecipes, entries = getCraftingRecipes()
	i.itemMetadata = getItemMetadata()
	i.appendTemplateEntries(entries)
	i.npcFactories, i.npcCombatAi, entries = getNpcFactories()
	i.appendTemplateEntries(entries)
	i.playerEncounterActions = getPlayerEncounterActions()
	i.gameSkills = getGameSkills()
	return i
}

func (i *Injector) fromResourceNodeDefinitions(defs []resourceNodeDefinition) {
	for _, def := range defs {
		i.resourceNodeFactories = append(i.resourceNodeFactories, def)
		i.harvestResourceNodeActions = append(i.harvestResourceNodeActions, def.ToHarvestResourceNodeAction())
		i.appendTemplateEntries(def.ToTemplateEntries())
	}
}

func (i *Injector) appendTemplateEntries(entries []templates.TemplateEntry) {
	i.templateEntries = append(i.templateEntries, entries...)
}

func (i Injector) GetResourceNodeFactories() []resources.ResourceNodeFactory {
	return i.resourceNodeFactories
}

func (i Injector) GetHarvestResourceNodeActions() []availableActions.HarvestResourceNodeAction {
	return i.harvestResourceNodeActions
}

func (i Injector) GetCraftingRecipes() []crafting.CraftRecipe {
	return i.craftingRecipes
}

func (i Injector) GetNpcFactories() []creature.NpcFactory {
	return i.npcFactories
}

func (i Injector) GetTemplateEntries() []templates.TemplateEntry {
	return i.templateEntries
}

func (i Injector) GetCombatHandlers() map[string]encounter.CombatHandler {
	return i.npcCombatAi
}

func (i Injector) GetEncounterActions() []encounter.Action {
	return i.playerEncounterActions
}

func (i Injector) GetGameSkills() []skills.Skill {
	return i.gameSkills
}

func (i Injector) GetItemMetadata() []inventory.ItemMetadata {
	return i.itemMetadata
}
