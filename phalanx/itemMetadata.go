package phalanx

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/inventory"
)

func getItemMetadata() []inventory.ItemMetadata {
	return []inventory.ItemMetadata{
		{
			Kind:       "yetAgainAnotherItem",
			Id:         0,
			Consumable: func(i *inventory.InventoryItem) bool { return true },
			Consume: func(i *inventory.InventoryItem, s core.Systemer) error {
				pSys, err := creature.GetPlayerSystem(s)
				if err != nil {
					return err
				}
				pSys.AddHunger(10).AddThirst(5)
				return nil
			},
		},
	}
}
