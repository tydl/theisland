package phalanx

import (
	"time"

	"gitlab.com/tydl/theisland/viking/anatomy"
	"gitlab.com/tydl/theisland/viking/availableActions"
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/mapgen"
	"gitlab.com/tydl/theisland/viking/resources"
	"gitlab.com/tydl/theisland/viking/skills"
	"gitlab.com/tydl/theisland/viking/templates"
)

type resourceNodeDefinition struct {
	// TemplateEntries for descriptive content
	Entries []templates.TemplateEntry
	// Harvesting
	NodeId         resources.ResourceNodeId
	DescriptionId  string
	HasAnatomy     func(*anatomy.Head) bool
	BaseHarvestMap map[string]func(s core.Systemer) error
	// Spawning
	InterfaceText               resources.Text
	InterfaceSpawnsInBiome      map[mapgen.Biome]int
	InterfaceBaseHarvest        resources.BaseHarvest
	InterfaceTickInterval       time.Duration
	InterfaceBaseQuantityGrowth map[resources.ResourceId]resources.QuantityGrowth
	InterfaceInitialQuantity    map[resources.ResourceId]resources.BoundedQuantity
}

func (r resourceNodeDefinition) ToHarvestResourceNodeAction() availableActions.HarvestResourceNodeAction {
	return availableActions.HarvestResourceNodeAction{
		NodeId:        r.NodeId,
		DescriptionId: r.DescriptionId,
		HasAnatomy:    r.HasAnatomy,
	}
}

func (r resourceNodeDefinition) ToTemplateEntries() []templates.TemplateEntry {
	return r.Entries
}

func (r resourceNodeDefinition) IdSpawned() resources.ResourceNodeId {
	return r.NodeId
}

func (r resourceNodeDefinition) Text() resources.Text {
	return r.InterfaceText
}

func (r resourceNodeDefinition) NumberSpawnsInBiome(b mapgen.Biome) int {
	if val, ok := r.InterfaceSpawnsInBiome[b]; !ok {
		return 0
	} else {
		return val
	}
}

func (r resourceNodeDefinition) BaseHarvest() resources.BaseHarvest {
	return r.InterfaceBaseHarvest
}

func (r resourceNodeDefinition) TickInterval() time.Duration {
	return r.InterfaceTickInterval
}

func (r resourceNodeDefinition) BaseQuantityGrowth() map[resources.ResourceId]resources.QuantityGrowth {
	return r.InterfaceBaseQuantityGrowth
}

func (r resourceNodeDefinition) InitialQuantity(res resources.ResourceId) resources.BoundedQuantity {
	return r.InterfaceInitialQuantity[res]
}

func (r resourceNodeDefinition) OnHarvested() map[string]func(s core.Systemer) error {
	return r.BaseHarvestMap
}

func getResNodes() []resourceNodeDefinition {
	return []resourceNodeDefinition{
		testNode,
	}
}

var testNode = resourceNodeDefinition{
	Entries: []templates.TemplateEntry{
		{"testNodeEntry", "This is a test description"},
		{"testSeeResourceNode", "Test Description"},
		{"testItemEntry", "This is a description of the test item."},
	},
	// Harvesting
	NodeId:        resources.ResourceNodeId("testNode"),
	DescriptionId: "testNodeEntry",
	HasAnatomy: func(h *anatomy.Head) bool {
		return true
	},
	BaseHarvestMap: map[string]func(core.Systemer) error{
		"baseHarvestId": func(s core.Systemer) error {
			pSkill, err := skills.GetPlayerSkillSystem(s)
			if err != nil {
				return err
			}
			pSkill.AddMentalPoints(50)
			pSkill.AddFaunaPoints(50)
			return nil
		},
	},
	// Spawning
	InterfaceText: resources.Text{DescriptionIds: []string{"testSeeResourceNode"}, Name: "Test Name"},
	InterfaceSpawnsInBiome: map[mapgen.Biome]int{
		&mapgen.OceanBiome{}:                  200,
		&mapgen.MarshBiome{}:                  200,
		&mapgen.IceBiome{}:                    200,
		&mapgen.LakeBiome{}:                   200,
		&mapgen.BeachBiome{}:                  200,
		&mapgen.SnowBiome{}:                   200,
		&mapgen.TundraBiome{}:                 200,
		&mapgen.BareBiome{}:                   200,
		&mapgen.ScorchedBiome{}:               200,
		&mapgen.TaigaBiome{}:                  200,
		&mapgen.ShrublandBiome{}:              200,
		&mapgen.TemperateDesertBiome{}:        200,
		&mapgen.TemperateRainforestBiome{}:    200,
		&mapgen.TemperateDeciduousBiome{}:     200,
		&mapgen.GrasslandBiome{}:              200,
		&mapgen.TropicalRainforestBiome{}:     200,
		&mapgen.TropicalSeasonalForestBiome{}: 200,
		&mapgen.SubtropicalDesertBiome{}:      200,
	},
	InterfaceBaseHarvest: resources.BaseHarvest{
		Id: "baseHarvestId",
		ResourceQuantity: map[resources.ResourceId]resources.HarvestReapItemPrototype{
			resources.ResourceId("testItem"): resources.HarvestReapItemPrototype{
				Quantity:      7,
				Id:            0,
				Name:          "Test Item",
				DescriptionId: "testItemEntry",
				Metadata: map[string]interface{}{
					"meta0": "Hi",
				},
			},
		},
		Time: time.Hour,
	},
	InterfaceTickInterval: time.Hour,
	InterfaceBaseQuantityGrowth: map[resources.ResourceId]resources.QuantityGrowth{
		resources.ResourceId("testItem"): {
			Resource: resources.ResourceId("testItem"),
			Quantity: 10,
			Growth:   resources.Med,
		},
	},
	InterfaceInitialQuantity: map[resources.ResourceId]resources.BoundedQuantity{
		resources.ResourceId("testItem"): {Current: 50, Maximum: 100},
	},
}
