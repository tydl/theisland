package phalanx

import (
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/skills"
)

func getGameSkills() []skills.Skill {
	return []skills.Skill{
		testSkill1,
		testSkill2,
		testSkill3,
		testSkill4,
		testSkill5,
		testSkill6,
		testSkill7,
		testSkill8,
	}
}

var testSkill1 = skills.Skill{
	Id:            "TestSkill1",
	Name:          "Test Skill Name 1",
	Description:   "This tests some stuff for skills 1",
	Difficulty:    skills.Trivial,
	Prerequesites: []skills.Prerequesite{},
	Source:        skills.Physical,
	Medium:        skills.Self,
	X:             50,
	Y:             50,
	Applies:       func(s skills.SkillEffectPayload) bool { return true },
	OnLevelUp:     func(s core.Systemer) error { return nil },
}
var testSkill2 = skills.Skill{
	Id:            "2",
	Name:          "TOOT TOOT",
	Description:   "Choo choo",
	Difficulty:    skills.VeryEasy,
	Prerequesites: []skills.Prerequesite{},
	Source:        skills.Mental,
	Medium:        skills.Fauna,
	X:             150,
	Y:             50,
	Applies:       func(s skills.SkillEffectPayload) bool { return true },
	OnLevelUp:     func(s core.Systemer) error { return nil },
}
var testSkill3 = skills.Skill{
	Id:            "TestSkill",
	Name:          "Test Skill Name",
	Description:   "This tests some stuff for skills",
	Difficulty:    skills.Easy,
	Prerequesites: []skills.Prerequesite{},
	Source:        skills.Magical,
	Medium:        skills.Terra,
	X:             250,
	Y:             50,
	Applies:       func(s skills.SkillEffectPayload) bool { return true },
	OnLevelUp:     func(s core.Systemer) error { return nil },
}
var testSkill4 = skills.Skill{
	Id:          "Layer2",
	Name:        "Fight",
	Description: "Boom pow bang bang",
	Difficulty:  skills.Medium,
	Prerequesites: []skills.Prerequesite{
		{SkillId: "TestSkill1", Level: 1},
		{SkillId: "2", Level: 2},
	},
	Source:    skills.Physical,
	Medium:    skills.Terra,
	X:         50,
	Y:         150,
	Applies:   func(s skills.SkillEffectPayload) bool { return true },
	OnLevelUp: func(s core.Systemer) error { return nil },
}
var testSkill5 = skills.Skill{
	Id:          "Another",
	Name:        "Magicland",
	Description: "Magic stuff wut",
	Difficulty:  skills.Tough,
	Prerequesites: []skills.Prerequesite{
		{SkillId: "2", Level: 2},
		{SkillId: "TestSkill", Level: 3},
	},
	Source:    skills.Magical,
	Medium:    skills.Self,
	X:         50,
	Y:         150,
	Applies:   func(s skills.SkillEffectPayload) bool { return true },
	OnLevelUp: func(s core.Systemer) error { return nil },
}
var testSkill6 = skills.Skill{
	Id:          "6",
	Name:        "Test Skill Name",
	Description: "This tests some stuff for skills",
	Difficulty:  skills.VeryTough,
	Prerequesites: []skills.Prerequesite{
		{SkillId: "Layer2", Level: 4},
	},
	Source:    skills.Mental,
	Medium:    skills.Fauna,
	X:         50,
	Y:         250,
	Applies:   func(s skills.SkillEffectPayload) bool { return true },
	OnLevelUp: func(s core.Systemer) error { return nil },
}
var testSkill7 = skills.Skill{
	Id:          "7",
	Name:        "Wait For It",
	Description: "Legen--wait for it--dary",
	Difficulty:  skills.Legendary,
	Prerequesites: []skills.Prerequesite{
		{SkillId: "Layer2", Level: 5},
		{SkillId: "Another", Level: 6},
	},
	Source:    skills.Physical,
	Medium:    skills.Fauna,
	X:         150,
	Y:         250,
	Applies:   func(s skills.SkillEffectPayload) bool { return true },
	OnLevelUp: func(s core.Systemer) error { return nil },
}
var testSkill8 = skills.Skill{
	Id:          "TestSk88",
	Name:        "The Ocho",
	Description: "This tests some 888888s",
	Difficulty:  skills.Medium,
	Prerequesites: []skills.Prerequesite{
		{SkillId: "Another", Level: 7},
	},
	Source:    skills.Mental,
	Medium:    skills.Terra,
	X:         250,
	Y:         250,
	Applies:   func(s skills.SkillEffectPayload) bool { return true },
	OnLevelUp: func(s core.Systemer) error { return nil },
}
