package phalanx

import (
	"time"

	"gitlab.com/tydl/theisland/viking/crafting"
	"gitlab.com/tydl/theisland/viking/inventory"
	"gitlab.com/tydl/theisland/viking/templates"
)

func getCraftingRecipes() ([]crafting.CraftRecipe, []templates.TemplateEntry) {
	return []crafting.CraftRecipe{
			testRecipe1,
			testRecipe2,
		}, []templates.TemplateEntry{
			anotherItemEntry,
			yetAnotherItemEntry,
			yetAgainAnotherItemEntry,
		}
}

var testRecipe1 = crafting.CraftRecipe{
	Id:        1,
	CraftTime: time.Hour,
	Consumed: []crafting.RecipeItem{
		{
			Kind:     "testItem",
			Id:       0,
			Quantity: 1,
		},
	},
	Produced: []inventory.InventoryItemQuantity{
		{
			Item: inventory.InventoryItem{
				Kind:          "anotherItem",
				Id:            0,
				Name:          "Recipe Item 1",
				DescriptionId: "anotherItemEntry",
				Metadata:      map[string]interface{}{},
			},
			Quantity: 1,
		},
	},
}
var anotherItemEntry = templates.TemplateEntry{
	Name:    "anotherItemEntry",
	Content: "An item crafted from a resource",
}

var testRecipe2 = crafting.CraftRecipe{
	Id:        2,
	CraftTime: time.Minute,
	Consumed: []crafting.RecipeItem{
		{
			Kind:     "testItem",
			Id:       0,
			Quantity: 3,
		},
		{
			Kind:     "anotherItem",
			Id:       0,
			Quantity: 5,
		},
	},
	Produced: []inventory.InventoryItemQuantity{
		{
			Item: inventory.InventoryItem{
				Kind:          "yetAnotherItem",
				Id:            0,
				Name:          "Recipe Item 2",
				DescriptionId: "yetAnotherItemEntry",
				Metadata:      map[string]interface{}{},
			},
			Quantity: 1,
		},
		{
			Item: inventory.InventoryItem{
				Kind:          "yetAgainAnotherItem",
				Id:            0,
				Name:          "Recipe Item 2.1",
				DescriptionId: "yetAgainAnotherItemEntry",
				Metadata:      map[string]interface{}{},
			},
			Quantity: 1,
		},
	},
}
var yetAnotherItemEntry = templates.TemplateEntry{
	Name:    "yetAnotherItemEntry",
	Content: "An item crafted from a resource and Recipe Item 1",
}
var yetAgainAnotherItemEntry = templates.TemplateEntry{
	Name:    "yetAgainAnotherItemEntry",
	Content: "An item crafted from a resource and Recipe Item 1, but a different one",
}
