package phalanx

import (
	"math/rand"
	"time"

	"gitlab.com/tydl/theisland/phalanx/ai"
	"gitlab.com/tydl/theisland/viking/anatomy"
	"gitlab.com/tydl/theisland/viking/core"
	"gitlab.com/tydl/theisland/viking/creature"
	"gitlab.com/tydl/theisland/viking/encounter"
	"gitlab.com/tydl/theisland/viking/mapgen"
	"gitlab.com/tydl/theisland/viking/templates"
)

var _ creature.NpcFactory = &SimpleNpcFactory{}
var _ encounter.CombatHandler = &SimpleCombatHandler{}

type SimpleNpcFactory struct {
	spawnsInBiome   map[string]int
	baseCreature    creature.Creature
	kind            string
	descriptions    []string
	timePasserAi    creature.TimePassHandler
	anatomyCreateFn func() *anatomy.Head
}

func (s SimpleNpcFactory) NumberSpawnsInBiome(b mapgen.Biome) int {
	val, ok := s.spawnsInBiome[b.Name()]
	if !ok {
		return 0
	}
	return val
}

func (s SimpleNpcFactory) Creature(b mapgen.Biome, r *rand.Rand) *creature.Creature {
	return &s.baseCreature
}

func (s SimpleNpcFactory) Kind() string {
	return s.kind
}

func (s SimpleNpcFactory) DescriptionIds() []string {
	return s.descriptions
}

func (s SimpleNpcFactory) TimePassHandler() creature.TimePassHandler {
	return s.timePasserAi
}

func (s SimpleNpcFactory) Anatomy(r *rand.Rand) *anatomy.Head {
	return s.anatomyCreateFn()
}

type actionChance struct {
	Chance float64
	Action encounter.Action
}

type SimpleCombatHandler struct {
	timeThreshold time.Duration
	chanceAct     float64
	randomActions []actionChance
}

func (s SimpleCombatHandler) ShouldAct(timeSinceLastActed time.Duration, rng *rand.Rand) bool {
	if timeSinceLastActed < s.timeThreshold {
		return false
	}
	return rng.Float64() < s.chanceAct
}

func (s SimpleCombatHandler) GetAction(self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head, rng *rand.Rand) encounter.Action {
	actionValue := rng.Float64()
	currValue := 0.0
	for _, randomAction := range s.randomActions {
		currValue += randomAction.Chance
		if actionValue < currValue {
			return randomAction.Action
		}
	}
	return s.randomActions[0].Action
}

func (s SimpleCombatHandler) OnVictory(self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head, rng *rand.Rand, game core.Systemer) error {
	return nil
}

func (s SimpleCombatHandler) OnDefeat(self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head, rng *rand.Rand, game core.Systemer) error {
	return nil
}

func (s SimpleCombatHandler) OnTie(self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head, rng *rand.Rand, game core.Systemer) error {
	return nil
}

type SimpleEncounterAction struct {
	name               string
	description        string
	execute            func(s core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (string, time.Duration, error)
	isPlayerApplicable func(s core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (bool, error)
}

func (s SimpleEncounterAction) Name() string {
	return s.name
}

func (s SimpleEncounterAction) Description() string {
	return s.description
}

func (s SimpleEncounterAction) Execute(sy core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (string, time.Duration, error) {
	return s.execute(sy, self, other, selfAnat, otherAnat)
}

func (s SimpleEncounterAction) IsApplicableForPlayer(sy core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (bool, error) {
	return s.isPlayerApplicable(sy, self, other, selfAnat, otherAnat)
}

func getNpcFactories() ([]creature.NpcFactory, map[string]encounter.CombatHandler, []templates.TemplateEntry) {
	return []creature.NpcFactory{
			&SimpleNpcFactory{
				map[string]int{mapgen.TaigaName: 100},
				creature.Creature{
					Hunger:            500,
					MaxHunger:         500,
					Thirst:            500,
					MaxThirst:         500,
					Consciousness:     500,
					MaxConsciousness:  500,
					Submissiveness:    500,
					MaxSubmissiveness: 500,
					Breath:            500,
					MaxBreath:         500,
					Will:              500,
					MaxWill:           500,
					Strength:          8,
					Acumen:            8,
					Attunement:        8,
					Introspection:     8,
					Meditation:        8,
					Compassion:        8,
				},
				"testKind",
				[]string{"testKindEntry"},
				&ai.BiomeWalker{
					MoveTick:       time.Second * 5,
					TimeNotMoving:  &ai.UniformTimeDist{Min: time.Minute * 2, Max: time.Minute * 5},
					DistanceMoving: &ai.UniformIntDist{Min: 4, Max: 10},
				},
				func() *anatomy.Head {
					return anatomy.HumanMaleAnatomy()
				},
			},
		}, map[string]encounter.CombatHandler{
			"testKind": &SimpleCombatHandler{
				timeThreshold: time.Minute,
				chanceAct:     0.25,
				randomActions: []actionChance{
					{
						Chance: 1.0,
						Action: &SimpleEncounterAction{
							name:        "Roar",
							description: "Roar loudly",
							execute: func(s core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (string, time.Duration, error) {
								other.Submissiveness--
								return "ROAR!", time.Second, nil
							},
							isPlayerApplicable: func(s core.Systemer, self, other *creature.Creature, selfAnat, otherAnat *anatomy.Head) (bool, error) {
								return false, nil
							},
						},
					},
				},
			},
		}, []templates.TemplateEntry{
			{"testKindEntry", "There is a wild Test creature here."},
		}
}
